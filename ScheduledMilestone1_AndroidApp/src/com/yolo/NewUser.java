package com.yolo;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * The Main Activity.
 * 
 * This activity starts up the RegisterActivity immediately, which communicates
 * with your App Engine backend using Cloud Endpoints. It also receives push
 * notifications from backend via Google Cloud Messaging (GCM).
 * 
 * Check out RegisterActivity.java for more details.
 */
public class NewUser extends Activity {

	EditText eTxtUser;
	EditText eTxtPassword;
	
	
	Button verifyBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_user);
		verifyBtn 		= (Button) findViewById(R.id.btnVerifyReg);
		verifyBtn.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {

				Intent i = new Intent();
				i.setClassName("com.yolo", "com.yolo.AccountList");
				startActivity(i);
				}
		    }); //*/
	}
	
	
}
