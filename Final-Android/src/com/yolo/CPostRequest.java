package com.yolo;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CPostRequest extends CPost {

	
	public CPostRequest(String startPointIn, String endPointIn){
		super();
		startPoint 	= startPointIn;
		endPoint 	= endPointIn;
		
	}
	 public CPostRequest(String startPointIn, String endPointIn, String depTimeIn,
				String depDateIn, boolean pickupIn, boolean petsIn,
				boolean handicapIn) { 

				super(startPointIn, endPointIn, depTimeIn, depDateIn, pickupIn, petsIn,
						handicapIn);
		} //*/

	 public CPostRequest(String poster, String startPointIn, String endPointIn, String depTimeIn,
			String depDateIn, boolean pickupIn, boolean petsIn,
			boolean handicapIn) { 
			super(poster,startPointIn, endPointIn, depTimeIn, depDateIn, pickupIn, petsIn,
					handicapIn);
	} //*/

	


	@Override
	List<NameValuePair> submit() {
		List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
		listTemp.add(new BasicNameValuePair("class", "CPostRequest"));
		listTemp.add(new BasicNameValuePair("startPoint", startPoint));
		System.out.println("START POINT IS:::   " + startPoint);
		listTemp.add(new BasicNameValuePair("endPoint", endPoint)); 
		System.out.println("END POINT IS:::   " + endPoint);
		listTemp.add(new BasicNameValuePair("depTime", depTime)); 
		listTemp.add(new BasicNameValuePair("depDate", depDate)); //*/
		listTemp.add(new BasicNameValuePair("pickup", String.valueOf(pickup))); 
		listTemp.add(new BasicNameValuePair("pets", String.valueOf(pets))); 
		listTemp.add(new BasicNameValuePair("handicap", String.valueOf(handicap))); 
		return listTemp;
	}
}
