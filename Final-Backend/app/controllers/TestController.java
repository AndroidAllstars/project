package controllers;

import play.mvc.*;

public class TestController extends Controller {
	 public static Result test() {
		 return ok("test1 successful");
	 }
	 
	 @With(SecurityController.class)
	 public static Result test2() {
		 return ok("test2 successful");
	 }
}
