package com.yolo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;


import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * The Main Activity.
 * 
 * This activity starts up the RegisterActivity immediately, which communicates
 * with your App Engine backend using Cloud Endpoints. It also receives push
 * notifications from backend via Google Cloud Messaging (GCM).
 * 
 * Check out RegisterActivity.java for more details.
 */
public class NewUser2 extends Activity {


	Button createBtn;
	CUser  user;
	String userStr;
	String passStr;
	String nameStr;
	String phoneStr;
	String addressStr;
	String idNumStr;
	String plateStr;
	String carStr;
	
	EditText eTxtUser;
	EditText eTxtPass;
	EditText eTxtName;
	EditText eTxtPhone;
	EditText eTxtAddress;
	EditText eTxtIDNum;
	EditText eTxtPlate;
	@Override
	protected void onResume() 
	{
		super.onResume();


	    YoloRide appState = ((YoloRide)getApplicationContext());
	    boolean hindi = appState.getHindi();
		if (hindi)
		{
			EditText user = (EditText) findViewById(R.id.eTxtUserReg);
			user.setHint("प्रयोक्ता नाम");
			EditText pass = (EditText) findViewById(R.id.eTxtPassReg);
			pass.setHint("पासवर्ड");
			EditText name = (EditText) findViewById(R.id.eTxtRealNameReg);
			name.setHint("असल नाम");
			EditText phone = (EditText) findViewById(R.id.eTxtPhoneReg);
			phone.setHint("फोन नंबर");
			EditText address = (EditText) findViewById(R.id.eTxtAddressReg);
			address.setHint("पता");
			EditText idnum = (EditText) findViewById(R.id.eTxtIDNumReg);
			idnum.setHint("ड्राइवर का लाइसेंस नंबर");
			EditText plate = (EditText) findViewById(R.id.eTxtLicensePlateReg);
			plate.setHint("लाइसेंस प्लेट संख्या");
			TextView required = (TextView) findViewById(R.id.textView2);
			required.setText("सवारी की पेशकश करने के लिए आवश्यक:");
			Button creator = (Button) findViewById(R.id.btnCreateReg);
			creator.setText("खाता बनाने के लिए");
			
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 

		createBtn 		= (Button) findViewById(R.id.btnCreateReg);
		createBtn.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {
		    	  
		    	  eTxtUser		= (EditText) findViewById(R.id.eTxtUserReg);
		    	  eTxtPass		= (EditText) findViewById(R.id.eTxtPassReg);
		    	  eTxtName		= (EditText) findViewById(R.id.eTxtRealNameReg);
		    	  eTxtPhone		= (EditText) findViewById(R.id.eTxtPhoneReg);
		    	  eTxtAddress	= (EditText) findViewById(R.id.eTxtAddressReg);
		    	  eTxtIDNum		= (EditText) findViewById(R.id.eTxtIDNumReg);
		    	  eTxtPlate		= (EditText) findViewById(R.id.eTxtLicensePlateReg);
		    	  
		    	  
		    	  
		    	  
		    	  userStr 	= eTxtUser.getText().toString();
		    	  passStr 	= eTxtPass.getText().toString();
		    	  nameStr 	= eTxtName.getText().toString();
		    	  phoneStr 	= eTxtPhone.getText().toString();
		    	  addressStr= eTxtAddress.getText().toString();
		    	  idNumStr  = eTxtIDNum.getText().toString();
		    	  carStr    = "junk";
		    	  plateStr  = eTxtPlate.getText().toString();
		    	  user = new CUser(userStr, passStr, nameStr, phoneStr, addressStr, idNumStr, carStr, plateStr);
		    	 //SEND STUFF TO SERVER
		    	  
		    	  
		    	  HttpClient httpclient = new DefaultHttpClient();
		    	  //  HttpPost httppost = new HttpPost("http://www.yoursite.com/script.php"); //set this up
		    	    HttpPost httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/users"); //set this up

		    	   //public void submit(){
		    	  try {
	  				   // Add your data
	  		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
	  		        listTemp =  user.submit();
		    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
		    		 

	  		        // Execute HTTP Post Request
	  		       HttpResponse response = httpclient.execute(httppost);
		    		  System.out.println("HEY LOOK AT THIS NUMBER HERE:   4444444444444444");
		    		  System.out.println("HERE IS THE RESPONSE: " + response.getEntity().getContent().toString());
		    		  
		  		    } catch (ClientProtocolException e) {
		  		        // TODO Auto-generated catch block
	  		    } catch (IOException e) {
	  		        // TODO Auto-generated catch block
	  		    } //*/
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
		    	  
				Intent i = new Intent();
				i.setClassName("com.yolo", "com.yolo.Login");
				startActivity(i);
				}
		    }); //*/
	}
	
	
}
