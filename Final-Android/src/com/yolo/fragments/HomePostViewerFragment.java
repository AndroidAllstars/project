package com.yolo.fragments;
import com.yolo.CPost;
import com.yolo.CPostOffer;
import com.yolo.R;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
public class HomePostViewerFragment extends DialogFragment {
    int mNum;
    View v;
    static CPostOffer post;
    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    
    public HomePostViewerFragment(){
    	super();
    	
    }
    
    
    public static HomePostViewerFragment newInstance(CPostOffer postIn) {
    	HomePostViewerFragment f = new HomePostViewerFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
       // args.putString("title", "Title!!");
        f.setArguments(args);
        post = (CPostOffer) postIn;
        return f;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       
    	//String title = getArguments().getString("title");

        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(style, theme);
        return new AlertDialog.Builder(getActivity())
                .setTitle(post.getPoster())// + getString())//.create();
                //.setMessage(getString())
                .setMessage(getString())
                //.setPositiveButton(R.string.ok,  null                )
                .setNegativeButton(R.string.ok, null )
                .create();
        
    }//*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Pick a style based on the num.
        
        /*
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
         //*/
        
    }
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	//CPostOffer postOffer =  post;  //don't do this in the long run.................
        v = inflater.inflate(R.layout.fragment_dialog, container, false);
        
        writeStuff();
  /*      Button button = (Button)v.findViewById(R.id.show);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                ((FragmentDialog)getActivity()).showDialog();
            }
        });  //*/ 
    /*

        return v;
    }//*/
    private String getString(){
    	String someText = (
    				   "Leaving From:	    \t\t" + post.getStart() + 
        		"\n" + "Destination:		\t\t" + post.getEnd() + 
        		"\n" + "Departure Time:		\t\t" + post.getTime() + 
        		"\n" + "Departure Date:  	\t\t" + post.getDate() + 
        		"\n" + "Smoking:	        \t\t" + post.getPickup() + 
        		"\n" + "Pets OK:			\t\t" + post.getPets() + 
        		"\n" + "Handicap Accessible:\t\t" + post.getCrip() + 
        		"\n" + "Fee:				\t\t" + post.getFee() + 
        		"\n" + "Seats Available:	\t" + post.getSeats() 
        						);
    	return someText;
    	
    }
    
    private String formatString(){

		  String outStr = String.format("%-15s %s  %-15s %s   %-15s %s   %-15s %s "
		  + " %-15s %s   %-15s %s   %-15s %s   %-15s %s   %-15s %s " ,
		  " Leaving From:" , post.getStart(),
	"\n Destination:" , post.getEnd() , 
	"\n Departure Time:" , post.getTime() , 
	"\n Departure Date:" , post.getDate() , 
	"\n Smoking:" , post.getPickup() , 
	"\n Pets OK:" , post.getPets() , 
	"\n Handicap Accessible:" , post.getCrip() , 
	"\n Fee:" , post.getFee() , 
	"\n Seats Available:" , post.getSeats() 
    	);
		  return outStr;
    }
    private void writeStuff(){

        View tv = v.findViewById(R.id.text);
        ((TextView)tv).setText("Leaving From:		" + //post.getStart() + "\n" + 
        		"\n" + "Destination:		." + //post.getEnd() + "\n" + 
        		"\n" + "Seats Available:	." + //"dunno yet" + "\n" + 
        		"\n" + "Departure Time:		." + //post.getTime() + "\n" + 
        		"\n" + "Departure Date:  	." + //post.getDate() + "\n" + 
        		"\n" + "Pets OK:			." + //post.getPets() + "\n" + 
        		"\n" + "Handicap Accessible:." + //post.getCrip() + "\n" + 
        		"\n" + "Pickup Availible:	." //+ //post.getPickup()
        						);
        View tv2 = v.findViewById(R.id.text2);

        ((TextView)tv2).setText( post.getStart() + "\n" + 
        						post.getEnd() + "\n" + 
        					    "dunno yet" + "\n" + 
        					    post.getTime() + "\n" + 
        					    post.getDate() + "\n" + 
        					    post.getPets() + "\n" + 
        					    post.getCrip() + "\n" + 
        					    post.getPickup()
        						);
        
    	
    }
}