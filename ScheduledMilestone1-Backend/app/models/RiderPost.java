package models;

import java.util.List;

import javax.persistence.*;

import org.joda.time.DateTime;

import play.db.ebean.Model.Finder;

@Entity
//@Table(name="RPOST")
public class RiderPost extends BasePost {
	
	public static Finder<Long,RiderPost> findRiderPost = new Finder(
		Long.class, RiderPost.class
	);
	
	public static List<RiderPost> allRiderPost() {
		return findRiderPost.all();
	}

	public static void riderCreate(RiderPost riderPost) { 
		riderPost.setPostedDate(DateTime.now());
	    riderPost.save();
	  }
}
