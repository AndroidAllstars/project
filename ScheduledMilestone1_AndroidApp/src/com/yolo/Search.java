package com.yolo;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;


import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Search extends Activity {
	EditText eTxtStartPoint;
	EditText eTxtEndPoint;
	ArrayList<CPostOffer> offers = new ArrayList<CPostOffer>();
	
	
	Button 	 	 searchBtn;
	ToggleButton toggleOptionsSearch;
	LinearLayout linearLayoutSearch;
	ListView listSearch;
	String startPointStr;
	String	endPointStr;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 
		final Context ctx = this;
		eTxtStartPoint = (EditText) findViewById(R.id.eTxtStartPointSearch);
		eTxtEndPoint = (EditText) findViewById(R.id.eTxtEndPointSearch);
		
		linearLayoutSearch  = (LinearLayout) findViewById(R.id.linearLayoutSearch);
		listSearch          = (ListView) findViewById(R.id.listSearch);
		
	//	listSearch.a
		
		
		
		toggleOptionsSearch = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
		toggleOptionsSearch.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {

	  		if(toggleOptionsSearch.isChecked())
	  		{	
	  			linearLayoutSearch.setVisibility(0);
	  		}
	  		else
	  		{
	  			linearLayoutSearch.setVisibility(8);
	  		}
			
		  }
	    }); //*/
		
	    searchBtn = (Button) findViewById(R.id.btnSearch);
	    searchBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  //send stuff here
	    	  HttpClient httpclient = new DefaultHttpClient();
	    	    HttpPost httppost = new HttpPost("http://peaceful-sands-8466.herokuapp.com/driverposts/search"); //set this up
	    	    startPointStr = eTxtStartPoint.getText().toString();
	    	    endPointStr   = eTxtEndPoint.getText().toString();
	    	   //public void submit(){
	    	  try {
				   // Add your data
	    		  CPostOffer searchPostOffer = new CPostOffer(startPointStr, endPointStr);
	    		  
		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
		        listTemp =  searchPostOffer.submit();
	    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
	    		 
		        // Execute HTTP Post Request
		       HttpResponse response = httpclient.execute(httppost);
		       HttpEntity entity = response.getEntity();
		       Integer statusCode = response.getStatusLine().getStatusCode();
		       StringBuilder builder = new StringBuilder();
		       
		       InputStream content = entity.getContent();
		       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		       String line = reader.readLine();
		       builder.append(line);
		       try {
		    	   Translate translate = new Translate();
		    	   offers = translate.action(new JSONObject(builder.toString()));
		    	   for(CPostOffer offer : offers) {
		    		   System.out.println("offer = " + offer.toString());
		    	   }
		       } catch(Exception e) {
		    	   e.printStackTrace();
		       }
	    		  //System.out.println("HERE IS THE RESPONSE - Status Code " + statusCode + " and Response text = " + builder.toString());
	    		  
	  		    } catch (ClientProtocolException e) {
	  		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    } //*/	  
	    	  

	  	    final ListView listview = (ListView) findViewById(R.id.listSearch);
	  	    //String[] values = new String[] { tempPostOffer.toString()};
	  	  
	  	    
	  	    final ArrayList<String> list = new ArrayList<String>();
	  	    for (int i = 0; i < offers.size(); ++i) {
	  	      list.add(offers.get(i).toString());
	  	      System.out.println("offered OOO");
	  	    }
	  	    final StableArrayAdapter adapter = new StableArrayAdapter(ctx,
	  	        android.R.layout.simple_list_item_1, list);
	  	    listview.setAdapter(adapter);
			}
	      
	      
	      
	    });  //*/
	    


/*	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final String item = (String) parent.getItemAtPosition(position);
	      }

	    });//*/
	
	}

	  private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	} //*/

}
