package models;

import java.util.*;
import javax.persistence.*;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonView;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.avaje.ebean.*;

//Named YRUser instead of just User, 'User' will cause a 
//conflict with default variable names in PostgreSQL, so don't use it.

@Entity
public class YRUser extends Model {

	@Id
    @Constraints.Required
    @Formats.NonEmpty
    //@JsonView(JsonViews.Uname.class)
    private String uname;

    @Transient
    @Constraints.Required
    @Constraints.MaxLength(256)
    @JsonIgnore
    private String pass;
    
    @Column(length = 64, nullable = false)
    private byte[] shaPassword;
    
    @JsonIgnore
    private String authToken;
    
    private String name;
    private String phone;
    private String address;
    
    private String license;
    private String plates;
    private String car;
    
    //Google Cloud Messaging registration ID
    @JsonIgnore
    private String registration_id;
    
    //@OneToMany(cascade = CascadeType.PERSIST)
    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    public List<DriverPost> driverPosts = new ArrayList<DriverPost>();
    
    @JsonIgnore
    public List<Long> getUserDriverPosts() {
    	List<Long> list = new ArrayList<Long>();
    	for(DriverPost post : driverPosts) {
    		list.add(post.id);
    	}
    	return list;
    }
    
    //@OneToMany(cascade = CascadeType.PERSIST)
    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    public List<RiderPost> riderPosts = new ArrayList<RiderPost>();
    
    @JsonIgnore
    public List<Long> getUserRiderPosts() {
    	List<Long> list = new ArrayList<Long>();
    	for(RiderPost post : riderPosts) {
    		list.add(post.id);
    	}
    	return list;
    }
    
    public static Model.Finder<String,YRUser> findUser = new Model.Finder(String.class, YRUser.class);
    
    public static List<YRUser> findAllUsers() {
        return findUser.all();
    }
    
    public static void create(YRUser user) {
    	user.save();
    }
    
    public void addDriverPost(DriverPost post) {
    	driverPosts.add(post);
    	save();
    }
    
    public void addRiderPost(RiderPost post) {
    	riderPosts.add(post);
    	save();
    }
    
    public static YRUser getYRUser(String username) {
    	return findUser.where()
    		.eq("uname", username)
    		.findUnique();
    }
    
    public String createToken() {
        authToken = UUID.randomUUID().toString();
        save();
        return authToken;
    }

    public void deleteAuthToken() {
        authToken = null;
        save();
    }
    
    public static byte[] getSha512(String value) {
        try {
            return MessageDigest.getInstance("SHA-512").digest(value.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static YRUser findByAuthToken(String authToken) {
        if (authToken == null) {
            return null;
        }

        try {
            return findUser.where().eq("authToken", authToken).findUnique();
        }
        catch (Exception e) {
            return null;
        }
    }

    public static YRUser findByUsernameAndPassword(String username, String password) {
        // todo: verify this query is correct. Does it need an "and" statement?
        return findUser.where()
        		.eq("uname", username)
        		.eq("shaPassword", getSha512(password))
        		.findUnique();
    }
    
    public static YRUser findByUsername(String username) {
    	return findUser.where()
    			.eq("uname", username)
    			.findUnique();
    }

	public String getRegistration_id() {
		return registration_id;
	}

	public void setRegistration_id(String registration_id) {
		this.registration_id = registration_id;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
		shaPassword = getSha512(pass);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getPlates() {
		return plates;
	}

	public void setPlates(String plates) {
		this.plates = plates;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

    
    
    
}
