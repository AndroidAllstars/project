package controllers;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import org.codehaus.jackson.node.ObjectNode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;

import models.DriverPost;
import models.OfferFeedback;
import models.Payment;
import models.RiderPost;
import models.YRUser;
import play.Logger;
import play.Play;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Akka;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.*;

public class OfferController extends Controller {

	static Form<DriverPost> driverPostForm = Form.form(DriverPost.class);
	static Form<OfferFeedback> offerFeedbackForm = Form.form(OfferFeedback.class);
	
	static String timeFormat = "MM/dd/yyyy HH:mm";
	
	public static Result driverPostsJson() {
    	ObjectNode result = Json.newObject();
    	result.put("numberOfResults", DriverPost.allDriverPost().size());
    	result.put("results", Json.toJson(DriverPost.allDriverPost()));
    	return ok(result);
    }
	
	@With(SecurityController.class)
	public static Result newDriverPost() {
    	ObjectNode result = Json.newObject();
    	Form<DriverPost> filledForm = driverPostForm.bindFromRequest();
    	if(filledForm.hasErrors()) {
    		result.put("success", false);
    		result.put("errorMessage", filledForm.errorsAsJson());
    		return badRequest(result);
    	} else {
    		DriverPost post = filledForm.get();
    		if(post.depDate == null || post.depTime == null) {
    			result.put("success", false);
    			result.put("errorMessage", "The departing date and time are required.");
    			return badRequest(result);
    		}
    		post.depDateTime = DateTime.parse(
    				post.depDate.toString() + " " + post.depTime.toString(),
    				DateTimeFormat.forPattern(timeFormat));
    		String maxSeats = filledForm.field("maxSeats").value();
    		String reqFee = filledForm.field("reqFee").value();
    		if(maxSeats != null) {
    			post.maxSeats = Integer.parseInt(filledForm.field("maxSeats").value());
    		}
    		if(reqFee != null) {
    			post.reqFee = Double.parseDouble(filledForm.field("reqFee").value());
    		}
    		
    		YRUser user = SecurityController.getUser();
    		
    		/* The JPA relationship automatically adds this post to the user's list of posts */
    		post.owner = user;
    		post.escrow = 0.00;
    		DriverPost.driverCreate(post);
    		
    		result.put("success",true);
    		result.put("user", user.getUname());
    		result.put("id", post.id);
    		return ok(result);
    	}
    }
	
	@With(SecurityController.class)
	public static Result deleteDriverPost(Long id) {
    	ObjectNode result = Json.newObject();
    	if(DriverPost.findDriverPost.byId(id).owner != SecurityController.getUser()) {
    		result.put("success", false);
    		result.put("errorMessage", "You do not own this post.");
    		return badRequest(result);
    	}
    	if(DriverPost.deleteDriverPost(id) == false) {
    		result.put("success", false);
    		result.put("errorMessage", "Driver post of id " + id + " does not exist");
    		return badRequest(result);
    	}
    	result.put("success", true);
    	return ok(result);
    }
	
	public static Result driverPostSearch() {
    	
    	DynamicForm searchParams = DynamicForm.form().bindFromRequest();
    	ObjectNode result = Json.newObject();
    	
    	if(searchParams.get("endPoint") == null) {
    		result.put("success", false);
    		result.put("error", true);
    		result.put("errorMessage", "Your destination is missing.");
    		return badRequest(result);
    	}
    	
    	
    	DateTime reqDateTime = null;
    	if(searchParams.get("depDate") != null) {
    		Logger.debug("depDate - " + searchParams.get("depDate"));
    		if(searchParams.get("depTime") != null) {
    			Logger.debug("depTime - " + searchParams.get("depTime"));
    			reqDateTime = DateTime.parse(
    				searchParams.get("depDate") + " " + searchParams.get("depTime"),
    				DateTimeFormat.forPattern(timeFormat));
    		}
    		reqDateTime = DateTime.parse(
    				searchParams.get("depDate") + " 00:00", DateTimeFormat.forPattern(timeFormat));
    	}
    	
    	
    	List<DriverPost> queryResults = DriverPost.searchDriverPost(
    			searchParams.get("startPoint"),
    			searchParams.get("endPoint"),
    			reqDateTime,
    			searchParams.get("pets"),
    			searchParams.get("handicap"),
    			searchParams.get("pickup"), 
    			searchParams.get("reqFee"));
    	
    	result.put("success", true);
    	result.put("numberOfResults", queryResults.size());
    	result.put("results", Json.toJson(queryResults));
    	return ok(result);
    }
	
	@With(SecurityController.class)
    public static Result acceptRider(final Long id) {
		final YRUser user = SecurityController.getUser();
    	final DynamicForm form = DynamicForm.form().bindFromRequest();
		
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
			new Callable<ObjectNode>() {
				public ObjectNode call() {
					ObjectNode result = Json.newObject();
			    	
			    	YRUser requestedRider = YRUser.findByUsername(form.get("rider"));
			    	
			    	if(requestedRider == null) {
			    		result.put("success", false);
			    		result.put("errorMessage", "Rider does not exist.");
			    		return result;
			    	}
			    	
			    	DriverPost post = DriverPost.findDriverPost.byId(id);
			    	if(!post.owner.equals(user)) {
			    		result.put("success", false);
			    		result.put("errorMessage", "You do not own this posting.");
			    		return result;
			    	}
			    	if(!post.getRequestedList().contains(requestedRider.getUname())) {
			    		result.put("success", false);
			    		result.put("errorMessage", "That user did not request to ride in this carpool or has already been accepted into the carpool.");
			    		return result;
			    	}
			    	if(post.seatsOpen != null) {
			    		if(post.seatsOpen < 1) {
			    			result.put("success", false);
			    			result.put("errorMessage", "No more seats are available.");
			    			return result;
			    		}
			    	}
			    	post.riders.add(requestedRider);
			    	post.requestedRiders.remove(requestedRider);
			    	post.save();
			    	
			    	/* Send notification to rider */
			    	com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
					Message message = new Message.Builder()
						.collapseKey("acceptNotification")
						.timeToLive(86400)
						.delayWhileIdle(false)
						.addData("message", post.owner.getUname() + " has accepted your carpool request!")
						.addData("type", "accept_rider")
						.addData("id", String.valueOf(post.id))
						.addData("driver", post.owner.getUname())
						.build();
					try {
						gcmResult = sender.send(message, requestedRider.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error.");
						//e.printStackTrace();
						return result;
					}
				}
			}
		);
		
		return async(
			gcmResultPromise.map(
	    		new Function<ObjectNode,Result>() {
	   				public Result apply(ObjectNode r) {
	   					if(r.get("success").asBoolean() == false) {
	   						return badRequest(r);
	   					}
	   					return ok(r);
	   				}
	   			}
    		)
		);
    	
    }
	
	@With(SecurityController.class)
	public static Result rejectRequest(final Long id) {
		final YRUser user = SecurityController.getUser();
		final DynamicForm form = DynamicForm.form().bindFromRequest();
	
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
				new Callable<ObjectNode>() {
					public ObjectNode call() {
						ObjectNode result = Json.newObject();
				    	
				    	YRUser requestedRider = YRUser.findByUsername(form.get("rider"));
				    	
				    	if(requestedRider == null) {
				    		result.put("success", false);
				    		result.put("errorMessage", "Rider does not exist.");
				    		return result;
				    	}
				    	
				    	DriverPost post = DriverPost.findDriverPost.byId(id);
				    	if(!post.owner.equals(user)) {
				    		result.put("success", false);
				    		result.put("errorMessage", "You do not own this posting.");
				    		return result;
				    	}
				    	if(!post.getRequestedList().contains(requestedRider.getUname())) {
				    		result.put("success", false);
				    		result.put("errorMessage", "That user did not request to ride in this carpool.");
				    		return result;
				    	}
				    	post.requestedRiders.remove(requestedRider);
				    	post.save();
				    	
				    	/* Send notification to rider */
				    	com.google.android.gcm.server.Result gcmResult;
						Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
						Message message = new Message.Builder()
							.collapseKey("acceptNotification")
							.timeToLive(86400)
							.delayWhileIdle(false)
							.addData("type", "reject_rider")
							.addData("uname", post.owner.getUname())
							.addData("message", post.owner.getUname() + " has rejected your carpool request!")
							.addData("id", String.valueOf(post.id))
							.build();
						try {
							gcmResult = sender.send(message, requestedRider.getRegistration_id(), 5);
							result.put("success", true);
							result.put("gcmResult", gcmResult.toString());
							result.put("gcmErrorCode", gcmResult.getErrorCodeName());
							return result;
						} catch (IOException e) {
							result.put("success", false);
							result.put("gcmResult", "Internal server error.");
							//e.printStackTrace();
							return result;
						}
					}
				}
			);
			
			return async(
				gcmResultPromise.map(
		    		new Function<ObjectNode,Result>() {
		   				public Result apply(ObjectNode r) {
		   					if(r.get("success").asBoolean() == false) {
		   						return badRequest(r);
		   					}
		   					return ok(r);
		   				}
		   			}
	    		)
			);
	    	
	    }
    
    @With(SecurityController.class)
    public static Result riderRequest(final Long id) {
    	final YRUser user = SecurityController.getUser();
    	
    	/* Promise = Asynchronous call to prevent server from blocking */
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
			new Callable<ObjectNode>() {
				public ObjectNode call() {
					
					ObjectNode result = Json.newObject();
			    	
			    	DriverPost post = DriverPost.findDriverPost.byId(id);
			    	if(post.riders.contains(user)) {
			    		result.put("success", false);
			    		result.put("errorMessage", "You have already been confirmed as a rider!");
			    		return result;
			    	}
			    	if(post.seatsOpen != null) {
			    		if(post.seatsOpen < 1) {
			    			result.put("success", false);
			    			result.put("errorMessage", "No more seats are available.");
			    			return result;
			    		}
			    	}
			    	post.requestedRiders.add(user);
			    	post.save();
			    	
					com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
					Message message = new Message.Builder()
						.collapseKey("requestNotification")
						.timeToLive(86400)
						.delayWhileIdle(false)
						.addData("message", user.getUname() + " has requested to join your carpool!")
						.addData("type", "rider_request")
						.addData("id", String.valueOf(post.id))
						.addData("rider", user.getUname())
						.build();
					try {
						gcmResult = sender.send(message, post.owner.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error.");
						//e.printStackTrace();
						return result;
					}
				}
			}
		);
		
    	return async(
    		gcmResultPromise.map(
    			new Function<ObjectNode,Result>() {
    				public Result apply(ObjectNode r) {
    					if(r.get("success").asBoolean() == false) {
	   						return badRequest(r);
	   					}
    					return ok(r);
    				}
    			}
    		)
    	);
    }
    
    @With(SecurityController.class)
    public static Result cancelRequest(final Long id) {
    	final YRUser user = SecurityController.getUser();
    	
    	Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
    		new Callable<ObjectNode>() {
    			public ObjectNode call() {
    				ObjectNode result = Json.newObject();
    				
    				com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
    		    	
    		    	DriverPost post = DriverPost.findDriverPost.byId(id);
    		    	if(!post.requestedRiders.contains(user)) {
    		    		if(!post.riders.contains(user)) {
    		    			result.put("success", false);
    		    			result.put("errorMessage", 
    		    				"The driver has either denied your request to join the carpool or you did not request to join this ride.");
    		    			return result;
    		    		}
    		    		post.riders.remove(user);
    		    		result.put("success", true);
    		    		
    		    		/* This is if the user ALREADY in the carpool cancels his carpool spot */
    		    		
    		    		Message message = new Message.Builder()
						.collapseKey("requestNotification")
						.timeToLive(86400)
						.delayWhileIdle(false)
						.addData("message", user.getUname() + " has left your carpool!")
						.addData("id", String.valueOf(post.id))
						.addData("rider", user.getUname())
						.build();
    		    		
    		    		try {
    						gcmResult = sender.send(message, post.owner.getRegistration_id(), 5);
    						result.put("success", true);
    						result.put("gcmResult", gcmResult.toString());
    						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
    						return result;
    					} catch (IOException e) {
    						result.put("success", false);
    						result.put("gcmResult", "Internal server error.");
    						//e.printStackTrace();
    						return result;
    					}
    		    	}
    		    	post.requestedRiders.remove(user);
    		    	
    		    	/* This is if the user is only on the request list but NOT accepted */
    		    	
    		    	Message message = new Message.Builder()
					.collapseKey("requestNotification")
					.timeToLive(86400)
					.delayWhileIdle(false)
					.addData("message", user.getUname() + " does not request to join your carpool anymore.")
					.addData("id", String.valueOf(post.id))
					.addData("rider", user.getUname())
					.build();
		    		
		    		try {
						gcmResult = sender.send(message, post.owner.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error.");
						//e.printStackTrace();
						return result;
					}
    			}
    		}
    	);
    	
    	return async(
    		gcmResultPromise.map(
    	   		new Function<ObjectNode,Result>() {
     				public Result apply(ObjectNode r) {
       					if(r.get("success").asBoolean() == false) {
  	   						return badRequest(r);
   	   					}
  	   					return ok(r);
       				}
      			}
       		)
    	);	
    }
    
    @With(SecurityController.class) 
    public static Result transferMoney(final Long id) {
    	final YRUser rider = SecurityController.getUser();
    	
    	Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
        		new Callable<ObjectNode>() {
        			public ObjectNode call() {
        				ObjectNode result = Json.newObject();
        				
        				com.google.android.gcm.server.Result gcmResult;
    					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
        		    	
        		    	DriverPost post = DriverPost.findDriverPost.byId(id);
        		    	/*if(!post.riders.contains(rider)) {
        		    		result.put("success", false);
        		    		result.put("errorMessage", "You are not in this ride.");
        		    		return result;
        		    	}*/
        		    	
        		    	Payment payment = new Payment();
        		    	payment.amount = post.reqFee;
        		    	payment.driver = post.owner;
        		    	payment.rider = rider;
        		    	payment.post = post;
        		    	payment.save();
        		    	post.payments.add(payment);
        		    	post.escrow += payment.amount;
        		    	post.save();
        		    	
        		    	/* This is if the user is only on the request list but NOT accepted */
        		    	
        		    	Message message = new Message.Builder()
    					.collapseKey("paymentNotification")
    					.timeToLive(86400)
    					.delayWhileIdle(false)
    					.addData("type", "payment")
    					.addData("message", rider.getUname() + " has sent you a payment of $" + payment.amount)
    					.addData("id", String.valueOf(post.id))
    					.addData("rider", rider.getUname())
    					.build();
    		    		
    		    		try {
    						gcmResult = sender.send(message, post.owner.getRegistration_id(), 5);
    						result.put("success", true);
    						result.put("gcmResult", gcmResult.toString());
    						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
    						return result;
    					} catch (IOException e) {
    						result.put("success", false);
    						result.put("gcmResult", "Internal server error.");
    						//e.printStackTrace();
    						return result;
    					}
        			}
        		}
        	);
        	
        	return async(
        		gcmResultPromise.map(
        	   		new Function<ObjectNode,Result>() {
         				public Result apply(ObjectNode r) {
           					if(r.get("success").asBoolean() == false) {
      	   						return badRequest(r);
       	   					}
      	   					return ok(r);
           				}
          			}
           		)
        	);
    } 
    
    @With(SecurityController.class)
    public static Result rateRide(Long id) {
    	YRUser rider = SecurityController.getUser();
    	DriverPost driverPost = DriverPost.findDriverPost.byId(id);
    	ObjectNode result = Json.newObject();
    	
    	if(!driverPost.riders.contains(rider)) {
    		result.put("success", false);
    		result.put("errorMessage", rider.getUname() + " was not in this carpool.");
    		return badRequest(result);
    	}
    	
    	Form<OfferFeedback> filledForm = offerFeedbackForm.bindFromRequest();
    	if(filledForm.hasErrors()) {
    		result.put("success", false);
    		result.put("errorMessage", filledForm.errorsAsJson());
    		return badRequest(result);
    	}
    	OfferFeedback feedback = filledForm.get();
    	feedback.rater = rider;
    	feedback.ratee = driverPost.owner;
    	feedback.post = driverPost;
    	
    	// equals returns if a user has already posted something
    	// not necessarily if the feedback is the same object
    	/*if(driverPost.feedback.contains(feedback)) {
    		result.put("success", false);
    		result.put("errorMessage", "You have already submitted feedback.");
    		return badRequest(result);
    	}*/
    	feedback.save();
    	driverPost.feedback.add(feedback);
    	driverPost.save();
    	
    	result.put("success", true);
    	
    	return ok(result);
    }
    
    @With(SecurityController.class)
    public static Result rateRider(Long id) {
    	ObjectNode result = Json.newObject();
    	
    	DriverPost driverPost = DriverPost.findDriverPost.byId(id);
    	DynamicForm filledForm = DynamicForm.form().bindFromRequest();
    	YRUser rider = YRUser.findByUsername(filledForm.get("rider"));
    	
    	if(!driverPost.riders.contains(rider)) {
    		result.put("success", false);
    		result.put("errorMessage", rider.getUname() + " did not ride with this carpool.");
    		return badRequest(result);
    	}
    	
    	YRUser driver = SecurityController.getUser();
    	
    	return ok(result);
    }
}
