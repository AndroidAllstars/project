
 package com.yolo;

 


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
public class AccountList extends ListActivity 
{
	public AccountManager accountManager;
	public Intent intent;
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{ //should look into savedinstance state, not sure about that
		super.onCreate(savedInstanceState);
		accountManager = AccountManager.get(getApplicationContext());
		Account[] accounts = accountManager.getAccountsByType("com.google");
		this.setListAdapter(new ArrayAdapter(this, R.layout.list_item, accounts));
	}
	@Override 
	protected void onListItemClick(ListView l, View v, int position, long id)
	{	System.out.println("AT LEAST WE GOT TO HERE!!!!!!!!!!!!!!!!!!!!!");
		Account account = (Account)getListView().getItemAtPosition(position);
		Intent intent = new Intent(this, AppInfo.class);
		intent.putExtra("account", account);
		intent.setClassName("com.yolo", "com.yolo.AppInfo");

		startActivity(intent);
	}
}

//*/