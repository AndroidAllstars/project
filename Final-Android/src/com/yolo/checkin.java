package com.yolo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
	
public class checkin extends Activity
{	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin);
		Button CCBtn = (Button) findViewById(R.id.CCNS);

		CCBtn.setOnClickListener(new OnClickListener() { 
			public void onClick(View v) 
			{			
				EditText eCCN = (EditText) findViewById(R.id.CCNO);
				if (Check(eCCN.getText().toString()) == true)
				{
					//ADD USER TO RIDE HERE
					Toast.makeText(getApplicationContext(), "Successfully checked in!", Toast.LENGTH_LONG).show();
					Intent i = new Intent();
					i.setClassName("com.yolo", "com.yolo.Home");
					startActivity(i);
				}
				else	
				{
					Toast.makeText(getApplicationContext(), "INVALID CREDIT CARD #", Toast.LENGTH_LONG).show();
					
				}
			}
		
		});
	}
	@Override
	protected void onResume()
	  {
		  super.onResume();
		  if (Locale.getDefault().getDisplayLanguage().equals("Hindi"))
		  {
			
			  Button submit = (Button) findViewById(R.id.CCNS);
			  submit.setText("प्रस्तुत करना");
			  EditText number = (EditText) findViewById(R.id.CCNO);
			  number.setHint("क्रेडिट कार्ड नंबर");
		  }
		  else
		  {
			 
			  Button submit = (Button) findViewById(R.id.CCNS);
			  submit.setText("Submit");
			  EditText number = (EditText) findViewById(R.id.CCNO);
			  number.setHint("credit card number");
		  }
	  }


	public boolean Check(String ccNumber)
	{
		int sum = 0;
		boolean alternate = false;
		for (int i = ccNumber.length() - 1; i >= 0; i--)
		{
			int n = Integer.parseInt(ccNumber.substring(i, i + 1));
			if (alternate)
			{
				n *= 2;
				if (n > 9)
				{
					n = (n % 10) + 1;
				}
			}
			sum += n;
			alternate = !alternate;
		}
		return (sum % 10 == 0);
	}
}	


