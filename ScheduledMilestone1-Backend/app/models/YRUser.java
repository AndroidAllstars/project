package models;

import java.util.*;
import javax.persistence.*;

import play.db.ebean.*;
import play.data.format.*;
import play.data.validation.*;

import com.avaje.ebean.*;

//Named YRUser instead of just User, 'User' will cause a 
//conflict with default variable names in PostgreSQL, so don't use it.

@Entity
public class YRUser extends Model {

	@Id
    @Constraints.Required
    @Formats.NonEmpty
    private String uname;
    
    @Constraints.Required
    //Should be hashed
    private String pass;
    
    private String name;
    private String phone;
    private String address;
    
    private String license;
    private String plates;
    private String car;
    
    public static Model.Finder<String,YRUser> findUser = new Model.Finder(String.class, YRUser.class);
    
    public static List<YRUser> findAllUsers() {
        return findUser.all();
    }
    
    public static void create(YRUser user) {
    	user.save();
    }
    
    public static YRUser authenticate(String username, String password) {
    	return findUser.where()
    		.eq("uname", username)
    		.eq("pass", password)
    		.findUnique();
    }
    
    public static YRUser getYRUser(String username) {
    	return findUser.where()
    		.eq("uname", username)
    		.findUnique();
    }

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getPlates() {
		return plates;
	}

	public void setPlates(String plates) {
		this.plates = plates;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

    
    
    
}
