package models;

import java.util.*;

import play.db.ebean.*;
import play.data.validation.Constraints.*;

import javax.persistence.*;

import org.joda.time.DateTime;

@Entity
/* @Table(name="BPOST")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) */
public class BasePost extends Model {

@Id
  public Long id;
  
  @Required
  public String startPoint;
  @Required
  public String endPoint;
  public String depTime;
  public String depDate;
  public String depTimes;
  public String depDates;
  
  private DateTime postedDate;
  
  private Boolean pets;
  private Boolean pickup;
  private Boolean handicap;

  public static Finder<Long,BasePost> findBasePost = new Finder(
    Long.class, BasePost.class
  );

  public static List<BasePost> allBasePost() {
	  return findBasePost.all();
  }
 
  public static void create(BasePost basePost) { 
	basePost.setPostedDate(DateTime.now());
    basePost.save();
  }

  public static void delete(Long id) {
    findBasePost.ref(id).delete(); 
  }
  
  public DateTime getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(DateTime postedDate) {
		this.postedDate = postedDate;
	}

	public Boolean getPets() {
		return pets;
	}

	public void setPets(Boolean pets) {
		this.pets = pets;
	}

	public Boolean getPickup() {
		return pickup;
	}

	public void setPickup(Boolean pickup) {
		this.pickup = pickup;
	}

	public Boolean getHandicap() {
		return handicap;
	}

	public void setHandicap(Boolean handicap) {
		this.handicap = handicap;
	}
}