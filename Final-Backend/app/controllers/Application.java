package controllers;

import java.util.List;

import play.libs.Json;
import play.mvc.*;
import play.data.*;

import models.*;
import org.codehaus.jackson.node.ObjectNode;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class Application extends Controller {
 
	static Form<BasePost> basePostForm = Form.form(BasePost.class);
	static Form<RiderPost> riderPostForm = Form.form(RiderPost.class);
	static Form<DriverPost> driverPostForm = Form.form(DriverPost.class);
    static Form<YRUser> userForm = Form.form(YRUser.class);
   
    static String timeFormat = "MM/dd/yyyy HH:mm";
  
    public static Result index() {
      return redirect(routes.Application.posts());
    }
 
    public static Result posts() {
      return ok(views.html.index.render(BasePost.allBasePost(), basePostForm));
    }
    
    public static Result riderPostsJson() {
    	ObjectNode result = Json.newObject();
    	result.put("numberOfResults", RiderPost.allRiderPost().size());
    	result.put("results", Json.toJson(RiderPost.allRiderPost()));
    	return ok(result);
    }
    
    @With(SecurityController.class)
    public static Result newRiderPost() {
    	ObjectNode result = Json.newObject();
    	Form<RiderPost> filledForm = riderPostForm.bindFromRequest();
    	if(filledForm.hasErrors()) {
    		result.put("success", false);
    		result.put("errorMessage", filledForm.errorsAsJson());
    		return badRequest(result);
    	} else {
    		RiderPost post = filledForm.get();
    		if(post.depDate == null || post.depTime == null) {
    			result.put("success", false);
    			result.put("errorMessage", "The departing date and time are required.");
    			return badRequest(result);
    		}
    		post.depDateTime = DateTime.parse(
    				post.depDate.toString() + " " + post.depTime.toString(),
    				DateTimeFormat.forPattern(timeFormat));
    		
    		YRUser user = SecurityController.getUser();
    		
    		post.owner = user;
    		RiderPost.riderPostCreate(post);
    		
    		result.put("success",true);
    		result.put("depDateTime", post.depDateTime.toString(DateTimeFormat.forPattern(timeFormat)));
    		return ok(result);
    	}
    }
    
    
    
    public static Result deleteRiderPost(Long id) {
    	ObjectNode result = Json.newObject();
    	RiderPost.deleteRiderPost(id);
    	result.put("success", true);
    	return ok(result);
    }
    
    public static Result riderPostSearch() {
    	ObjectNode result = Json.newObject();
    	DynamicForm searchParams = DynamicForm.form().bindFromRequest();
    	
    	if(searchParams.get("endPoint") == null) {
    		result.put("success", false);
    		result.put("error", true);
    		result.put("errorMessage", "Your destination is missing.");
    		return badRequest(result);
    	}
    	
    	DateTime reqDateTime = null;
    	if(searchParams.get("depDate") != null && searchParams.get("depTime") != null) {
    		reqDateTime = DateTime.parse(
				searchParams.get("depDate") + " " + searchParams.get("depTime"),
				DateTimeFormat.forPattern(timeFormat));
    	}
    	
    	List<RiderPost> queryResults = RiderPost.searchRiderPost(
    			searchParams.get("startPoint"),
    			searchParams.get("endPoint"),
    			reqDateTime,
    			searchParams.get("pets"),
    			searchParams.get("handicap"),
    			searchParams.get("pickup"), 
    			searchParams.get("reqFee"));
    	
    	result.put("success", true);
    	result.put("numberOfResults", queryResults.size());
    	result.put("results", Json.toJson(queryResults));
    	return ok(result);
    }
}
