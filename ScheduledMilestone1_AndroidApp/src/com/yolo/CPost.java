package com.yolo;

import java.util.List;

import org.apache.http.NameValuePair;

abstract public class CPost {
	
	String startPoint;
	String endPoint;
	String depTime;
	String depDate;
	// add in double fee;
	// add in seats later;
	boolean pickup;
	boolean pets;
	boolean handicap;

	public CPost(){

		 startPoint = "";
		 endPoint 	= "";
		 depTime 	= "";
		 depDate 	= "";
		// add in double fee;
		// add in seats later;
		 pickup 	= false;
		 pets 		= false;
		 handicap 	= false;
	}
	public CPost(String startPointIn, String endPointIn, String depTimeIn, String depDateIn, boolean pickupIn, boolean petsIn, boolean handicapIn) {

		 startPoint = startPointIn;
		 endPoint 	= endPointIn;
		 depTime 	= depTimeIn;
		 depDate 	= depDateIn;
		// add in double fee;
		// add in seats later;
		 pickup 	= pickupIn;
		 pets 		= petsIn;
		 handicap 	= handicapIn;
	}
	
	public String toString(){
		String outStr;
		outStr = "Ride on " + depDate + " at " + depTime + "\nFrom " + startPoint + " to " + endPoint;
		return outStr;
	}
	
	abstract List<NameValuePair> submit();
	
}
