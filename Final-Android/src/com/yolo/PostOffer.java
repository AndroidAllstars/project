package com.yolo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
//*/

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.yolo.fragments.DatePickerFragment;
import com.yolo.fragments.TimePickerFragment;


public class PostOffer extends Activity implements TimePickerFragment.OnCompleteListener, DatePickerFragment.OnCompleteListener{
	Button postBtn;
	CPostOffer tempPostOffer;
	String startPointTemp;
	String endPointTemp;
	String depTimeTemp;
	String depDateTemp;
	final static int SIZE = CPost.getSize();
	
	boolean smokeTemp;
	boolean petsTemp;
	boolean cripTemp;
	
	String smokeStr;
	String petsStr;
	String handicapStr;
	double reqFeeTemp;
	
	String reqFeeStr;
	
	int seatsTemp;
	
	String seatsStr;
	EditText eTxtStartPoint;
	EditText eTxtEndPoint;
	EditText eTxtDepTime;
	EditText eTxtDepDate;
	EditText eTxtSeats;
	EditText eTxtReqFee;
	CheckBox checkSmoke;
	CheckBox checkPets;
	CheckBox checkCrips;
	Button   timeInput;
	Button   dateInput;
    long 	 myID;

	HttpClient httpclient = new DefaultHttpClient();
  //  HttpPost httppost = new HttpPost("http://www.yoursite.com/script.php"); //set this up
    HttpPost httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/driverposts"); //set this up
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_offer);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 
		timeInput     = (Button) findViewById(R.id.timeInBtnPO);
		dateInput     = (Button) findViewById(R.id.dateInBtnPO);
		eTxtStartPoint = (EditText) findViewById(R.id.eTxtStartPointPO);
		eTxtEndPoint = (EditText) findViewById(R.id.eTxtEndPointPO);
		eTxtDepTime = (EditText) findViewById(R.id.timeTxtPO);
		eTxtDepDate = (EditText) findViewById(R.id.eTxtDepDatePO);
		eTxtSeats = (EditText) findViewById(R.id.eTxtSeatsPO);
		eTxtReqFee = (EditText) findViewById(R.id.eTxtReqFeePO);
		checkSmoke = (CheckBox) findViewById(R.id.checkPickupPO);
		checkPets = (CheckBox) findViewById(R.id.checkPetsPO);
		checkCrips = (CheckBox) findViewById(R.id.checkCripsPO);

		timeInput.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  showTimePickerDialog(v);
	    	  
	      }
		});
		dateInput.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {
		    	  showDatePickerDialog(v);
		      }
			});
		postBtn = (Button) findViewById(R.id.btnSubmitPO);
		postBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    /*	  startPointTemp = eTxtStartPoint.toString();
	    	  endPointTemp   = eTxtEndPoint.toString();
	    	  depTimeTemp    = eTxtDepTime.toString();
	    	  depDateTemp    = eTxtDepTime.toString();
	    	  

	    	  smokeStr     = checkSmoke.toString();
	    	  petsStr       = checkSmoke.toString();
	    	  handicapStr   = checkCrips.toString();
	    	  
	    	 
	    	  seatsStr       = (eTxtSeats.toString());
	    	  reqFeeStr      = (eTxtReqFee.toString());
	    	  
	    	  //*/
	    	  startPointTemp = eTxtStartPoint.getText().toString();
	    	  endPointTemp   = eTxtEndPoint.getText().toString();
	    	  depTimeTemp    = eTxtDepTime.getText().toString();
	    	  depDateTemp    = eTxtDepDate.getText().toString();
	    	  //depTimeTemp = "some bullshit time";
	    	  //depDateTemp = "whenever, man";
	    	  
	    	  
	    	  smokeStr     = checkSmoke.getText().toString();
	    	  petsStr       = checkPets.getText().toString();
	    	  handicapStr   = checkCrips.getText().toString();
	    	  
	    	  smokeTemp     = checkSmoke.isChecked();
	    	  petsTemp       = checkPets.isChecked();
	    	  cripTemp   = checkCrips.isChecked();
	    	 
	    	  seatsStr       = (eTxtSeats.getText().toString());
	    	  reqFeeStr      = (eTxtReqFee.getText().toString());
	    	  
	    	  
	    	  
	    	  //*/
	    	//  System.out.println("HEY HEY HEY LOOK HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  : '" + seatsStr + "' DONE");
		    	//  seatsTemp      = Integer.valueOf(eTxtSeats.toString());
		    	//  reqFeeTemp      = Double.valueOf(eTxtReqFee.toString());

	    	
	    	  
	    	//  seatsTemp = 3;
	    	// reqFeeTemp = 10000;
	    	/*  smokeTemp = false;
	    	  petsTemp   = false;
	    	  handicapTemp = false; //*/
	    	  
	    	  System.out.println("ABOUT TO CREATE A CPostOffer OBJECT!!!");
	    	  tempPostOffer = new CPostOffer(startPointTemp, endPointTemp, depTimeTemp, depDateTemp, smokeTemp, petsTemp, cripTemp, reqFeeStr, seatsStr);
	    	  System.out.println("JUST CREATED A CPostOffer OBJECT!!! niice.");

	    	  //public void submit(){
	    	  try {
  				   // Add your data
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   1111111111111111");
  		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(SIZE);
  		        System.out.println("the size of the 1ST arraylist is: " + listTemp.size());
  		        listTemp = tempPostOffer.submit();
  		        System.out.println("the size of the 2ND arraylist is: " + listTemp.size());
  		        
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   2222222222222222");
	    		  httppost.setEntity(new UrlEncodedFormEntity(listTemp));
	    		 
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   3333333333333333");

  		        // Execute HTTP Post Request
	    		

	  		      YoloRide appState = ((YoloRide)getApplicationContext());
	  		      HttpContext localContext = appState.getContext();
	  		      System.out.println("THE 	` STORE IN POST OFFER SAYS: " + localContext.getAttribute(ClientContext.COOKIE_STORE));
	  		    CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
	  		  Cookie chocolate = store.getCookies().get(0);
	  		httppost.setHeader("X-AUTH-TOKEN", chocolate.getValue());
  		       HttpResponse response = httpclient.execute(httppost, localContext); //, ctx);
  		       System.out.println(httppost.getAllHeaders());
  		       HttpEntity entity = response.getEntity();
		       Integer statusCode = response.getStatusLine().getStatusCode();
		       String responseText = null;
		       StringBuilder builder = new StringBuilder();
		       
		       InputStream content = entity.getContent();
		       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		       String line = reader.readLine();
		       builder.append(line);
		       try {
		    	 JSONObject jsonObject = new JSONObject(builder.toString());
		    	 Boolean postSuccess = jsonObject.getBoolean("success");
		    	myID = jsonObject.getLong("id");
		    	 
		    	 if(postSuccess == true) {
		    		 System.out.println("SUCCESS");
		    	 } else {
		    		 System.out.println("ERROR");
		    		 System.out.println("ERROR = " + jsonObject.getString("errorMessage"));
		    		 /*JSONArray errors = jsonObject.getJSONArray("errorMessage");
		    		 for(int i = 0; i < errors.length(); i++) {
		    			 System.out.println("Error " + i + " - " + errors.get(i));
		    		 } */
		    	 }
		    	 
		       } catch(Exception e) {
		    	   e.printStackTrace();
		       }
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   4444444444444444");
	    		  //System.out.println("HERE IS THE RESPONSE: " + response.getEntity().getContent().toString());
	    		  
	    		  
	    		  
	    		  
	    		  
	    		  
	  		    } catch (ClientProtocolException e) {
	  		        // TODO Auto-generated catch block
  		    } catch (IOException e) {
  		        // TODO Auto-generated catch block
  		    } //*/
  		
	   
	    	  //somehow submit this sheet
	    	  Intent i = new Intent();
	    	//  i.setClassName("com.yolo", "com.yolo.Home");
	    	  i.setClassName("com.yolo", "com.yolo.Search"); //deal wit it
				String startSearchStr = eTxtStartPoint.getText().toString();
				String endSearchStr   =   eTxtEndPoint.getText().toString();
				String offerReqStr   = "r"; 
				i.putExtra("start", startSearchStr);
				i.putExtra("end",     endSearchStr);
				i.putExtra("depTime", depTimeTemp);
				i.putExtra("depDate", depDateTemp);
				i.putExtra("pets",   petsTemp);
				i.putExtra("smoke",     smokeTemp);
				i.putExtra("crip",     cripTemp);//*/
				i.putExtra("o/r",     offerReqStr);
				i.putExtra("myID",      myID);
	    	  startActivity(i);
			}
	    }); //*/
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (Locale.getDefault().getDisplayLanguage().equals("Hindi"))
		{
			TextView starter = (TextView) findViewById(R.id.TextView03PO);
			starter.setText("से छोड़कर:");
			TextView dester = (TextView) findViewById(R.id.TextView02);
			dester.setText("गंतव्य:");
			TextView timer = (TextView) findViewById(R.id.TextView07);
			timer.setText("टाइम प्रस्थान:");
			TextView dater = (TextView) findViewById(R.id.TextView08);
			dater.setText("तारीख प्रस्थान:");
			TextView fee = (TextView) findViewById(R.id.TextView09);
			fee.setText("निवेदित शुल्क:");
			TextView seater = (TextView) findViewById(R.id.TextView05);
			seater.setText("उपलब्ध सीटें:");
			TextView smokin = (TextView) findViewById(R.id.TextView06);
			smokin.setText("धूम्रपान ठीक है:");
			TextView petter = (TextView) findViewById(R.id.TextView01);
			petter.setText("पालतू जानवर ठीक:");
			TextView cripper = (TextView) findViewById(R.id.TextView04);
			cripper.setText("सुलभ बाधा:");
		}
		else
		{
			TextView starter = (TextView) findViewById(R.id.TextView03PO);
			starter.setText("Leaving From:");
			TextView dester = (TextView) findViewById(R.id.TextView02);
			dester.setText("Destination:");
			TextView timer = (TextView) findViewById(R.id.TextView07);
			timer.setText("Departing time:");
			TextView dater = (TextView) findViewById(R.id.TextView08);
			dater.setText("Departure Date:");
			TextView fee = (TextView) findViewById(R.id.TextView09);
			fee.setText("Requested Fee:");
			TextView seater = (TextView) findViewById(R.id.TextView05);
			seater.setText("Seats Available:");
			TextView smokin = (TextView) findViewById(R.id.TextView06);
			smokin.setText("Smoking:");
			TextView petter = (TextView) findViewById(R.id.TextView01);
			petter.setText("Pets OK:");
			TextView cripper = (TextView) findViewById(R.id.TextView04);
			cripper.setText("Handicap Accessible");
		}
	}
	
	  public void showTimePickerDialog(View v) {
		    DialogFragment newFragment = new TimePickerFragment();
		    newFragment.show(getFragmentManager(), "timePicker");
		    
		}

	  public void showDatePickerDialog(View v) {
		    DialogFragment newFragment = new DatePickerFragment();
		    newFragment.show(getFragmentManager(), "datePicker");
		    
		}

		@Override
		public void onComplete(String time, String type) {
			if(type.equals("time"))
				eTxtDepTime.setText(time);
			else if(type.equals("date"))
				eTxtDepDate.setText(time);
		}
		
		/*
		@Override
		public void DatePickerFragment.onComplete(String time) {
			eTxtDepTime.setText(time);
		}
		/*/
}
