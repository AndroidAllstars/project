package com.yolo.fragments;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;


import com.yolo.CPostOffer;
import com.yolo.R;
import com.yolo.YoloRide;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
public class SearchPostOfferViewerFragment extends DialogFragment {
    int mNum;
    View v;
    static CPostOffer post;
    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    
    public SearchPostOfferViewerFragment(){
    	super();
    	
    }
    
    
    public static SearchPostOfferViewerFragment newInstance(CPostOffer postIn) {
    	SearchPostOfferViewerFragment f = new SearchPostOfferViewerFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
       // args.putString("title", "Title!!");
        f.setArguments(args);
        post = (CPostOffer) postIn;
        return f;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
       
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(style, theme);
        return new AlertDialog.Builder(getActivity())
                .setTitle(post.getPoster())// + getString())//.create();
                .setMessage(getString())
                .setNegativeButton(R.string.done, null)
                .setPositiveButton(R.string.apply,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        	///public void onRyan()
                        	{
                        	HttpClient httpclient = new DefaultHttpClient();
                        	HttpPost httppost;

                        	//post.setID(1);
                        	httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/driverposts/"+post.getID()+"/request"); //set this up

                        	YoloRide appState = ((YoloRide) getActivity().getApplicationContext());
                        	HttpContext localContext = appState.getContext();
                        	CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
                        	Cookie macadamia = store.getCookies().get(0);
                        	httppost.setHeader("X-AUTH-TOKEN", macadamia.getValue());

                        	try {
                        	HttpResponse response = httpclient.execute(httppost, localContext);
                        	} catch (ClientProtocolException e) {
                        	// TODO Auto-generated catch block
                        	e.printStackTrace();
                        	} catch (IOException e) {
                        	// TODO Auto-generated catch block
                        	e.printStackTrace();
                        	}

                        	}
                        	}
                    }
                )
                .create();
        
    }//*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Pick a style based on the num.
        
        /*
            case 1: style = DialogFragment.STYLE_NO_TITLE; break;
            case 2: style = DialogFragment.STYLE_NO_FRAME; break;
            case 3: style = DialogFragment.STYLE_NO_INPUT; break;
            case 4: style = DialogFragment.STYLE_NORMAL; break;
         //*/
        
    }
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	//CPostOffer postOffer =  post;  //don't do this in the long run.................
        v = inflater.inflate(R.layout.fragment_dialog, container, false);
        
        writeStuff();
  /*      Button button = (Button)v.findViewById(R.id.show);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // When button is clicked, call up to owning activity.
                ((FragmentDialog)getActivity()).showDialog();
            }
        });  //*/ 
    /*

        return v;
    }//*/
    private String getString(){
    	String someText = (
    				   "Leaving From:	    \t\t" + post.getStart() + 
        		"\n" + "Destination:		\t\t" + post.getEnd() + 
        		"\n" + "Departure Time:		\t\t" + post.getTime() + 
        		"\n" + "Departure Date:  	\t\t" + post.getDate() + 
        		"\n" + "Smoking:	        \t\t" + post.getPickup() + 
        		"\n" + "Pets OK:			\t\t" + post.getPets() + 
        		"\n" + "Handicap Accessible:\t\t" + post.getCrip() + 
        		"\n" + "Fee:				\t\t" + post.getFee() + 
        		"\n" + "Seats Available:	\t\t" + post.getSeats() 
        						);
    	return someText;
    	
    }
    
    
    private void writeStuff(){

        View tv = v.findViewById(R.id.text);
        ((TextView)tv).setText("Leaving From:		" + //post.getStart() + "\n" + 
        		"\n" + "Destination:		." + //post.getEnd() + "\n" + 
        		"\n" + "Seats Available:	." + //"dunno yet" + "\n" + 
        		"\n" + "Departure Time:		." + //post.getTime() + "\n" + 
        		"\n" + "Departure Date:  	." + //post.getDate() + "\n" + 
        		"\n" + "Pets OK:			." + //post.getPets() + "\n" + 
        		"\n" + "Handicap Accessible:." + //post.getCrip() + "\n" + 
        		"\n" + "Pickup Availible:	." //+ //post.getPickup()
        						);
        View tv2 = v.findViewById(R.id.text2);

        ((TextView)tv2).setText( post.getStart() + "\n" + 
        						post.getEnd() + "\n" + 
        					    "dunno yet" + "\n" + 
        					    post.getTime() + "\n" + 
        					    post.getDate() + "\n" + 
        					    post.getPets() + "\n" + 
        					    post.getCrip() + "\n" + 
        					    post.getPickup()
        						);
        
    	
    }
}