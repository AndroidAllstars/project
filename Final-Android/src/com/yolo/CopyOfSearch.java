package com.yolo;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.yolo.fragments.DatePickerFragment;
import com.yolo.fragments.HomePostViewerFragment;
import com.yolo.fragments.SearchPostOfferViewerFragment;
import com.yolo.fragments.TimePickerFragment;


import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
//import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.AdapterView;
//import android.widget.ExpandableListView;
//import android.widget.ImageButton;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class CopyOfSearch extends Activity implements TimePickerFragment.OnCompleteListener, DatePickerFragment.OnCompleteListener{
	EditText eTxtStartPoint;
	EditText eTxtEndPoint;
	EditText eTxtDepTime;
	EditText eTxtDepDate;
	final ArrayList<CPost> offers = new ArrayList<CPost>();
	
	final static int SIZE = CPost.getSize();
	Button 	 	 searchBtn;
	ToggleButton toggleOptionsSearch;
	LinearLayout linearLayoutSearch;
	ListView listSearch;
	String startPointStr;
	String	endPointStr;
	String  offerReqStr;
	String	depTime;
	String  depDate;
	Boolean  offerReqBool;
	Boolean  smokeBool;
	Boolean  petsBool;
	Boolean  cripsBool;
	CheckBox smokeCheck;
	CheckBox petsCheck;
	CheckBox cripCheck; 
	
	final Context ctx = this;
	@Override
	protected void onPause() {
		super.onPause();
		offers.clear();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 
	//	final Context ctx = this;
		Intent i = getIntent();
		eTxtStartPoint = (EditText) findViewById(R.id.eTxtStartPointSearch);
		eTxtEndPoint = (EditText) findViewById(R.id.eTxtEndPointSearch);
		eTxtDepTime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
		eTxtDepDate = (EditText) findViewById(R.id.eTxtDepDateSearch);
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupSearch);
		RadioButton radioOffer = (RadioButton) findViewById(R.id.radioOSearch);
		RadioButton radioReq = (RadioButton) findViewById(R.id.radioRSearch); //*/
		Button timeInput     = (Button) findViewById(R.id.timeInBtnSearch);
		Button dateInput     = (Button) findViewById(R.id.dateInBtnSearch);  //*/
		 smokeCheck    = (CheckBox) findViewById(R.id.checkSmokeSearch);
		 petsCheck     = (CheckBox) findViewById(R.id.checkPetSearch);  //*/
		 cripCheck     = (CheckBox) findViewById(R.id.checkCripSearch);  //*/
			startPointStr = i.getStringExtra("start");
			endPointStr   = i.getStringExtra("end");
			depTime = "";
			depDate = "";
			depTime 	= i.getStringExtra("depTime");
			depDate     = i.getStringExtra("depDate");
		offerReqStr = "r";
		offerReqStr  = i.getStringExtra("o/r");  
		
		timeInput.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  showTimePickerDialog(v);
	    	  
	      }
		});
		dateInput.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {
		    	  showDatePickerDialog(v);
		      }
			});
		//*/
		if(offerReqStr.equals("o"))
			{offerReqBool = true;
				radioGroup.check(R.id.radioOSearch);
			}
		else if(offerReqStr.equals("r"))
			{ offerReqBool = false; 
				System.out.println("Should be checked for R");
				radioGroup.check(R.id.radioRSearch);
			}
		else
			System.out.println("WHYYYYYYY does it say '" + offerReqStr + "'");


		eTxtStartPoint.setText(startPointStr);
		eTxtEndPoint.setText(endPointStr);//*/
		eTxtDepTime.setText(depTime);
		eTxtDepDate.setText(depDate);//*/
		
		linearLayoutSearch  = (LinearLayout) findViewById(R.id.linearLayoutSearch);
		listSearch          = (ListView) findViewById(R.id.listSearch);
		
		toggleOptionsSearch = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
		toggleOptionsSearch.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  options();
		  }
	    }); //*/
		
	    searchBtn = (Button) findViewById(R.id.btnSearch);
	    searchBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  search();
			}
	      
	      

	    });  //*/
	    
	 //   search();
	    options();

  	 //   final ListView listview = (ListView) findViewById(R.id.listSearch);
  	    //String[] values = new String[] { tempPostOffer.toString()};
	    
	    
  	    final StableArrayAdapter adapter = new StableArrayAdapter(ctx,
  	        android.R.layout.simple_list_item_1, offers);
  	    listSearch.setAdapter(adapter);
  	    /*
	    listSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	    	//  final String item = (String) parent.getItemAtPosition(position);
	      }

	    });//*/
	    
	    
	
	}


	  public void showTimePickerDialog(View v) {
		    DialogFragment newFragment = new TimePickerFragment();
		    newFragment.show(getFragmentManager(), "timePicker");
		    
		}

	  public void showDatePickerDialog(View v) {
		    DialogFragment newFragment = new DatePickerFragment();
		    newFragment.show(getFragmentManager(), "datePicker");
		    
		}

		@Override
		public void onComplete(String time, String type) {
			if(type.equals("time"))
				eTxtDepTime.setText(time);
			else if(type.equals("date"))
				eTxtDepDate.setText(time);
		}//*/
		/*
		  void showPostView(CPostOffer postIn) {
		        //mStackLevel++;

		        // DialogFragment.show() will take care of adding the fragment
		        // in a transaction.  We also want to remove any currently showing
		        // dialog, so make our own transaction and take care of that here.

			    DialogFragment newFragment =  SearchPostOfferViewerFragment.newInstance(postIn);
			    newFragment.show(getFragmentManager(), "dialogFrag");
			  
		    }
		  //*/
	  private class StableArrayAdapter extends ArrayAdapter<CPost> {

	    HashMap<CPost, Integer> mIdMap = new HashMap<CPost, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<CPost> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      CPost item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
      public void options() {

  		if(toggleOptionsSearch.isChecked())
  		{	
  			linearLayoutSearch.setVisibility(0);
  		}
  		else
  		{
  			linearLayoutSearch.setVisibility(8);
  		}
		
	  }
	  public void search() {
		  
			RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupSearch);
			RadioButton radioOffer = (RadioButton) findViewById(R.id.radioOSearch);
			RadioButton radioReq = (RadioButton) findViewById(R.id.radioRSearch); //*/
			offers.clear();
    	  //send stuff here
    	  HttpClient httpclient = new DefaultHttpClient();
    	  HttpPost httppost;
    	  CPostOffer searchPost;

    	  startPointStr = eTxtStartPoint.getText().toString();
    	  endPointStr   = eTxtEndPoint.getText().toString();
    	  depTime 		= eTxtDepTime.getText().toString();
    	  depDate	    = eTxtDepDate.getText().toString();
    	  //smokeBool = false;
    	  smokeBool 	= smokeCheck.isChecked();
    	  petsBool 		= petsCheck.isChecked();
    	  cripsBool 	= cripCheck.isChecked();//*/
    	  char checker;
    	  if(radioOffer.isChecked())
    	    {	
    		  	checker = 'o';
    		  	httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/driverposts/search"); //set this up
    	    	if(depTime.equals(null))
        	    	{searchPost =  new CPostOffer(startPointStr, endPointStr);
        	    		System.out.println("THE TIME IS NULLLL");
        	    	}
    	    	else if(depTime.equals(""))
    	    		{searchPost =  new CPostOffer(startPointStr, endPointStr);
	    				System.out.println("THE TIME IS THE EMPTY STRING!");
    	    		}
    	    	else
    	    		{ searchPost =  new CPostOffer(startPointStr, endPointStr, depTime, depDate, smokeBool, petsBool, cripsBool);
    					System.out.println("THE TIME IS NOT EMPTY!");
    	    			
    	    		}
    	    }
    	  else
      	    {	System.out.println("request =============");
    		  checker = 'r';
    		  httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/riderposts/search"); 
	    	//searchPost =  new CPostRequest(startPointStr, endPointStr);
	    	searchPost =  new CPostOffer(startPointStr, endPointStr);  		//THIS IS VERY WRONG

      	    }
    	   //public void submit(){
    	  try {
			   // Add your data
    		 // CPostOffer searchPostOffer = new CPostOffer(startPointStr, endPointStr);
    		  
	        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(SIZE);
	        listTemp =  searchPost.submit(1);
	       System.out.println("RAN THE SUBMIT FUNCTION ON : " + listTemp.get(1).toString());
    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
    		 YoloRide appState = ((YoloRide)getApplicationContext());
   	      HttpContext localContext = appState.getContext();
   	      CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
   	      Cookie peanutbutter = store.getCookies().get(0);
   	      httppost.setHeader("X-AUTH-TOKEN", peanutbutter.getValue());
	        // Execute HTTP Post Request
	       HttpResponse response = httpclient.execute(httppost, localContext);
			System.out.println("CREATED THE RESPONSE");
	       HttpEntity entity = response.getEntity();
	       Integer statusCode = response.getStatusLine().getStatusCode();
	       StringBuilder builder = new StringBuilder();
			System.out.println("STRING BUILT");
	       
	       InputStream content = entity.getContent();
	       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	       String line = reader.readLine();
	       builder.append(line);
	       try {
	    	   Translate translate = new Translate();
	    	    ArrayList<CPostOffer> tempo;
	    	    ArrayList<CPostRequest> tempr;
	    	   if (checker == 'o')
	    	   {
	    		   tempo = translate.action(new JSONObject(builder.toString()), new CPostOffer());
	    		   for (int k = 0; k < tempo.size(); k++)
	    		   {
	    			   offers.add(tempo.get(k));
	    		   }
	    	   }
	    	   else
	    	   {
	    		   tempr = translate.action(new JSONObject(builder.toString()), new CPostRequest("",""));
	    		   for (int k = 0; k < tempr.size(); k++)
	    		   {
	    			   offers.add(tempr.get(k));
	    		   }
	    	   }
				System.out.println("THERE ARE : " + offers.size() + " OFFERS");
				
	       } catch(Exception e) {
	    	   e.printStackTrace();
	       }
    		  //System.out.println("HERE IS THE RESPONSE - Status Code " + statusCode + " and Response text = " + builder.toString());
    		  
  		    } catch (ClientProtocolException e) {
  		        // TODO Auto-generated catch block
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    } //*/	  
    	  

	  }
	  @Override
	  protected void onResume()
	  {
		  super.onResume();
		  YoloRide appState = ((YoloRide)getApplicationContext());
		  boolean hindi = appState.getHindi();
		  if (hindi)
		  {
			  CheckBox smokess = (CheckBox) findViewById(R.id.checkSmokeSearch);
			  smokess.setText("à¤§à¥‚à¤®à¥à¤°à¤ªà¤¾à¤¨ à¤ à¥€à¤• à¤¹à¥ˆ:");
			  CheckBox petss = (CheckBox) findViewById(R.id.checkPetSearch);
			  petss.setText("à¤ à¥€à¤• à¤ªà¤¾à¤²à¤¤à¥‚ à¤œà¤¾à¤¨à¤µà¤°:");
			  CheckBox cripss = (CheckBox) findViewById(R.id.checkCripSearch);
			  cripss.setText("à¤¸à¥à¤²à¤­ à¤µà¤¿à¤•à¤²à¤¾à¤‚à¤—:");
			  Button searcher = (Button) findViewById(R.id.btnSearch);
			  searcher.setText("à¤–à¥‹à¤œ");
			  Button picktime = (Button) findViewById(R.id.timeInBtnSearch);
			  picktime.setText("à¤¸à¤®à¤¯ à¤²à¥‡à¤¨à¥‡");
			  Button pickdate = (Button) findViewById(R.id.dateInBtnSearch);
			  pickdate.setText("à¤¤à¤¾à¤°à¥€à¤– à¤²à¥‡à¤¨à¥‡");
			  ToggleButton moreops = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
			  moreops.setTextOn("à¤•à¤® à¤µà¤¿à¤•à¤²à¥à¤ª");
			  moreops.setTextOff("à¤…à¤§à¤¿à¤• à¤µà¤¿à¤•à¤²à¥à¤ª");
			  RadioButton offeron = (RadioButton) findViewById(R.id.radioOSearch);
			  offeron.setText("à¤ªà¥à¤°à¤¦à¤¾à¤¨ à¤•à¤°à¤¤à¤¾ à¤¹à¥ˆ");
			  RadioButton requeston = (RadioButton) findViewById(R.id.radioRSearch);
			  requeston.setText("à¤…à¤¨à¥à¤°à¥‹à¤§à¥‹à¤‚");
			  EditText starter = (EditText) findViewById(R.id.eTxtStartPointSearch);
			  starter.setHint("à¤¬à¤¿à¤‚à¤¦à¥ à¤ªà¥à¤°à¤¾à¤°à¤‚à¤­");
			  EditText ender = (EditText) findViewById(R.id.eTxtEndPointSearch);
			  ender.setHint("à¤—à¤‚à¤¤à¤µà¥à¤¯");
			  EditText dtime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
			  dtime.setHint("à¤ªà¥à¤°à¤¸à¥à¤¥à¤¾à¤¨ à¤•à¤¾ à¤¸à¤®à¤¯");
			  EditText ddate = (EditText) findViewById(R.id.eTxtDepDateSearch);
			  ddate.setHint("à¤ªà¥à¤°à¤¸à¥à¤¥à¤¾à¤¨ à¤¤à¤¿à¤¥à¤¿");
		  }
		  else
		  {
			  CheckBox smokess = (CheckBox) findViewById(R.id.checkSmokeSearch);
			  smokess.setText("Smoking");
			  CheckBox petss = (CheckBox) findViewById(R.id.checkPetSearch);
			  petss.setText("Pets OK");
			  CheckBox cripss = (CheckBox) findViewById(R.id.checkCripSearch);
			  cripss.setText("Handicap Accessible");
			  Button searcher = (Button) findViewById(R.id.btnSearch);
			  searcher.setText("Search");
			  Button picktime = (Button) findViewById(R.id.timeInBtnSearch);
			  picktime.setText("Pick Time");
			  Button pickdate = (Button) findViewById(R.id.dateInBtnSearch);
			  pickdate.setText("Pick Date");
			  ToggleButton moreops = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
			  moreops.setTextOn("Less Options");
			  moreops.setTextOff("More Options");
			  RadioButton offeron = (RadioButton) findViewById(R.id.radioOSearch);
			  offeron.setText("Offers");
			  RadioButton requeston = (RadioButton) findViewById(R.id.radioRSearch);
			  requeston.setText("Requests");
			  EditText starter = (EditText) findViewById(R.id.eTxtStartPointSearch);
			  starter.setHint("Enter Start Address");
			  EditText ender = (EditText) findViewById(R.id.eTxtEndPointSearch);
			  ender.setHint("Enter Destination");
			  EditText dtime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
			  dtime.setHint("Departure Time");
			  EditText ddate = (EditText) findViewById(R.id.eTxtDepDateSearch);
			  ddate.setHint("Departure Date");
		  }
	  }
	  
}