package com.yolo;

import static com.yolo.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.yolo.CommonUtilities.EXTRA_MESSAGE;
import static com.yolo.CommonUtilities.SENDER_ID;
import static com.yolo.CommonUtilities.SERVER_URL;
import static com.yolo.CommonUtilities.TAG;
import static com.yolo.CommonUtilities.displayMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	
	static String serverAuthToken;
	
    TextView mDisplay;
    AsyncTask<Void, Void, Void> mRegisterTask;
	
	// Kevin's Code
	private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();
	
	
	public static final String PREFS_NAME = "MyPrefsFile";
	private static final String PREF_USERNAME = "username";
	private static final String PREF_PASSWORD = "password";
	private static final String PREF_REMEMBER = "remember";
	String userIn;
	String passwordIn;

	EditText eTxtUser;
	EditText eTxtPassword;
	
	CheckBox remember;
	Button loginBtn;
	Button createBtn;
	Button forgotBtn;
	//*/
	@Override
	protected void onResume()
	  {
		  super.onResume();
		  if (Locale.getDefault().getDisplayLanguage().equals("Hindi"))
		  {
			  CheckBox remember = (CheckBox) findViewById(R.id.checkRememberLogin);
			  remember.setText("लॉगिन याद रखें");
			  Button accounter = (Button) findViewById(R.id.btnCreateLogin);
			  accounter.setText("खाता बनाएँ");
			  Button enter = (Button) findViewById(R.id.btnLogin);
			  enter.setText("लॉगिन");
			  EditText name = (EditText) findViewById(R.id.eTxtUserNameLogin);
			  name.setHint("उपयोगकर्ता नाम");
			  EditText pass = (EditText) findViewById(R.id.eTxtPasswordLogin);
			  pass.setHint("पासवर्ड");
		  }
		  else
		  {
			 
			  CheckBox remember = (CheckBox) findViewById(R.id.checkRememberLogin);
			  remember.setText("Remember login info");
			  Button accounter = (Button) findViewById(R.id.btnCreateLogin);
			  accounter.setText("Create Account");
			  Button enter = (Button) findViewById(R.id.btnLogin);
			  enter.setText("Log In");
			  EditText name = (EditText) findViewById(R.id.eTxtUserNameLogin);
			  name.setHint("User Name");
			  EditText pass = (EditText) findViewById(R.id.eTxtPasswordLogin);
			  pass.setHint("Password");
		  }
	  }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 

		final Context ctx = this;
		eTxtUser 		= (EditText) findViewById(R.id.eTxtUserNameLogin);
		eTxtPassword 	= (EditText) findViewById(R.id.eTxtPasswordLogin);
		remember 		= (CheckBox) findViewById(R.id.checkRememberLogin);
		loginBtn 		= (Button) findViewById(R.id.btnLogin);
		forgotBtn 		= (Button) findViewById(R.id.btnForgotLogin);
		createBtn 		= (Button) findViewById(R.id.btnCreateLogin);
		//do something with these
		SharedPreferences pref = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);   
		String username = pref.getString(PREF_USERNAME, null);
		String password = pref.getString(PREF_PASSWORD, null);
		boolean remembered = pref.getBoolean(PREF_REMEMBER, false);
		if (remembered == true)
		{
				remember.setChecked(true);
				if (username != null && password != null) 
				{
					eTxtUser.setText(username);
					eTxtPassword.setText(password);
				}
		}
		else
		{
				remember.setChecked(false);
		}

		loginBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {

	  		userIn = eTxtUser.getText().toString();
	  		passwordIn = eTxtPassword.getText().toString();
	  		if(remember.isChecked())
	  		{
	  			getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
	  	        .edit()
	  	        .putString(PREF_USERNAME, userIn)
	  	        .putString(PREF_PASSWORD, passwordIn)
	  	        .putBoolean(PREF_REMEMBER, true)
	  	        .commit();
	  		}
	  		else
	  		{
	  			getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
	  	        .edit()
	  	        .putString(PREF_USERNAME, null)
	  	        .putString(PREF_PASSWORD, null)
	  	        .putBoolean(PREF_REMEMBER, false)
	  	        .commit();
	  		}
	  		CUser user = new CUser(userIn, passwordIn);
	    	  HttpClient httpclient = new DefaultHttpClient();
	    	    HttpPost httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/login"); //set this up

	    	   //public void submit(){
	    	  try {
  				   // Add your data
  		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
  		        listTemp =  user.submit();
	    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
	    		 

  		        // Execute HTTP Post Request
  		       HttpResponse response = httpclient.execute(httppost);
  		       HttpEntity entity = response.getEntity();
  		       Integer statusCode = response.getStatusLine().getStatusCode();
  		       String responseText = null;
  		       StringBuilder builder = new StringBuilder();
  		       
  		       InputStream content = entity.getContent();
  		       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
  		       String line = reader.readLine();
  		       builder.append(line);
  		       
  		       try {
  		    	 JSONObject jsonObject = new JSONObject(builder.toString());
  		    	 Boolean loginSuccess = jsonObject.getBoolean("success");
  		    	 String authToken = jsonObject.getString("authToken");
  		    	 System.out.println("THE AUTHTOKEN IS: " + authToken);//*/
  		    	 
  		    	 serverAuthToken = authToken;

  		      // Create a local instance of cookie store
  		    	 Cookie cookie = new BasicClientCookie("X-AUTH-TOKEN", authToken);
  		    	 //cookie.setValue(authToken);
  		      CookieStore cookieStore =  new BasicCookieStore();
  		      cookieStore.addCookie(cookie);
  		      // Create local HTTP context
  		      // Bind custom cookie store to the local context
  		      HttpContext localContext = new BasicHttpContext();
  		      localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

  		      YoloRide appState = ((YoloRide)getApplicationContext());
  		      appState.setContext(localContext);
  		      
  		      
  		      //HttpGet httpget = new HttpGet("http://www.google.com/");
  		      
  		    	 if(loginSuccess == true) {
  		    		 
  		    		
  		           checkNotNull(SERVER_URL, "SERVER_URL");
  		         checkNotNull(SENDER_ID, "SENDER_ID");
  		         // Make sure the device has the proper dependencies.
  		         GCMRegistrar.checkDevice(ctx);
  		         // Make sure the manifest was properly set - comment out this line
  		         // while developing the app, then uncomment it when it's ready.
  		         GCMRegistrar.checkManifest(ctx);
  		         setContentView(R.layout.main);
  		         mDisplay = (TextView) findViewById(R.id.display);
  		         registerReceiver(mHandleMessageReceiver,
  		                 new IntentFilter(DISPLAY_MESSAGE_ACTION));
  		         final String regId = GCMRegistrar.getRegistrationId(ctx);
  		         
  		         System.out.println("REGGGGGGG IS = " + regId);
  		         System.out.println("REGGGGGGG IS = " + regId);
  		         System.out.println("REGGGGGGG IS = " + regId);
  		         
  		         if (regId.equals("")) {
  		             // Automatically registers application on startup.
  		        	 
  	  		         final String regId2 = GCMRegistrar.getRegistrationId(ctx);
  		        	 
  		             GCMRegistrar.register(ctx, SENDER_ID);
  		         } else {
  		        	 
  	  		         final String regId2 = GCMRegistrar.getRegistrationId(ctx);
  		        	 
  		             // Device is already registered on GCM, check server.
  		             if (GCMRegistrar.isRegisteredOnServer(ctx)) 
  		             
  		             	{ // START ASYNC
  		             	
  		                 // Skips registration.
  		                 // mDisplay.append(getString(R.string.already_registered) + "\n");
  		             
  		             	// KEVIN THAI SAYS REGISTER NO MATTER WHAT!
  		                 // Try to register again, but not in the UI thread.
  		                 // It's also necessary to cancel the thread onDestroy(),
  		                 // hence the use of AsyncTask instead of a raw thread.
  		                 final Context context = ctx;
  		                 mRegisterTask = new AsyncTask<Void, Void, Void>() {
  		                	 
  		                	 
  		                     @Override
  		                     protected Void doInBackground(Void... params) {
  		                         boolean registered;

  		                    	 if(regId.equals(""))
  		                    	 {
  		                         registered =
  		                                 register(context, regId2);
  		                    	 }
  		                    	 else
  		                    	 {
  		                    	 registered =
  	  		                             register(context, regId);
  		                    	 }
  		                         
  		                         if (!registered) {
  		                             GCMRegistrar.unregister(context);
  		                         }
  		                         return null;
  		                     }

  		                     @Override
  		                     protected void onPostExecute(Void result) {
  		                         mRegisterTask = null;
  		                     }

  		                 };
  		                 mRegisterTask.execute(null, null, null);
  		             } // END ASYNC
  		             
  		             else {
  		                 // Try to register again, but not in the UI thread.
  		                 // It's also necessary to cancel the thread onDestroy(),
  		                 // hence the use of AsyncTask instead of a raw thread.
  		                 final Context context = ctx;
  		                 mRegisterTask = new AsyncTask<Void, Void, Void>() {

  		                     @Override
  		                     protected Void doInBackground(Void... params) {
  		                         boolean registered =
  		                                 register(context, regId);
  		                         // At this point all attempts to register with the app
  		                         // server failed, so we need to unregister the device
  		                         // from GCM - the app will try to register again when
  		                         // it is restarted. Note that GCM will send an
  		                         // unregistered callback upon completion, but
  		                         // GCMIntentService.onUnregistered() will ignore it.
  		                         if (!registered) {
  		                             GCMRegistrar.unregister(context);
  		                         }
  		                         return null;
  		                     }

  		                     @Override
  		                     protected void onPostExecute(Void result) {
  		                         mRegisterTask = null;
  		                     }

  		                 };
  		                 mRegisterTask.execute(null, null, null);
  		             }
  		         }
  		    		 
  		    		 
  		    		 
  		    		 
  		    		 
  		    		 
  					Intent i = new Intent();
  					i.setClassName("com.yolo", "com.yolo.Home");
  					startActivity(i);
  		    	 } else {
  		    		Toast toast = Toast.makeText(ctx, "Wrong password", Toast.LENGTH_LONG);
  		    		toast.show();

  		    	 }
  		       } catch(Exception e) {
  		    	   e.printStackTrace();
  		       }
  		       
	    		  System.out.println("HERE IS THE RESPONSE - Status Code " + statusCode + " and Response text = " + builder.toString());
	    		  
	  		    } catch (ClientProtocolException e) {
	  		        // TODO Auto-generated catch block
  		    } catch (IOException e) {
  		        // TODO Auto-generated catch block
  		    } //*/
	    	  
	    	  //do something with those
	  		
			}
	    }); //*/

		createBtn.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {

			  		userIn = eTxtUser.getText().toString();
			  		passwordIn = eTxtPassword.getText().toString();
			    	  //do something with those maybe?
			  		
				Intent i = new Intent();
				i.setClassName("com.yolo", "com.yolo.NewUser");
				startActivity(i);
				}
		    }); //*/
	
	forgotBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {

		  /*		
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.Home");
			startActivity(i);//*/
			}
	    }); //*/
}

	
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}  //*/

	/**
     * Register this account/device pair within the server.
     *
     * @return whether the registration succeeded or not.
     */
    static boolean register(final Context context, final String regId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL; // + "/register";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register it in the
        // demo server. As the server might be down, we will retry it a couple
        // times.
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            Log.d(TAG, "Attempt #" + i + " to register");
            try {
                displayMessage(context, context.getString(
                        R.string.server_registering, i, MAX_ATTEMPTS));
                post(serverUrl, params);
                GCMRegistrar.setRegisteredOnServer(context, true);
                String message = context.getString(R.string.server_registered);
                CommonUtilities.displayMessage(context, message);
                return true;
            } catch (IOException e) {
                // Here we are simplifying and retrying on any error; in a real
                // application, it should retry only on unrecoverable errors
                // (like HTTP error code 503).
                Log.e(TAG, "Failed to register on attempt " + i, e);
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Log.d(TAG, "Sleeping for " + backoff + " ms before retry");
                    Thread.sleep(backoff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Log.d(TAG, "Thread interrupted: abort remaining retries!");
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backoff *= 2;
            }
        }
        String message = context.getString(R.string.server_register_error,
                MAX_ATTEMPTS);
        CommonUtilities.displayMessage(context, message);
        return false;
    }

    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
            String message = context.getString(R.string.server_unregistered);
            CommonUtilities.displayMessage(context, message);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = context.getString(R.string.server_unregister_error,
                    e.getMessage());
            CommonUtilities.displayMessage(context, message);
        }
    }

    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(String endpoint, Map<String, String> params)
            throws IOException {
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) url.openConnection();
            
            // Kevin's code
            //YoloRide appState = ((YoloRide) getActivity().getApplicationContext());
     	    //HttpContext localContext = appState.getContext();
     	    //CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
     	    //Cookie peanutbutter = store.getCookies().get(0);
                        
            
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            
            // Kevin's code
            conn.addRequestProperty("X-AUTH-TOKEN", serverAuthToken);

            
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
      }
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            /*
             * Typically, an application registers automatically, so options
             * below are disabled. Uncomment them if you want to manually
             * register or unregister the device (you will also need to
             * uncomment the equivalent options on options_menu.xml).
             */
            /*
            case R.id.options_register:
                GCMRegistrar.register(this, SENDER_ID);
                return true;
            case R.id.options_unregister:
                GCMRegistrar.unregister(this);
                return true;
             */
            case R.id.options_clear:
                mDisplay.setText(null);
                return true;
            case R.id.options_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        unregisterReceiver(mHandleMessageReceiver);
        GCMRegistrar.onDestroy(this);
        super.onDestroy();
    }

    private void checkNotNull(Object reference, String name) {
        if (reference == null) {
            throw new NullPointerException(
                    getString(R.string.error_config, name));
        }
    }

    private final BroadcastReceiver mHandleMessageReceiver =
            new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            mDisplay.append(newMessage + "\n");
        }
    };
}
