# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table base_feedback (
  id                        bigint not null,
  comments                  varchar(255),
  constraint pk_base_feedback primary key (id))
;

create table base_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_date_time             timestamp,
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  owner_uname               varchar(255),
  constraint pk_base_post primary key (id))
;

create table driver_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_date_time             timestamp,
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  owner_uname               varchar(255),
  max_seats                 integer,
  seats_open                integer,
  req_fee                   float,
  escrow                    float,
  constraint pk_driver_post primary key (id))
;

create table offer_feedback (
  id                        bigint not null,
  comments                  varchar(255),
  rater_uname               varchar(255),
  ratee_uname               varchar(255),
  post_id                   bigint,
  successful_ride           boolean,
  would_ride_again          boolean,
  rating_of_driver          integer,
  rating_of_car             integer,
  constraint pk_offer_feedback primary key (id))
;

create table payment (
  id                        bigint not null,
  amount                    float,
  rider_uname               varchar(255),
  driver_uname              varchar(255),
  post_id                   bigint,
  constraint pk_payment primary key (id))
;

create table rider_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_date_time             timestamp,
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  owner_uname               varchar(255),
  constraint pk_rider_post primary key (id))
;

create table yruser (
  uname                     varchar(255) not null,
  sha_password              bytea not null,
  auth_token                varchar(255),
  name                      varchar(255),
  phone                     varchar(255),
  address                   varchar(255),
  license                   varchar(255),
  plates                    varchar(255),
  car                       varchar(255),
  registration_id           varchar(255),
  constraint pk_yruser primary key (uname))
;


create table driver_post_riders (
  driver_post_id                 bigint not null,
  yruser_uname                   varchar(255) not null,
  constraint pk_driver_post_riders primary key (driver_post_id, yruser_uname))
;

create table driver_post_req (
  driver_post_id                 bigint not null,
  yruser_uname                   varchar(255) not null,
  constraint pk_driver_post_req primary key (driver_post_id, yruser_uname))
;

create table rider_post_yruser (
  rider_post_id                  bigint not null,
  yruser_uname                   varchar(255) not null,
  constraint pk_rider_post_yruser primary key (rider_post_id, yruser_uname))
;
create sequence base_feedback_seq;

create sequence base_post_seq;

create sequence driver_post_seq;

create sequence offer_feedback_seq;

create sequence payment_seq;

create sequence rider_post_seq;

create sequence yruser_seq;

alter table base_post add constraint fk_base_post_owner_1 foreign key (owner_uname) references yruser (uname);
create index ix_base_post_owner_1 on base_post (owner_uname);
alter table driver_post add constraint fk_driver_post_owner_2 foreign key (owner_uname) references yruser (uname);
create index ix_driver_post_owner_2 on driver_post (owner_uname);
alter table offer_feedback add constraint fk_offer_feedback_rater_3 foreign key (rater_uname) references yruser (uname);
create index ix_offer_feedback_rater_3 on offer_feedback (rater_uname);
alter table offer_feedback add constraint fk_offer_feedback_ratee_4 foreign key (ratee_uname) references yruser (uname);
create index ix_offer_feedback_ratee_4 on offer_feedback (ratee_uname);
alter table offer_feedback add constraint fk_offer_feedback_post_5 foreign key (post_id) references driver_post (id);
create index ix_offer_feedback_post_5 on offer_feedback (post_id);
alter table payment add constraint fk_payment_rider_6 foreign key (rider_uname) references yruser (uname);
create index ix_payment_rider_6 on payment (rider_uname);
alter table payment add constraint fk_payment_driver_7 foreign key (driver_uname) references yruser (uname);
create index ix_payment_driver_7 on payment (driver_uname);
alter table payment add constraint fk_payment_post_8 foreign key (post_id) references driver_post (id);
create index ix_payment_post_8 on payment (post_id);
alter table rider_post add constraint fk_rider_post_owner_9 foreign key (owner_uname) references yruser (uname);
create index ix_rider_post_owner_9 on rider_post (owner_uname);



alter table driver_post_riders add constraint fk_driver_post_riders_driver__01 foreign key (driver_post_id) references driver_post (id);

alter table driver_post_riders add constraint fk_driver_post_riders_yruser_02 foreign key (yruser_uname) references yruser (uname);

alter table driver_post_req add constraint fk_driver_post_req_driver_pos_01 foreign key (driver_post_id) references driver_post (id);

alter table driver_post_req add constraint fk_driver_post_req_yruser_02 foreign key (yruser_uname) references yruser (uname);

alter table rider_post_yruser add constraint fk_rider_post_yruser_rider_po_01 foreign key (rider_post_id) references rider_post (id);

alter table rider_post_yruser add constraint fk_rider_post_yruser_yruser_02 foreign key (yruser_uname) references yruser (uname);

# --- !Downs

drop table if exists base_feedback cascade;

drop table if exists base_post cascade;

drop table if exists driver_post cascade;

drop table if exists driver_post_riders cascade;

drop table if exists driver_post_req cascade;

drop table if exists offer_feedback cascade;

drop table if exists payment cascade;

drop table if exists rider_post cascade;

drop table if exists rider_post_yruser cascade;

drop table if exists yruser cascade;

drop sequence if exists base_feedback_seq;

drop sequence if exists base_post_seq;

drop sequence if exists driver_post_seq;

drop sequence if exists offer_feedback_seq;

drop sequence if exists payment_seq;

drop sequence if exists rider_post_seq;

drop sequence if exists yruser_seq;

