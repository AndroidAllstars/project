package models;

import java.util.List;

import javax.persistence.*;

import org.joda.time.DateTime;

import play.db.ebean.Model.Finder;

@Entity
//@Table(name="DPOST")
public class DriverPost extends BasePost {
	
	private Integer maxSeats;
	private Integer seatsOpen;
	private Integer reqFee;
	
	public static Finder<Long,DriverPost> findDriverPost = new Finder(
	   Long.class, DriverPost.class
	);

	public static List<DriverPost> allDriverPost() {
	   return findDriverPost.all();
	}
	
	public static List<DriverPost> searchByAddresses(String startAddress, String endAddress) {
		return findDriverPost.where()
				.eq("startPoint", startAddress)
				.eq("endPoint", endAddress)
				.orderBy("postedDate desc")
				.findList();
	}
		 
	public static void driverCreate(DriverPost driverPost) { 
	    //driverPost.startDate = DateTime.now();
	    //driverPost.endDate = null;
	    driverPost.setPostedDate(DateTime.now());
	    driverPost.save();
	}
	
	public static Boolean deleteDriverPost(Long id) {
		if(findDriverPost.where().eq("id", id).findList().size() < 1) {
			return false;
		}
		findDriverPost.ref(id).delete();
		return true;
	}
	
	public Integer getMaxSeats() {
		return maxSeats;
	}

	public void setMaxSeats(Integer maxSeats) {
		this.maxSeats = maxSeats;
	}

	public Integer getReqFee() {
		return reqFee;
	}

	public void setReqFee(Integer reqFee) {
		this.reqFee = reqFee;
	}

	public Integer getSeatsOpen() {
		return seatsOpen;
	}

	public void setSeatsOpen(Integer seatsOpen) {
		this.seatsOpen = seatsOpen;
	}
	

}
