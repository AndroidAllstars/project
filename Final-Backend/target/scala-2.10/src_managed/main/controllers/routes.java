// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:40711fa2b91ede6446d97298b17c9a57afbf599a
// @DATE:Tue Jun 04 21:46:55 PDT 2013

package controllers;

public class routes {
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static final controllers.ReverseSecurityController SecurityController = new controllers.ReverseSecurityController();
public static final controllers.ReverseTestController TestController = new controllers.ReverseTestController();
public static final controllers.ReverseOfferController OfferController = new controllers.ReverseOfferController();
public static final controllers.ReverseUserController UserController = new controllers.ReverseUserController();
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseSearchController SearchController = new controllers.ReverseSearchController();
public static final controllers.ReverseNotificationController NotificationController = new controllers.ReverseNotificationController();
public static class javascript {
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();
public static final controllers.javascript.ReverseSecurityController SecurityController = new controllers.javascript.ReverseSecurityController();
public static final controllers.javascript.ReverseTestController TestController = new controllers.javascript.ReverseTestController();
public static final controllers.javascript.ReverseOfferController OfferController = new controllers.javascript.ReverseOfferController();
public static final controllers.javascript.ReverseUserController UserController = new controllers.javascript.ReverseUserController();
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseSearchController SearchController = new controllers.javascript.ReverseSearchController();
public static final controllers.javascript.ReverseNotificationController NotificationController = new controllers.javascript.ReverseNotificationController();    
}   
public static class ref {
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();
public static final controllers.ref.ReverseSecurityController SecurityController = new controllers.ref.ReverseSecurityController();
public static final controllers.ref.ReverseTestController TestController = new controllers.ref.ReverseTestController();
public static final controllers.ref.ReverseOfferController OfferController = new controllers.ref.ReverseOfferController();
public static final controllers.ref.ReverseUserController UserController = new controllers.ref.ReverseUserController();
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseSearchController SearchController = new controllers.ref.ReverseSearchController();
public static final controllers.ref.ReverseNotificationController NotificationController = new controllers.ref.ReverseNotificationController();    
} 
}
              