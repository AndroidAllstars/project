package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.joda.time.DateTime;

import com.avaje.ebean.ExpressionList;

import play.db.ebean.Model.Finder;

@Entity
//@Table(name="RPOST")
public class RiderPost extends BasePost {
	
	@ManyToMany
	@JsonIgnore
	public Set<YRUser> offeringDrivers = new HashSet<YRUser>();
	
	public Set<String> getOfferingDriversList() {
		Set<String> offeringDriversList = new HashSet<String>(offeringDrivers.size());
		for(YRUser driver : offeringDrivers) {
			offeringDriversList.add(driver.getUname());
		}
		return offeringDriversList;
	} 
	
	public static Finder<Long,RiderPost> findRiderPost = new Finder(
		Long.class, RiderPost.class
	);
	
	public static List<RiderPost> allRiderPost() {
		return findRiderPost.all();
	}

	public static void riderPostCreate(RiderPost riderPost) { 
		riderPost.setPostedDate(DateTime.now());
	    riderPost.save();
	  }
	
	public static void deleteRiderPost(Long id) {
		RiderPost.findRiderPost.ref(id).delete();
	}
	
	public static List<RiderPost> searchRiderPost(String startPoint, String endPoint,
			DateTime reqDateTime, String pets, String handicap, String pickup, String reqFee) {
		ExpressionList<RiderPost> search = findRiderPost.where().eq("endPoint", endPoint);
		
		if(startPoint != null) search = search.eq("startPoint", startPoint);
		if(reqDateTime != null) search = search.between("depDateTime", reqDateTime, reqDateTime.plusMonths(1));
		if(pets != null) search = search.eq("pets", Boolean.parseBoolean(pets));
		if(handicap != null) search = search.eq("handicap", Boolean.parseBoolean(handicap));
		if(pickup != null) search = search.eq("pickup", Boolean.parseBoolean(pickup));
		if(reqFee != null) search = search.between("reqFee", 0, Double.parseDouble(reqFee));
		
		return search.orderBy("depDateTime asc").findList();
		
	}
}
