package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.*;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.OpenID;
import play.libs.OpenID.UserInfo;
import play.mvc.*;
import play.mvc.Http.RequestBody;
import play.data.*;

import views.html.*;
import models.*;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import org.mindrot.jbcrypt.BCrypt;

public class Application extends Controller {
 
	static Form<BasePost> basePostForm = Form.form(BasePost.class);
	static Form<RiderPost> riderPostForm = Form.form(RiderPost.class);
	static Form<DriverPost> driverPostForm = Form.form(DriverPost.class);
    static Form<Ride> rideForm = Form.form(Ride.class);
    static Form<YRUser> userForm = Form.form(YRUser.class);
  
    public static Result index() {
      return redirect(routes.Application.posts());
    }
 
    public static Result posts() {
      return ok(views.html.index.render(BasePost.allBasePost(), basePostForm));
    }
    
    public static Result driverPosts() {
      return ok(views.html.driverposts.render(DriverPost.allDriverPost(), driverPostForm));
    }
    
    public static Result driverPostsJson() {
    	ObjectNode result = Json.newObject();
    	result.put("numberOfResults", DriverPost.allDriverPost().size());
    	result.put("results", Json.toJson(DriverPost.allDriverPost()));
    	return ok(result);
    }
  
    public static Result newBasePost() {
      Form<BasePost> filledForm = basePostForm.bindFromRequest();
      if(filledForm.hasErrors()) {
        return badRequest(views.html.index.render(BasePost.allBasePost(), filledForm));
      }
      else {
        BasePost.create(filledForm.get());
        return redirect(routes.Application.posts());
      }
    }
    
    
    public static Result newDriverPost() {
    	ObjectNode result = Json.newObject();
    	Form<DriverPost> filledForm = driverPostForm.bindFromRequest();
    	if(filledForm.hasErrors()) {
    		result.put("success", false);
    		result.put("error", true);
    		result.put("errorMessage", filledForm.errorsAsJson());
    		//return badRequest(views.html.driverposts.render(DriverPost.allDriverPost(), filledForm));
    		return badRequest(result);
    	} else {
    		DriverPost.driverCreate(filledForm.get());
    		result.put("success",true);
    		return ok(result);
    	}
    }
    
    /*public static Result newRiderPost() {
    	Form<RiderPost> filledForm = riderPostForm.bindFromRequest();
    	if(filledForm.hasErrors()) {
    		return badRequest(views.html.index.render(RiderPost.all(), filledForm));
    	}
    }*/

    public static Result deletePost(Long id) {
      BasePost.delete(id);
      return redirect(routes.Application.posts());
    }
    
    public static Result deleteDriverPost(Long id) {
    	ObjectNode result = Json.newObject();
    	if(DriverPost.deleteDriverPost(id) == false) {
    		result.put("success", false);
    		result.put("error", true);
    		result.put("errorMessage", "Driver post of id " + id + " does not exist");
    		return ok(result);
    	}
    	result.put("success", true);
    	return ok(result);
    }
    
    public static Result search() {
    	
    	/* Only searches for start and end address so far */
    	
    	DynamicForm searchParams = DynamicForm.form().bindFromRequest();
    	ObjectNode result = Json.newObject();
    	//JsonNode parameters = Json.toJson(searchParams.data());
    	
    	if(searchParams.get("startPoint") == null || searchParams.get("endPoint") == null) {
    		result.put("success", false);
    		result.put("error", true);
    		result.put("errorMessage", "Either your start or end address is missing");
    		return ok(result);
    	}
    	
    	List<DriverPost> queryResults = DriverPost.searchByAddresses(searchParams.get("startPoint"), searchParams.get("endPoint"));
    	
    	result.put("success", true);
    	result.put("numberOfResults", queryResults.size());
    	result.put("results", Json.toJson(queryResults));
    	return ok(result);
    }
    
    public static Result users() {
    	ObjectNode result = Json.newObject();
    	
    	result.put("success", true);
    	result.put("numberOfResults", YRUser.findAllUsers().size());
    	result.put("results", Json.toJson(YRUser.findAllUsers()));
    	return ok(result);
    }
    
    public static Result registerUser() {
    	ObjectNode result = Json.newObject();
    	Form<YRUser> filledForm = userForm.bindFromRequest();
    	YRUser user = filledForm.get();
    	
    	if(YRUser.getYRUser(user.getUname()) != null) {
    		result.put("success", false);
    		result.put("errorMessage", "User with that username already exists.");
    		return ok(result);
    	}
    	
    	String hashed = BCrypt.hashpw(user.getPass(), BCrypt.gensalt());
    	user.setPass(hashed);
    	YRUser.create(user);
    	result.put("success", true);
    	result.put("user", Json.toJson(user));
    	
    	return ok(result);
    }
    
    public static Result loginUser() {
    	ObjectNode result = Json.newObject();
    	DynamicForm form = null;
    
    	if(DynamicForm.form() != null) {
    		form = DynamicForm.form().bindFromRequest();
    	} else {
    		result.put("success", false);
    		return badRequest(result);
    	}
    	
    	if(form.get("uname") == null || form.get("pass") == null) {
    		result.put("success", false);
    		return badRequest(result);
    	}
    	
    	String username = form.get("uname");
    	String password = form.get("pass");
    	
    	YRUser user = YRUser.getYRUser(username);
    	if(BCrypt.checkpw(password, user.getPass())) {
    		result.put("success", true);
    		return ok(result);
    	} else {
    		result.put("success", false);
    		return badRequest(result);
    	}
    }
    
    @SuppressWarnings("serial")
    public static final Map<String, String> identifiers = new HashMap<String, String>() {
    {
    put("google", "https://www.google.com/accounts/o8/id");
    }
    };
     
    public static Result auth() {
    Logger.debug("authenticate");
    String providerId = "google";
    String providerUrl = identifiers.get(providerId);
    String returnToUrl = "http://localhost:9000/login/verify";
     
    if (providerUrl == null) {
    return badRequest("Could not find provider " + providerId);
    }
     
    Map<String, String> attributes = new HashMap<String, String>();
    attributes.put("Email", "http://schema.openid.net/contact/email");
    attributes.put("FirstName", "http://schema.openid.net/namePerson/first");
    attributes.put("LastName", "http://schema.openid.net/namePerson/last");
     
    Promise<String> redirectUrl = OpenID.redirectURL(providerUrl, returnToUrl, attributes);
    return redirect(redirectUrl.get());
    }
     
    public static Result verify() {
    Logger.debug("verifyLogin");
    Promise<UserInfo> userInfoPromise = OpenID.verifiedId();
    UserInfo userInfo = userInfoPromise.get();
    JsonNode json = Json.toJson(userInfo);
    return ok(json);
    }
}
