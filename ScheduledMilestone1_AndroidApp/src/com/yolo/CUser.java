package com.yolo;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CUser
{
	public String uname;
	public String pass;
	public String name;
	public String phone;
	public String address;
	public String license;
	public String car;
	public String plates;
	public CUser(String tuname, String tpass, String tname, String tphone, String taddress, String tlicense, String tcar, String tplates)
	{
		uname = tuname;
		pass = tpass;
		name = tname;
		phone = tphone;
		address = taddress;
		license = tlicense;
		car = tcar;
		plates = tplates;
	}
		public CUser(String tuname, String tpass) 
		{
			uname = tuname;
			pass = tpass;

			name = "";
			phone = "";
			address = "";
			license = "";
			car = "";
			plates = "";
		}
		public CUser(String tuname, String tpass, String tname, String tphone, String taddress)
	{
		uname = tuname;
		pass = tpass;
		name = tname;
		phone = tphone;
		address = taddress;
		license = "";
		car = "";
		plates = "";
	}

	List<NameValuePair> submit() {
		List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
		listTemp.add(new BasicNameValuePair("class", "CUser"));
		listTemp.add(new BasicNameValuePair("uname", uname));
		listTemp.add(new BasicNameValuePair("pass", pass)); 
		listTemp.add(new BasicNameValuePair("name", name)); 
		listTemp.add(new BasicNameValuePair("phone", phone)); 
		listTemp.add(new BasicNameValuePair("address", address)); 
		listTemp.add(new BasicNameValuePair("license", license)); 
		listTemp.add(new BasicNameValuePair("car", car)); 
		listTemp.add(new BasicNameValuePair("plates", plates));
		return listTemp;
	}
	
}
