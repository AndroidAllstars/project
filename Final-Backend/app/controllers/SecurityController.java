package controllers;

import models.YRUser;
import org.codehaus.jackson.node.ObjectNode;
import play.Logger;
import play.data.Form;
import play.data.validation.Constraints;
import play.libs.Json;
import play.mvc.*;

import static play.libs.Json.toJson;
import static play.mvc.Controller.request;
import static play.mvc.Controller.response;

public class SecurityController extends Action.Simple {

    public final static String AUTH_TOKEN_HEADER = "X-Auth-Token";
    public static final String AUTH_TOKEN = "authToken";

    public Result call(Http.Context ctx) throws Throwable {
        YRUser user = null;
        Logger.debug("length of headers = " + String.valueOf(ctx.request().headers().size()));
        for(String key : ctx.request().headers().keySet()) {
        	Logger.debug("key = " + key);
        }
        String[] authTokenHeaderValues = ctx.request().headers().get(AUTH_TOKEN_HEADER);
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            user = models.YRUser.findByAuthToken(authTokenHeaderValues[0]);
            if (user != null) {
                ctx.args.put("user", user);
                return delegate.call(ctx);
            }
        }
        
        ObjectNode result = Json.newObject();
        result.put("success", false);
        result.put("errorMessage", "Unauthorized authentication token.");
        
        
        
        return unauthorized(result);
    }

    public static YRUser getUser() {
        return (YRUser)Http.Context.current().args.get("user");
    }

    // returns an authToken
    public static Result login() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            return badRequest(loginForm.errorsAsJson());
        }

        Login login = loginForm.get();

        YRUser user = YRUser.findByUsernameAndPassword(login.uname, login.pass);

        if (user == null) {
        	ObjectNode authTokenJson = Json.newObject();
        	authTokenJson.put("success", false);
            return unauthorized(authTokenJson);
        }
        else {
            String authToken = user.createToken();
            ObjectNode authTokenJson = Json.newObject();
            authTokenJson.put(AUTH_TOKEN, authToken);
            authTokenJson.put("success", true);
            response().setCookie(AUTH_TOKEN, authToken);
            return ok(authTokenJson);
        }
    }

    @With(SecurityController.class)
    public static Result logout() {
        response().discardCookie(AUTH_TOKEN);
        getUser().deleteAuthToken();
        return redirect("/");
    }

    public static class Login {

        @Constraints.Required
        public String uname;

        @Constraints.Required
        public String pass;

    }


}