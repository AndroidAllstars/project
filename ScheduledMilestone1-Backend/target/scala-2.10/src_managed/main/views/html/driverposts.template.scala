
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object driverposts extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[DriverPost],Form[DriverPost],play.api.templates.Html] {

    /**/
    def apply/*1.2*/(posts: List[DriverPost], driverPostForm: Form[DriverPost]):play.api.templates.Html = {
        _display_ {import helper._


Seq[Any](format.raw/*1.61*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Driver post list")/*5.26*/ {_display_(Seq[Any](format.raw/*5.28*/("""
    
    <h1>"""),_display_(Seq[Any](/*7.10*/posts/*7.15*/.size())),format.raw/*7.22*/(""" post(s)</h1>

    <ul>
      """),_display_(Seq[Any](/*10.8*/for(post <- posts) yield /*10.26*/ {_display_(Seq[Any](format.raw/*10.28*/("""
        <li>
        """),_display_(Seq[Any](/*12.10*/post/*12.14*/.startPoint)),format.raw/*12.25*/(""" - """),_display_(Seq[Any](/*12.29*/post/*12.33*/.endPoint)),format.raw/*12.42*/("""
        </li>
      """)))})),format.raw/*14.8*/("""
    </ul>

    <h2>Add a new post</h2>
    """),_display_(Seq[Any](/*18.6*/form(routes.Application.newDriverPost())/*18.46*/ {_display_(Seq[Any](format.raw/*18.48*/("""
      """),_display_(Seq[Any](/*19.8*/inputText(driverPostForm("startPoint")))),format.raw/*19.47*/(""" - """),_display_(Seq[Any](/*19.51*/inputText(driverPostForm("endPoint")))),format.raw/*19.88*/(""" 
      <input type="submit" value="Create">
    """)))})),format.raw/*21.6*/("""
""")))})))}
    }
    
    def render(posts:List[DriverPost],driverPostForm:Form[DriverPost]): play.api.templates.Html = apply(posts,driverPostForm)
    
    def f:((List[DriverPost],Form[DriverPost]) => play.api.templates.Html) = (posts,driverPostForm) => apply(posts,driverPostForm)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Mon May 20 03:39:13 PDT 2013
                    SOURCE: /Users/raymondlau/Projects/yolorideplay/app/views/driverposts.scala.html
                    HASH: 21127aebb622ebe71c80816eac66990ebde3beb2
                    MATRIX: 756->1|908->60|936->79|972->81|1004->105|1043->107|1093->122|1106->127|1134->134|1200->165|1234->183|1274->185|1333->208|1346->212|1379->223|1419->227|1432->231|1463->240|1516->262|1596->307|1645->347|1685->349|1728->357|1789->396|1829->400|1888->437|1969->487
                    LINES: 26->1|30->1|32->4|33->5|33->5|33->5|35->7|35->7|35->7|38->10|38->10|38->10|40->12|40->12|40->12|40->12|40->12|40->12|42->14|46->18|46->18|46->18|47->19|47->19|47->19|47->19|49->21
                    -- GENERATED --
                */
            