package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import models.YRUser;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.node.ObjectNode;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;

import play.Logger;
import play.Play;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.*;

public class NotificationController extends Controller {

	public static HttpClient client = new DefaultHttpClient();
	
	public static Result sendData() {
		ObjectNode result = Json.newObject();
		
		//DynamicForm filledForm = DynamicForm.form().bindFromRequest();
		String regId;
		if(request().getHeader("X-Auth-Token") != null) {
			YRUser user = YRUser.findByAuthToken(request().getHeader("X-Auth-Token").toString());
			if(user == null) {
				result.put("success", false);
				result.put("errorMessage", "Invalid auth token");
				return unauthorized(result);
			}
			regId = user.getRegistration_id();
		} else {
			regId = YRUser.findByUsername("raymond").getRegistration_id();
		}
		
		Logger.debug("-------- New GCM Call ----------");
		Logger.debug("regId = " + regId);
		
		try {
		      
		    Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
			Message message = new Message.Builder()
				.collapseKey("10")
				.timeToLive(3)
				.delayWhileIdle(true)
				.addData("testmessage", "thisfromraymond")
				.addData("key1", "value2")
				.build();
				
			com.google.android.gcm.server.Result gcmResult = sender.send(message, regId, 5);
			Logger.debug("message = " + gcmResult.getMessageId());
			Logger.debug("error = " + gcmResult.getErrorCodeName());
				
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ok();
	}
}
