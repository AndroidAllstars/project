package models;

import java.util.*;

import play.db.ebean.*;
import play.data.validation.Constraints.*;

import javax.persistence.*;

import org.joda.time.DateTime;

@Entity
public class Ride extends Model {

  @Id
  public Long id;
  
  @Required
  public String startAddress;
  @Required
  public String endAddress;
  public DateTime startDate;
  public DateTime endDate;

  public static Finder<Long,Ride> find = new Finder(
    Long.class, Ride.class
  );

  public static List<Ride> all() {
    return find.all();
  }
 
  public static void create(Ride ride) { 
    ride.startDate = DateTime.now();
    ride.endDate = null;
    ride.save();
  }

  public static void delete(Long id) {
    find.ref(id).delete(); 
  }
}
