# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table base_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_times                 varchar(255),
  dep_dates                 varchar(255),
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  constraint pk_base_post primary key (id))
;

create table driver_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_times                 varchar(255),
  dep_dates                 varchar(255),
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  max_seats                 integer,
  seats_open                integer,
  req_fee                   integer,
  constraint pk_driver_post primary key (id))
;

create table ride (
  id                        bigint not null,
  start_address             varchar(255),
  end_address               varchar(255),
  start_date                timestamp,
  end_date                  timestamp,
  constraint pk_ride primary key (id))
;

create table rider_post (
  id                        bigint not null,
  start_point               varchar(255),
  end_point                 varchar(255),
  dep_time                  varchar(255),
  dep_date                  varchar(255),
  dep_times                 varchar(255),
  dep_dates                 varchar(255),
  posted_date               timestamp,
  pets                      boolean,
  pickup                    boolean,
  handicap                  boolean,
  constraint pk_rider_post primary key (id))
;

create table yruser (
  uname                     varchar(255) not null,
  pass                      varchar(255),
  name                      varchar(255),
  phone                     varchar(255),
  address                   varchar(255),
  license                   varchar(255),
  plates                    varchar(255),
  car                       varchar(255),
  constraint pk_yruser primary key (uname))
;

create sequence base_post_seq;

create sequence driver_post_seq;

create sequence ride_seq;

create sequence rider_post_seq;

create sequence yruser_seq;




# --- !Downs

drop table if exists base_post cascade;

drop table if exists driver_post cascade;

drop table if exists ride cascade;

drop table if exists rider_post cascade;

drop table if exists yruser cascade;

drop sequence if exists base_post_seq;

drop sequence if exists driver_post_seq;

drop sequence if exists ride_seq;

drop sequence if exists rider_post_seq;

drop sequence if exists yruser_seq;

