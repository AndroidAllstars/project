package com.yolo;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

import com.yolo.fragments.DatePickerFragment;
import com.yolo.fragments.HomePostViewerFragment;
import com.yolo.fragments.SearchPostOfferViewerFragment;
import com.yolo.fragments.SearchPostRequestViewerFragment;
import com.yolo.fragments.TimePickerFragment;


import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
//import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.AdapterView;
//import android.widget.ExpandableListView;
//import android.widget.ImageButton;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Search extends Activity implements TimePickerFragment.OnCompleteListener, DatePickerFragment.OnCompleteListener{
	EditText eTxtStartPoint;
	EditText eTxtEndPoint;
	EditText eTxtDepTime;
	EditText eTxtDepDate;
	final ArrayList<CPost> offers = new ArrayList<CPost>();
	
	final static int SIZE = CPost.getSize();
	Button 	 	 searchBtn;
	ToggleButton toggleOptionsSearch;
	LinearLayout linearLayoutSearch;
	ListView listSearch;
	String startPointStr;
	String	endPointStr;
	String  offerReqStr;
	String	depTime;
	String  depDate;
	Boolean  offerReqBool;
	Boolean  smokeBool;
	Boolean  petsBool;
	Boolean  cripsBool;
	CheckBox smokeCheck;
	CheckBox petsCheck;
	CheckBox cripCheck; 
	long 	myID;
	final Context ctx = this;
	@Override
	protected void onPause() {
		super.onPause();
		offers.clear();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 
	//	final Context ctx = this;
		Intent i = getIntent();
		myID = 0;
		eTxtStartPoint = (EditText) findViewById(R.id.eTxtStartPointSearch);
		eTxtEndPoint = (EditText) findViewById(R.id.eTxtEndPointSearch);
		eTxtDepTime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
		eTxtDepDate = (EditText) findViewById(R.id.eTxtDepDateSearch);
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupSearch);
		 RadioButton radioOffer = (RadioButton) findViewById(R.id.radioOSearch);
		RadioButton radioReq = (RadioButton) findViewById(R.id.radioRSearch); //*/
		Button timeInput     = (Button) findViewById(R.id.timeInBtnSearch);
		Button dateInput     = (Button) findViewById(R.id.dateInBtnSearch);  //*/
		 smokeCheck    = (CheckBox) findViewById(R.id.checkSmokeSearch);
		 petsCheck     = (CheckBox) findViewById(R.id.checkPetSearch);  //*/
		 cripCheck     = (CheckBox) findViewById(R.id.checkCripSearch);  //*/
			startPointStr = i.getStringExtra("start");
			endPointStr   = i.getStringExtra("end");
			myID   		  = i.getLongExtra("myID", 0);
			depTime = "";
			depDate = "";
			depTime 	= i.getStringExtra("depTime");
			depDate     = i.getStringExtra("depDate");
			offerReqStr = "r";
			offerReqStr  = i.getStringExtra("o/r");  
		
		timeInput.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  showTimePickerDialog(v);
	    	  
	      }
		});
		dateInput.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {
		    	  showDatePickerDialog(v);
		      }
			});
		//*/
		if(offerReqStr.equals("o"))
			{offerReqBool = true;
				radioGroup.check(R.id.radioOSearch);
			}
		else if(offerReqStr.equals("r"))
			{ offerReqBool = false; 
				radioGroup.check(R.id.radioRSearch);
			}
		else
			System.out.println("WHYYYYYYY does it say '" + offerReqStr + "'");


		eTxtStartPoint.setText(startPointStr);
		eTxtEndPoint.setText(endPointStr);//*/
		eTxtDepTime.setText(depTime);
		eTxtDepDate.setText(depDate);//*/
		
		linearLayoutSearch  = (LinearLayout) findViewById(R.id.linearLayoutSearch);
		listSearch          = (ListView) findViewById(R.id.listSearch);
		
		toggleOptionsSearch = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
		toggleOptionsSearch.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  options();
		  }
	    }); //*/
		
	    searchBtn = (Button) findViewById(R.id.btnSearch);
	    searchBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  search();
			}
	      
	      

	    });  //*/
	    
	    search();
	    options();


/*	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final String item = (String) parent.getItemAtPosition(position);
	      }

	    });//*/
	    
	    
	
	}


	  public void showTimePickerDialog(View v) {
		    DialogFragment newFragment = new TimePickerFragment();
		    newFragment.show(getFragmentManager(), "timePicker");
		    
		}

	  public void showDatePickerDialog(View v) {
		    DialogFragment newFragment = new DatePickerFragment();
		    newFragment.show(getFragmentManager(), "datePicker");
		    
		}

		@Override
		public void onComplete(String time, String type) {
			if(type.equals("time"))
				eTxtDepTime.setText(time);
			else if(type.equals("date"))
				eTxtDepDate.setText(time);
		}//*/
		
	  private class StableArrayAdapter extends ArrayAdapter<CPost> {

	    HashMap<CPost, Integer> mIdMap = new HashMap<CPost, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<CPost> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      CPost item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
      public void options() {

  		if(toggleOptionsSearch.isChecked())
  		{	
  			linearLayoutSearch.setVisibility(0);
  		}
  		else
  		{
  			linearLayoutSearch.setVisibility(8);
  		}
		
	  }
	  public void search() {
		  
			RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroupSearch);
			RadioButton radioOffer = (RadioButton) findViewById(R.id.radioOSearch);
			RadioButton radioReq = (RadioButton) findViewById(R.id.radioRSearch); //*/
			offers.clear();
    	  //send stuff here
    	  HttpClient httpclient = new DefaultHttpClient();
    	  HttpPost httppost;
    	  CPostOffer searchPost;
    	  CPostRequest searchRPost;
    	  startPointStr = eTxtStartPoint.getText().toString();
    	  endPointStr   = eTxtEndPoint.getText().toString();
    	  depTime 		= eTxtDepTime.getText().toString();
    	  depDate	    = eTxtDepDate.getText().toString();
    	  //smokeBool = false;
    	  smokeBool 	= smokeCheck.isChecked();
    	  petsBool 		= petsCheck.isChecked();
    	  cripsBool 	= cripCheck.isChecked();//*/
    	  char checker;
    	  final ListView listview = (ListView) findViewById(R.id.listSearch);
	        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(SIZE);

    	  if(radioOffer.isChecked())
    	    {	
    		  	checker = 'o';
    		  	httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/driverposts/search"); //set this up
    	    	if(depTime.equals(null))
        	    	{searchPost =  new CPostOffer(startPointStr, endPointStr);
        	        listTemp =  searchPost.submit(1);
        	    		System.out.println("THE TIME IS NULLLL");
        	    	}
    	    	else if(depTime.equals(""))
    	    		{searchPost =  new CPostOffer(startPointStr, endPointStr);
    		        listTemp =  searchPost.submit(1);
	    				System.out.println("THE TIME IS THE EMPTY STRING!");
    	    		}
    	    	else
    	    		{ searchPost =  new CPostOffer(startPointStr, endPointStr, depTime, depDate, smokeBool, petsBool, cripsBool);

    		        listTemp =  searchPost.submit(1);
    		       System.out.println("THE TIME IS NOT EMPTY!");
    	    			
    	    		}
    	    }
    	  else
      	    {	System.out.println("request =============");
    		  checker = 'r';
    		  httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/riderposts/search"); 
	    	
	    	if(depTime.equals(""))
    		{	searchRPost =  new CPostRequest(startPointStr, endPointStr);
	        	listTemp =  searchRPost.submit();
				System.out.println("THE TIME IS THE EMPTY STRING!");
    		}
    	else
    		{ searchRPost =  new CPostRequest(startPointStr, endPointStr, depTime, depDate, smokeBool, petsBool, cripsBool);
    			listTemp =  searchRPost.submit();
    			System.out.println("THE TIME IS NOT EMPTY!");
    		}
	    	//searchPost =  new CPostOffer(startPointStr, endPointStr);  		//THIS IS VERY WRONG

      	    }
    	   //public void submit(){
    	  

    	  try {
			   // Add your data
    		 // CPostOffer searchPostOffer = new CPostOffer(startPointStr, endPointStr);
    		  
	       System.out.println("RAN THE SUBMIT FUNCTION ON : " + listTemp.get(1).toString());
    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
    		 YoloRide appState = ((YoloRide)getApplicationContext());
   	      HttpContext localContext = appState.getContext();
   	      CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
   	      Cookie peanutbutter = store.getCookies().get(0);
   	      httppost.setHeader("X-AUTH-TOKEN", peanutbutter.getValue());
	        // Execute HTTP Post Request
	       HttpResponse response = httpclient.execute(httppost, localContext);
			System.out.println("CREATED THE RESPONSE");
	       HttpEntity entity = response.getEntity();
	       Integer statusCode = response.getStatusLine().getStatusCode();
	       StringBuilder builder = new StringBuilder();
			System.out.println("STRING BUILT");
	       
	       InputStream content = entity.getContent();
	       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	       String line = reader.readLine();
	       builder.append(line);
	       try {
	    	   Translate translate = new Translate();
	    	    ArrayList<CPostOffer> tempo;
	    	    ArrayList<CPostRequest> tempr;
	    	   if (checker == 'o')
	    	   {
	    		   tempo = translate.action(new JSONObject(builder.toString()), new CPostOffer());
	    		   for (int k = 0; k < tempo.size(); k++)
	    		   {
	    			   offers.add(tempo.get(k));
	    		   }
	    	   }
	    	   else
	    	   {
	    		   tempr = translate.action(new JSONObject(builder.toString()), new CPostRequest("",""));
	    		   for (int k = 0; k < tempr.size(); k++)
	    		   {
	    			   offers.add(tempr.get(k));
	    		   }
	    	   }
				System.out.println("THERE ARE : " + offers.size() + " OFFERS");
				
	       } catch(Exception e) {
	    	   e.printStackTrace();
	       }
    		  //System.out.println("HERE IS THE RESPONSE - Status Code " + statusCode + " and Response text = " + builder.toString());
    		  
  		    } catch (ClientProtocolException e) {
  		        // TODO Auto-generated catch block
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    } //*/	  
    	  


  	    final StableArrayAdapter adapter = new StableArrayAdapter(ctx,
  	        android.R.layout.simple_list_item_1, offers);
  	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	         CPostOffer item1;
	         CPostRequest item2;
	    /*    Toast.makeText(getApplicationContext(),
	        	      "Items start point is " + item.startPoint, Toast.LENGTH_LONG)
	        	      .show(); //*/
			 RadioButton radioOffer = (RadioButton) findViewById(R.id.radioOSearch);
	    	  if(radioOffer.isChecked())
	    	    { 	System.out.println("got to the if block 000000000000000000");
	        		item1 = (CPostOffer) parent.getItemAtPosition(position);
	    	   		showPostView(item1);
	        	}
	        else
	        	{ 	System.out.println("got to the else block 000000000000000000");

	        		item2 = (CPostRequest) parent.getItemAtPosition(position);
    	   			showRequestView(item2);//*/
	        	}
	      }
	    });
	  }


	    void showRequestView(CPostRequest postIn) {
	        //mStackLevel++;

	        // DialogFragment.show() will take care of adding the fragment
	        // in a transaction.  We also want to remove any currently showing
	        // dialog, so make our own transaction and take care of that here.
	    	System.out.println("got to showRequestView() 000000000000000000");
	        FragmentTransaction ft = getFragmentManager().beginTransaction();
	        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	        if (prev != null) {
	            ft.remove(prev);
	        }
	        ft.addToBackStack(null);
	        //Create and show the dialog.
	        System.out.println("ABOUT TO START THE DIALOG");
	        DialogFragment newFragment = SearchPostRequestViewerFragment.newInstance(postIn, myID);
	        newFragment.show(ft, "dialog");
	    }
	    
	    void showPostView(CPostOffer postIn) {
	        //mStackLevel++;

	        // DialogFragment.show() will take care of adding the fragment
	        // in a transaction.  We also want to remove any currently showing
	        // dialog, so make our own transaction and take care of that here.
	    	
	        FragmentTransaction ft = getFragmentManager().beginTransaction();
	        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	        if (prev != null) {
	            ft.remove(prev);
	        }
	        ft.addToBackStack(null);

	        // Create and show the dialog.
	        System.out.println("ABOUT TO START THE DIALOG");
	        DialogFragment newFragment = SearchPostOfferViewerFragment.newInstance(postIn);
	        newFragment.show(ft, "dialog");
	    }
	    @Override
		  protected void onResume()
		  {
			  super.onResume();
			  if (Locale.getDefault().getDisplayLanguage().equals("Hindi"))
			  {
				  CheckBox smokess = (CheckBox) findViewById(R.id.checkSmokeSearch);
				  smokess.setText("धूम्रपान ठीक है:");
				  CheckBox petss = (CheckBox) findViewById(R.id.checkPetSearch);
				  petss.setText("ठीक पालतू जानवर:");
				  CheckBox cripss = (CheckBox) findViewById(R.id.checkCripSearch);
				  cripss.setText("सुलभ विकलांग:");
				  Button searcher = (Button) findViewById(R.id.btnSearch);
				  searcher.setText("खोज");
				  Button picktime = (Button) findViewById(R.id.timeInBtnSearch);
				  picktime.setText("समय लेने");
				  Button pickdate = (Button) findViewById(R.id.dateInBtnSearch);
				  pickdate.setText("तारीख लेने");
				  ToggleButton moreops = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
				  moreops.setTextOn("कम विकल्प");
				  moreops.setTextOff("अधिक विकल्प");
				  RadioButton offeron = (RadioButton) findViewById(R.id.radioOSearch);
				  offeron.setText("प्रदान करता है");
				  RadioButton requeston = (RadioButton) findViewById(R.id.radioRSearch);
				  requeston.setText("अनुरोधों");
				  EditText starter = (EditText) findViewById(R.id.eTxtStartPointSearch);
				  starter.setHint("बिंदु प्रारंभ");
				  EditText ender = (EditText) findViewById(R.id.eTxtEndPointSearch);
				  ender.setHint("गंतव्य");
				  EditText dtime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
				  dtime.setHint("प्रस्थान का समय");
				  EditText ddate = (EditText) findViewById(R.id.eTxtDepDateSearch);
				  ddate.setHint("प्रस्थान तिथि");
			  }
			  else
			  {
				  CheckBox smokess = (CheckBox) findViewById(R.id.checkSmokeSearch);
				  smokess.setText("Smoking");
				  CheckBox petss = (CheckBox) findViewById(R.id.checkPetSearch);
				  petss.setText("Pets OK");
				  CheckBox cripss = (CheckBox) findViewById(R.id.checkCripSearch);
				  cripss.setText("Handicap Accessible");
				  Button searcher = (Button) findViewById(R.id.btnSearch);
				  searcher.setText("Search");
				  Button picktime = (Button) findViewById(R.id.timeInBtnSearch);
				  picktime.setText("Pick Time");
				  Button pickdate = (Button) findViewById(R.id.dateInBtnSearch);
				  pickdate.setText("Pick Date");
				  ToggleButton moreops = (ToggleButton) findViewById(R.id.toggleOptionsSearch);
				  moreops.setTextOn("Less Options");
				  moreops.setTextOff("More Options");
				  RadioButton offeron = (RadioButton) findViewById(R.id.radioOSearch);
				  offeron.setText("Offers");
				  RadioButton requeston = (RadioButton) findViewById(R.id.radioRSearch);
				  requeston.setText("Requests");
				  EditText starter = (EditText) findViewById(R.id.eTxtStartPointSearch);
				  starter.setHint("Enter Start Address");
				  EditText ender = (EditText) findViewById(R.id.eTxtEndPointSearch);
				  ender.setHint("Enter Destination");
				  EditText dtime = (EditText) findViewById(R.id.eTxtDepTimeSearch);
				  dtime.setHint("Departure Time");
				  EditText ddate = (EditText) findViewById(R.id.eTxtDepDateSearch);
				  ddate.setHint("Departure Date");
			  }
		  }
}