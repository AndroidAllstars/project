package com.yolo;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class CPostOffer extends CPost {

	private double reqFee;
	private int    seatsOpen;
	private int    maxSeats;
	
	public CPostOffer(){
		super();
		reqFee = 0;
		maxSeats = 0;
		seatsOpen = maxSeats;
		
	}
	public CPostOffer(String startPointIn, String endPointIn){
		super();
		startPoint 	= startPointIn;
		endPoint 	= endPointIn;
		reqFee = 0;
		maxSeats = 0;
		seatsOpen = maxSeats;
		
	}
	public CPostOffer(String startPointIn, String endPointIn, String depTimeIn,
			String depDateIn, boolean pickupIn, boolean petsIn,
			boolean handicapIn, double reqFeeIn, int seatsIn) {
		super(startPointIn, endPointIn, depTimeIn, depDateIn, pickupIn, petsIn,
				handicapIn);
		reqFee = reqFeeIn;
		maxSeats = seatsIn;
		seatsOpen = maxSeats;
		// TODO Auto-generated constructor stub
	}

	
	
	public void addPassenger()
	{
		seatsOpen--;
	}
	public void addPassenger(int newPassengers)
	{
		seatsOpen = seatsOpen - newPassengers;
	}
	public void removePassenger()
	{	
		if(seatsOpen < maxSeats)
			seatsOpen++;
	}
	public void removePassenger(int oldPassengers)
	{	while(oldPassengers > 0)
		{ if(seatsOpen < maxSeats)
			{ 	seatsOpen++;
				oldPassengers--;
			}
		}
	}



	@Override
	List<NameValuePair> submit() {
		List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
		listTemp.add(new BasicNameValuePair("class", "CPostOffer"));
		listTemp.add(new BasicNameValuePair("startPoint", startPoint));
		System.out.println("START POINT IS:::   " + startPoint);
		listTemp.add(new BasicNameValuePair("endPoint", endPoint)); 
		System.out.println("END POINT IS:::   " + endPoint);
		listTemp.add(new BasicNameValuePair("depTime", depTime)); 
		listTemp.add(new BasicNameValuePair("depDate", depDate)); 
	/*	listTemp.add(new BasicNameValuePair("pickup", String.valueOf(pickup))); 
		listTemp.add(new BasicNameValuePair("pets", String.valueOf(pets))); 
		listTemp.add(new BasicNameValuePair("handicap", String.valueOf(handicap))); 
		listTemp.add(new BasicNameValuePair("reqFee", String.valueOf(reqFee)));  
		listTemp.add(new BasicNameValuePair("maxSeats", String.valueOf(maxSeats)));
		listTemp.add(new BasicNameValuePair("seatsOpen", String.valueOf(seatsOpen)));   //*/
	/*	 String startPointIn, String endPointIn, String depTimeIn,
			String depDateIn, boolean pickupIn, boolean petsIn,
			boolean handicapIn, double reqFeeIn, int seatsIn
		// */
		return listTemp;
	}
}
