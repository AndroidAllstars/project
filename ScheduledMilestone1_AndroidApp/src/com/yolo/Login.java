package com.yolo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {

	String userIn;
	String passwordIn;
	
	EditText eTxtUser;
	EditText eTxtPassword;
	
	
	Button loginBtn;
	Button createBtn;
	Button forgotBtn;
	//*/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 

		final Context ctx = this;
		eTxtUser 		= (EditText) findViewById(R.id.eTxtUserNameLogin);
		eTxtPassword 	= (EditText) findViewById(R.id.eTxtPasswordLogin);
		

		loginBtn 		= (Button) findViewById(R.id.btnLogin);
		forgotBtn 		= (Button) findViewById(R.id.btnForgotLogin);
		createBtn 		= (Button) findViewById(R.id.btnCreateLogin);
		//do something with these
		
		loginBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {

	  		userIn = eTxtUser.getText().toString();
	  		passwordIn = eTxtPassword.getText().toString();
	  		CUser user = new CUser(userIn, passwordIn);
	    	  HttpClient httpclient = new DefaultHttpClient();
	    	    HttpPost httppost = new HttpPost("http://peaceful-sands-8466.herokuapp.com/users/login"); //set this up

	    	   //public void submit(){
	    	  try {
  				   // Add your data
  		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
  		        listTemp =  user.submit();
	    		httppost.setEntity(new UrlEncodedFormEntity(listTemp));
	    		 

  		        // Execute HTTP Post Request
  		       HttpResponse response = httpclient.execute(httppost);
  		       HttpEntity entity = response.getEntity();
  		       Integer statusCode = response.getStatusLine().getStatusCode();
  		       String responseText = null;
  		       StringBuilder builder = new StringBuilder();
  		       
  		       InputStream content = entity.getContent();
  		       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
  		       String line = reader.readLine();
  		       builder.append(line);
  		       
  		       try {
  		    	 JSONObject jsonObject = new JSONObject(builder.toString());
  		    	 Boolean loginSuccess = jsonObject.getBoolean("success");
  		    	 
  		    	 if(loginSuccess == true) {
  					Intent i = new Intent();
  					i.setClassName("com.yolo", "com.yolo.Home");
  					startActivity(i);
  		    	 } else {
  		    		Toast toast = Toast.makeText(ctx, "Wrong password", Toast.LENGTH_LONG);
  		    		toast.show();

  		    	 }
  		       } catch(Exception e) {
  		    	   e.printStackTrace();
  		       }
  		       
	    		  System.out.println("HERE IS THE RESPONSE - Status Code " + statusCode + " and Response text = " + builder.toString());
	    		  
	  		    } catch (ClientProtocolException e) {
	  		        // TODO Auto-generated catch block
  		    } catch (IOException e) {
  		        // TODO Auto-generated catch block
  		    } //*/
	    	  
	    	  //do something with those
	  		
			}
	    }); //*/

		createBtn.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {

			  		userIn = eTxtUser.getText().toString();
			  		passwordIn = eTxtPassword.getText().toString();
			    	  //do something with those mayybe?
			  		
				Intent i = new Intent();
				i.setClassName("com.yolo", "com.yolo.NewUser");
				startActivity(i);
				}
		    }); //*/
	
	forgotBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {

		  		
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.ListViewExampleActivity");
			startActivity(i);
			}
	    }); //*/
}

	
	
/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}  //*/

}
