// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:40711fa2b91ede6446d97298b17c9a57afbf599a
// @DATE:Tue Jun 04 21:46:55 PDT 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" } 


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:8
private[this] lazy val controllers_Application_posts1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("posts"))))
        

// @LINE:9
private[this] lazy val controllers_OfferController_driverPostsJson2 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts"))))
        

// @LINE:10
private[this] lazy val controllers_OfferController_deleteDriverPost3 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/delete"))))
        

// @LINE:11
private[this] lazy val controllers_OfferController_newDriverPost4 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts"))))
        

// @LINE:12
private[this] lazy val controllers_OfferController_driverPostSearch5 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/search"))))
        

// @LINE:13
private[this] lazy val controllers_OfferController_riderRequest6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/request"))))
        

// @LINE:14
private[this] lazy val controllers_OfferController_rejectRequest7 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/reject"))))
        

// @LINE:15
private[this] lazy val controllers_OfferController_acceptRider8 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/accept"))))
        

// @LINE:16
private[this] lazy val controllers_OfferController_rateRide9 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/rate"))))
        

// @LINE:17
private[this] lazy val controllers_OfferController_rateRider10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/raterider"))))
        

// @LINE:18
private[this] lazy val controllers_OfferController_cancelRequest11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/cancel"))))
        

// @LINE:19
private[this] lazy val controllers_OfferController_transferMoney12 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/pay"))))
        

// @LINE:21
private[this] lazy val controllers_Application_riderPostsJson13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts"))))
        

// @LINE:22
private[this] lazy val controllers_Application_newRiderPost14 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts"))))
        

// @LINE:23
private[this] lazy val controllers_Application_deleteRiderPost15 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/delete"))))
        

// @LINE:24
private[this] lazy val controllers_Application_riderPostSearch16 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts/search"))))
        

// @LINE:25
private[this] lazy val controllers_SearchController_offerRide17 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/offer"))))
        

// @LINE:26
private[this] lazy val controllers_SearchController_acceptRide18 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("riderposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/accept"))))
        

// @LINE:28
private[this] lazy val controllers_UserController_users19 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:29
private[this] lazy val controllers_UserController_registerUser20 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:30
private[this] lazy val controllers_UserController_setRegistrationId21 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/regid"))))
        

// @LINE:31
private[this] lazy val controllers_UserController_findByAuthToken22 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/token"))))
        

// @LINE:32
private[this] lazy val controllers_UserController_getDriverPosts23 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/driverposts"))))
        

// @LINE:33
private[this] lazy val controllers_UserController_getRiderPosts24 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/riderposts"))))
        

// @LINE:35
private[this] lazy val controllers_SecurityController_login25 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:36
private[this] lazy val controllers_SecurityController_logout26 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("logout"))))
        

// @LINE:38
private[this] lazy val controllers_TestController_test27 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("test"))))
        

// @LINE:39
private[this] lazy val controllers_TestController_test228 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("test2"))))
        

// @LINE:41
private[this] lazy val controllers_NotificationController_sendData29 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("gcm"))))
        

// @LINE:44
private[this] lazy val controllers_Assets_at30 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """posts""","""controllers.Application.posts()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts""","""controllers.OfferController.driverPostsJson()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/delete""","""controllers.OfferController.deleteDriverPost(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts""","""controllers.OfferController.newDriverPost()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/search""","""controllers.OfferController.driverPostSearch()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/request""","""controllers.OfferController.riderRequest(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/reject""","""controllers.OfferController.rejectRequest(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/accept""","""controllers.OfferController.acceptRider(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/rate""","""controllers.OfferController.rateRide(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/raterider""","""controllers.OfferController.rateRider(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/cancel""","""controllers.OfferController.cancelRequest(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/pay""","""controllers.OfferController.transferMoney(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts""","""controllers.Application.riderPostsJson()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts""","""controllers.Application.newRiderPost()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts/$id<[^/]+>/delete""","""controllers.Application.deleteRiderPost(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts/search""","""controllers.Application.riderPostSearch()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts/$id<[^/]+>/offer""","""controllers.SearchController.offerRide(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """riderposts/$id<[^/]+>/accept""","""controllers.SearchController.acceptRide(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.UserController.users()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.UserController.registerUser()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/regid""","""controllers.UserController.setRegistrationId()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/token""","""controllers.UserController.findByAuthToken()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/driverposts""","""controllers.UserController.getDriverPosts()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/riderposts""","""controllers.UserController.getRiderPosts()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""controllers.SecurityController.login()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """logout""","""controllers.SecurityController.logout()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """test""","""controllers.TestController.test()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """test2""","""controllers.TestController.test2()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """gcm""","""controllers.NotificationController.sendData()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
       
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:8
case controllers_Application_posts1(params) => {
   call { 
        invokeHandler(controllers.Application.posts(), HandlerDef(this, "controllers.Application", "posts", Nil,"GET", """""", Routes.prefix + """posts"""))
   }
}
        

// @LINE:9
case controllers_OfferController_driverPostsJson2(params) => {
   call { 
        invokeHandler(controllers.OfferController.driverPostsJson(), HandlerDef(this, "controllers.OfferController", "driverPostsJson", Nil,"GET", """""", Routes.prefix + """driverposts"""))
   }
}
        

// @LINE:10
case controllers_OfferController_deleteDriverPost3(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.deleteDriverPost(id), HandlerDef(this, "controllers.OfferController", "deleteDriverPost", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/delete"""))
   }
}
        

// @LINE:11
case controllers_OfferController_newDriverPost4(params) => {
   call { 
        invokeHandler(controllers.OfferController.newDriverPost(), HandlerDef(this, "controllers.OfferController", "newDriverPost", Nil,"POST", """""", Routes.prefix + """driverposts"""))
   }
}
        

// @LINE:12
case controllers_OfferController_driverPostSearch5(params) => {
   call { 
        invokeHandler(controllers.OfferController.driverPostSearch(), HandlerDef(this, "controllers.OfferController", "driverPostSearch", Nil,"POST", """""", Routes.prefix + """driverposts/search"""))
   }
}
        

// @LINE:13
case controllers_OfferController_riderRequest6(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.riderRequest(id), HandlerDef(this, "controllers.OfferController", "riderRequest", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/request"""))
   }
}
        

// @LINE:14
case controllers_OfferController_rejectRequest7(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.rejectRequest(id), HandlerDef(this, "controllers.OfferController", "rejectRequest", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/reject"""))
   }
}
        

// @LINE:15
case controllers_OfferController_acceptRider8(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.acceptRider(id), HandlerDef(this, "controllers.OfferController", "acceptRider", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/accept"""))
   }
}
        

// @LINE:16
case controllers_OfferController_rateRide9(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.rateRide(id), HandlerDef(this, "controllers.OfferController", "rateRide", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/rate"""))
   }
}
        

// @LINE:17
case controllers_OfferController_rateRider10(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.rateRider(id), HandlerDef(this, "controllers.OfferController", "rateRider", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/raterider"""))
   }
}
        

// @LINE:18
case controllers_OfferController_cancelRequest11(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.cancelRequest(id), HandlerDef(this, "controllers.OfferController", "cancelRequest", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/cancel"""))
   }
}
        

// @LINE:19
case controllers_OfferController_transferMoney12(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.OfferController.transferMoney(id), HandlerDef(this, "controllers.OfferController", "transferMoney", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/pay"""))
   }
}
        

// @LINE:21
case controllers_Application_riderPostsJson13(params) => {
   call { 
        invokeHandler(controllers.Application.riderPostsJson(), HandlerDef(this, "controllers.Application", "riderPostsJson", Nil,"GET", """""", Routes.prefix + """riderposts"""))
   }
}
        

// @LINE:22
case controllers_Application_newRiderPost14(params) => {
   call { 
        invokeHandler(controllers.Application.newRiderPost(), HandlerDef(this, "controllers.Application", "newRiderPost", Nil,"POST", """""", Routes.prefix + """riderposts"""))
   }
}
        

// @LINE:23
case controllers_Application_deleteRiderPost15(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.deleteRiderPost(id), HandlerDef(this, "controllers.Application", "deleteRiderPost", Seq(classOf[Long]),"POST", """""", Routes.prefix + """riderposts/$id<[^/]+>/delete"""))
   }
}
        

// @LINE:24
case controllers_Application_riderPostSearch16(params) => {
   call { 
        invokeHandler(controllers.Application.riderPostSearch(), HandlerDef(this, "controllers.Application", "riderPostSearch", Nil,"POST", """""", Routes.prefix + """riderposts/search"""))
   }
}
        

// @LINE:25
case controllers_SearchController_offerRide17(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.SearchController.offerRide(id), HandlerDef(this, "controllers.SearchController", "offerRide", Seq(classOf[Long]),"POST", """""", Routes.prefix + """riderposts/$id<[^/]+>/offer"""))
   }
}
        

// @LINE:26
case controllers_SearchController_acceptRide18(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.SearchController.acceptRide(id), HandlerDef(this, "controllers.SearchController", "acceptRide", Seq(classOf[Long]),"POST", """""", Routes.prefix + """riderposts/$id<[^/]+>/accept"""))
   }
}
        

// @LINE:28
case controllers_UserController_users19(params) => {
   call { 
        invokeHandler(controllers.UserController.users(), HandlerDef(this, "controllers.UserController", "users", Nil,"GET", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:29
case controllers_UserController_registerUser20(params) => {
   call { 
        invokeHandler(controllers.UserController.registerUser(), HandlerDef(this, "controllers.UserController", "registerUser", Nil,"POST", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:30
case controllers_UserController_setRegistrationId21(params) => {
   call { 
        invokeHandler(controllers.UserController.setRegistrationId(), HandlerDef(this, "controllers.UserController", "setRegistrationId", Nil,"POST", """""", Routes.prefix + """users/regid"""))
   }
}
        

// @LINE:31
case controllers_UserController_findByAuthToken22(params) => {
   call { 
        invokeHandler(controllers.UserController.findByAuthToken(), HandlerDef(this, "controllers.UserController", "findByAuthToken", Nil,"POST", """""", Routes.prefix + """users/token"""))
   }
}
        

// @LINE:32
case controllers_UserController_getDriverPosts23(params) => {
   call { 
        invokeHandler(controllers.UserController.getDriverPosts(), HandlerDef(this, "controllers.UserController", "getDriverPosts", Nil,"GET", """""", Routes.prefix + """users/driverposts"""))
   }
}
        

// @LINE:33
case controllers_UserController_getRiderPosts24(params) => {
   call { 
        invokeHandler(controllers.UserController.getRiderPosts(), HandlerDef(this, "controllers.UserController", "getRiderPosts", Nil,"GET", """""", Routes.prefix + """users/riderposts"""))
   }
}
        

// @LINE:35
case controllers_SecurityController_login25(params) => {
   call { 
        invokeHandler(controllers.SecurityController.login(), HandlerDef(this, "controllers.SecurityController", "login", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:36
case controllers_SecurityController_logout26(params) => {
   call { 
        invokeHandler(controllers.SecurityController.logout(), HandlerDef(this, "controllers.SecurityController", "logout", Nil,"POST", """""", Routes.prefix + """logout"""))
   }
}
        

// @LINE:38
case controllers_TestController_test27(params) => {
   call { 
        invokeHandler(controllers.TestController.test(), HandlerDef(this, "controllers.TestController", "test", Nil,"POST", """""", Routes.prefix + """test"""))
   }
}
        

// @LINE:39
case controllers_TestController_test228(params) => {
   call { 
        invokeHandler(controllers.TestController.test2(), HandlerDef(this, "controllers.TestController", "test2", Nil,"POST", """""", Routes.prefix + """test2"""))
   }
}
        

// @LINE:41
case controllers_NotificationController_sendData29(params) => {
   call { 
        invokeHandler(controllers.NotificationController.sendData(), HandlerDef(this, "controllers.NotificationController", "sendData", Nil,"GET", """""", Routes.prefix + """gcm"""))
   }
}
        

// @LINE:44
case controllers_Assets_at30(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}
    
}
        