package controllers;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.codehaus.jackson.node.ObjectNode;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Sender;

import play.Play;
import play.mvc.*;
import models.DriverPost;
import models.YRUser;
import models.RiderPost;

import play.data.DynamicForm;
import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.Json;

public class SearchController extends Controller {
	
	@With(SecurityController.class)
	public static Result offerRide(final Long id) {
		final YRUser driver = SecurityController.getUser();
		final DynamicForm filledForm = DynamicForm.form().bindFromRequest();
		
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
			new Callable<ObjectNode>() {
				public ObjectNode call() {
					ObjectNode result = Json.newObject();
					
					RiderPost riderPost = RiderPost.findRiderPost.byId(id);
					YRUser rider = riderPost.owner;
					riderPost.offeringDrivers.add(driver);
					riderPost.save();
					
					DriverPost driverPost = DriverPost.findDriverPost.byId(Long.parseLong(filledForm.get("id")));
					
					/* Send notification to rider */
			    	com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
					Message message = new Message.Builder()
						.collapseKey("offerNotification")
						.timeToLive(86400)
						.delayWhileIdle(false)
						.addData("type", "invite_rider")
						.addData("message", driver.getUname() + " has offered you a ride!")
						.addData("id", String.valueOf(driverPost.id))
						.addData("driver", driver.getUname())
						.build();
					try {
						gcmResult = sender.send(message, rider.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error. Failed to notify rider.");
						//e.printStackTrace();
						return result;
					}
					
				}
			}
		);
		
		return async(
			gcmResultPromise.map(
		    	new Function<ObjectNode,Result>() {
		   			public Result apply(ObjectNode r) {
		   				if(r.get("success").asBoolean() == false) {
		   					return badRequest(r);
		   				}
		   				return ok(r);
		   			}
		   		}
	    	)
		);
	}
	
	@With(SecurityController.class)
	public static Result acceptRide(final Long id) {
		final YRUser rider = SecurityController.getUser();
		final DynamicForm filledForm = DynamicForm.form().bindFromRequest();
		
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
			new Callable<ObjectNode>() {
				public ObjectNode call() {
					ObjectNode result = Json.newObject();
					com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
					
					RiderPost riderPost = RiderPost.findRiderPost.byId(id);
					Long driverPostId = Long.parseLong(filledForm.get("id"));
					DriverPost offer = DriverPost.findDriverPost.byId(driverPostId);
					
					if(!rider.equals(riderPost.owner)) {
						result.put("success", false);
						result.put("errorMessage", "You do not own this posting.");
						return result;
					}
					if(!riderPost.offeringDrivers.contains(offer.owner)) {
						result.put("success", false);
						result.put("errorMessage", "That driver did not offer you a ride.");
						return result;
					}
					
					/* Need to figure out what to do */
					
					riderPost.offeringDrivers.remove(offer.owner);
					riderPost.save();
					offer.riders.add(rider);
					offer.save();
					
					Message message = new Message.Builder()
					.collapseKey("acceptRideNotification")
					.timeToLive(86400)
					.delayWhileIdle(false)
					.addData("type", "invite_accept")
					.addData("message", rider.getUname() + " has accepted your ride offer!")
					.addData("id", String.valueOf(riderPost.id))
					.build();
		    		
		    		try {
						gcmResult = sender.send(message, offer.owner.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error.");
						//e.printStackTrace();
						return result;
					}
				}
			}
		);
		
		return async(
	    	gcmResultPromise.map(
	    	   	new Function<ObjectNode,Result>() {
	     			public Result apply(ObjectNode r) {
	       				if(r.get("success").asBoolean() == false) {
	  	   					return badRequest(r);
	   	   				}
	  	   				return ok(r);
	       			}
	      		}
	       	)
	    );	
	} 
	
	@With(SecurityController.class)
	public static Result rejectRide(final Long id) {
		final YRUser rider = SecurityController.getUser();
		final DynamicForm filledForm = DynamicForm.form().bindFromRequest();
		
		Promise<ObjectNode> gcmResultPromise = play.libs.Akka.future(
			new Callable<ObjectNode>() {
				public ObjectNode call() {
					ObjectNode result = Json.newObject();
					com.google.android.gcm.server.Result gcmResult;
					Sender sender = new Sender(Play.application().configuration().getString("google.apiKey"));
					
					if(filledForm.get("driver") == null) {
						result.put("success", false);
						result.put("errorMessage", "Please send a driver.");
						return result;
					}
					
					YRUser driver = YRUser.findByUsername(filledForm.get("driver"));
					if(driver == null) {
						result.put("success", false);
						result.put("errorMessage", "That driver does not exist.");
						return result;
					}
					
					RiderPost riderPost = RiderPost.findRiderPost.byId(id);
					if(!rider.equals(riderPost.owner)) {
						result.put("success", false);
						result.put("errorMessage", "You do not own this posting.");
						return result;
					}
					if(!riderPost.offeringDrivers.contains(driver)) {
						result.put("success", false);
						result.put("errorMessage", "That driver did not offer you a ride.");
						return result;
					}
					
					/* Need to figure out what to do */
					riderPost.offeringDrivers.remove(driver);
					riderPost.save();
					
					Message message = new Message.Builder()
					.collapseKey("rejectRideNotification")
					.timeToLive(86400)
					.delayWhileIdle(false)
					.addData("type", "invite_decline")
					.addData("message", rider.getUname() + " has rejected your ride offer!")
					.addData("id", String.valueOf(riderPost.id))
					.build();
		    		
		    		try {
						gcmResult = sender.send(message, driver.getRegistration_id(), 5);
						result.put("success", true);
						result.put("gcmResult", gcmResult.toString());
						result.put("gcmErrorCode", gcmResult.getErrorCodeName());
						return result;
					} catch (IOException e) {
						result.put("success", false);
						result.put("gcmResult", "Internal server error.");
						//e.printStackTrace();
						return result;
					}
				}
			}
		);
		
		return async(
	    	gcmResultPromise.map(
	    	   	new Function<ObjectNode,Result>() {
	     			public Result apply(ObjectNode r) {
	       				if(r.get("success").asBoolean() == false) {
	  	   					return badRequest(r);
	   	   				}
	  	   				return ok(r);
	       			}
	      		}
	       	)
	    );	
	}
}
