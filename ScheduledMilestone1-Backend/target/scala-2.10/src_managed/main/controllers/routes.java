// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:139fbc5a514ca31fca2be2c4620ddac11d6d3f90
// @DATE:Mon May 20 05:21:57 PDT 2013

package controllers;

public class routes {
public static final controllers.ReverseApplication Application = new controllers.ReverseApplication();
public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets();
public static class javascript {
public static final controllers.javascript.ReverseApplication Application = new controllers.javascript.ReverseApplication();
public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets();    
}   
public static class ref {
public static final controllers.ref.ReverseApplication Application = new controllers.ref.ReverseApplication();
public static final controllers.ref.ReverseAssets Assets = new controllers.ref.ReverseAssets();    
} 
}
              