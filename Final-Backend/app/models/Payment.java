package models;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Payment extends Model {

	@Id
	public Long id;
	
	public Double amount;
	
	@ManyToOne
	@JsonIgnore
	public YRUser rider;
	
	@ManyToOne
	@JsonIgnore
	public YRUser driver;
	
	@ManyToOne
	@JsonIgnore
	public DriverPost post;
	
	public String getSender() {
		return rider.getUname();
	}
	
	public String getRecipient() {
		return driver.getUname();
	}
	
	public static Finder<Long,Payment> findPayment = new Finder(
			   Long.class, Payment.class
			);
	
	public int hashCode() {
		return new HashCodeBuilder(17,31).append(rider.getUname()).build();
	}
	
	public boolean equals(Object obj) {
     if (obj == null)
         return false;
     if (obj == this)
         return true;
     if (!(obj instanceof Payment))
         return false;

     Payment rhs = (Payment) obj;
     return new EqualsBuilder().
         // if deriving: appendSuper(super.equals(obj)).
         append(rider.getUname(), rhs.rider.getUname()).
         isEquals();
	 }
}
