// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:40711fa2b91ede6446d97298b17c9a57afbf599a
// @DATE:Tue Jun 04 21:46:55 PDT 2013

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._
import java.net.URLEncoder

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:44
// @LINE:41
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers {

// @LINE:44
class ReverseAssets {
    

// @LINE:44
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          

// @LINE:36
// @LINE:35
class ReverseSecurityController {
    

// @LINE:36
def logout(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "logout")
}
                                                

// @LINE:35
def login(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                
    
}
                          

// @LINE:39
// @LINE:38
class ReverseTestController {
    

// @LINE:38
def test(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "test")
}
                                                

// @LINE:39
def test2(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "test2")
}
                                                
    
}
                          

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseOfferController {
    

// @LINE:16
def rateRide(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/rate")
}
                                                

// @LINE:12
def driverPostSearch(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/search")
}
                                                

// @LINE:13
def riderRequest(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/request")
}
                                                

// @LINE:15
def acceptRider(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/accept")
}
                                                

// @LINE:17
def rateRider(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/raterider")
}
                                                

// @LINE:19
def transferMoney(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/pay")
}
                                                

// @LINE:18
def cancelRequest(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/cancel")
}
                                                

// @LINE:11
def newDriverPost(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts")
}
                                                

// @LINE:14
def rejectRequest(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/reject")
}
                                                

// @LINE:10
def deleteDriverPost(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/delete")
}
                                                

// @LINE:9
def driverPostsJson(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "driverposts")
}
                                                
    
}
                          

// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
class ReverseUserController {
    

// @LINE:32
def getDriverPosts(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/driverposts")
}
                                                

// @LINE:28
def users(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:31
def findByAuthToken(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users/token")
}
                                                

// @LINE:29
def registerUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:33
def getRiderPosts(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users/riderposts")
}
                                                

// @LINE:30
def setRegistrationId(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users/regid")
}
                                                
    
}
                          

// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:23
def deleteRiderPost(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "riderposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/delete")
}
                                                

// @LINE:22
def newRiderPost(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "riderposts")
}
                                                

// @LINE:8
def posts(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "posts")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:21
def riderPostsJson(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "riderposts")
}
                                                

// @LINE:24
def riderPostSearch(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "riderposts/search")
}
                                                
    
}
                          

// @LINE:26
// @LINE:25
class ReverseSearchController {
    

// @LINE:26
def acceptRide(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "riderposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/accept")
}
                                                

// @LINE:25
def offerRide(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "riderposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/offer")
}
                                                
    
}
                          

// @LINE:41
class ReverseNotificationController {
    

// @LINE:41
def sendData(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "gcm")
}
                                                
    
}
                          
}
                  


// @LINE:44
// @LINE:41
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers.javascript {

// @LINE:44
class ReverseAssets {
    

// @LINE:44
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              

// @LINE:36
// @LINE:35
class ReverseSecurityController {
    

// @LINE:36
def logout : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SecurityController.logout",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "logout"})
      }
   """
)
                        

// @LINE:35
def login : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SecurityController.login",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        
    
}
              

// @LINE:39
// @LINE:38
class ReverseTestController {
    

// @LINE:38
def test : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.TestController.test",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "test"})
      }
   """
)
                        

// @LINE:39
def test2 : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.TestController.test2",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "test2"})
      }
   """
)
                        
    
}
              

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseOfferController {
    

// @LINE:16
def rateRide : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.rateRide",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/rate"})
      }
   """
)
                        

// @LINE:12
def driverPostSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.driverPostSearch",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/search"})
      }
   """
)
                        

// @LINE:13
def riderRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.riderRequest",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/request"})
      }
   """
)
                        

// @LINE:15
def acceptRider : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.acceptRider",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/accept"})
      }
   """
)
                        

// @LINE:17
def rateRider : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.rateRider",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/raterider"})
      }
   """
)
                        

// @LINE:19
def transferMoney : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.transferMoney",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/pay"})
      }
   """
)
                        

// @LINE:18
def cancelRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.cancelRequest",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/cancel"})
      }
   """
)
                        

// @LINE:11
def newDriverPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.newDriverPost",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts"})
      }
   """
)
                        

// @LINE:14
def rejectRequest : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.rejectRequest",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/reject"})
      }
   """
)
                        

// @LINE:10
def deleteDriverPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.deleteDriverPost",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                        

// @LINE:9
def driverPostsJson : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.OfferController.driverPostsJson",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts"})
      }
   """
)
                        
    
}
              

// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
class ReverseUserController {
    

// @LINE:32
def getDriverPosts : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.getDriverPosts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/driverposts"})
      }
   """
)
                        

// @LINE:28
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.users",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:31
def findByAuthToken : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.findByAuthToken",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users/token"})
      }
   """
)
                        

// @LINE:29
def registerUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.registerUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:33
def getRiderPosts : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.getRiderPosts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users/riderposts"})
      }
   """
)
                        

// @LINE:30
def setRegistrationId : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.UserController.setRegistrationId",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users/regid"})
      }
   """
)
                        
    
}
              

// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:23
def deleteRiderPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deleteRiderPost",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                        

// @LINE:22
def newRiderPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.newRiderPost",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts"})
      }
   """
)
                        

// @LINE:8
def posts : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.posts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "posts"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:21
def riderPostsJson : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.riderPostsJson",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts"})
      }
   """
)
                        

// @LINE:24
def riderPostSearch : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.riderPostSearch",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts/search"})
      }
   """
)
                        
    
}
              

// @LINE:26
// @LINE:25
class ReverseSearchController {
    

// @LINE:26
def acceptRide : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SearchController.acceptRide",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/accept"})
      }
   """
)
                        

// @LINE:25
def offerRide : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.SearchController.offerRide",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "riderposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/offer"})
      }
   """
)
                        
    
}
              

// @LINE:41
class ReverseNotificationController {
    

// @LINE:41
def sendData : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.NotificationController.sendData",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "gcm"})
      }
   """
)
                        
    
}
              
}
        


// @LINE:44
// @LINE:41
// @LINE:39
// @LINE:38
// @LINE:36
// @LINE:35
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers.ref {

// @LINE:44
class ReverseAssets {
    

// @LINE:44
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          

// @LINE:36
// @LINE:35
class ReverseSecurityController {
    

// @LINE:36
def logout(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SecurityController.logout(), HandlerDef(this, "controllers.SecurityController", "logout", Seq(), "POST", """""", _prefix + """logout""")
)
                      

// @LINE:35
def login(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SecurityController.login(), HandlerDef(this, "controllers.SecurityController", "login", Seq(), "POST", """""", _prefix + """login""")
)
                      
    
}
                          

// @LINE:39
// @LINE:38
class ReverseTestController {
    

// @LINE:38
def test(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.TestController.test(), HandlerDef(this, "controllers.TestController", "test", Seq(), "POST", """""", _prefix + """test""")
)
                      

// @LINE:39
def test2(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.TestController.test2(), HandlerDef(this, "controllers.TestController", "test2", Seq(), "POST", """""", _prefix + """test2""")
)
                      
    
}
                          

// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
class ReverseOfferController {
    

// @LINE:16
def rateRide(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.rateRide(id), HandlerDef(this, "controllers.OfferController", "rateRide", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/rate""")
)
                      

// @LINE:12
def driverPostSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.driverPostSearch(), HandlerDef(this, "controllers.OfferController", "driverPostSearch", Seq(), "POST", """""", _prefix + """driverposts/search""")
)
                      

// @LINE:13
def riderRequest(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.riderRequest(id), HandlerDef(this, "controllers.OfferController", "riderRequest", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/request""")
)
                      

// @LINE:15
def acceptRider(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.acceptRider(id), HandlerDef(this, "controllers.OfferController", "acceptRider", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/accept""")
)
                      

// @LINE:17
def rateRider(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.rateRider(id), HandlerDef(this, "controllers.OfferController", "rateRider", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/raterider""")
)
                      

// @LINE:19
def transferMoney(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.transferMoney(id), HandlerDef(this, "controllers.OfferController", "transferMoney", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/pay""")
)
                      

// @LINE:18
def cancelRequest(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.cancelRequest(id), HandlerDef(this, "controllers.OfferController", "cancelRequest", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/cancel""")
)
                      

// @LINE:11
def newDriverPost(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.newDriverPost(), HandlerDef(this, "controllers.OfferController", "newDriverPost", Seq(), "POST", """""", _prefix + """driverposts""")
)
                      

// @LINE:14
def rejectRequest(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.rejectRequest(id), HandlerDef(this, "controllers.OfferController", "rejectRequest", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/reject""")
)
                      

// @LINE:10
def deleteDriverPost(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.deleteDriverPost(id), HandlerDef(this, "controllers.OfferController", "deleteDriverPost", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/delete""")
)
                      

// @LINE:9
def driverPostsJson(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.OfferController.driverPostsJson(), HandlerDef(this, "controllers.OfferController", "driverPostsJson", Seq(), "GET", """""", _prefix + """driverposts""")
)
                      
    
}
                          

// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:30
// @LINE:29
// @LINE:28
class ReverseUserController {
    

// @LINE:32
def getDriverPosts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.getDriverPosts(), HandlerDef(this, "controllers.UserController", "getDriverPosts", Seq(), "GET", """""", _prefix + """users/driverposts""")
)
                      

// @LINE:28
def users(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.users(), HandlerDef(this, "controllers.UserController", "users", Seq(), "GET", """""", _prefix + """users""")
)
                      

// @LINE:31
def findByAuthToken(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.findByAuthToken(), HandlerDef(this, "controllers.UserController", "findByAuthToken", Seq(), "POST", """""", _prefix + """users/token""")
)
                      

// @LINE:29
def registerUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.registerUser(), HandlerDef(this, "controllers.UserController", "registerUser", Seq(), "POST", """""", _prefix + """users""")
)
                      

// @LINE:33
def getRiderPosts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.getRiderPosts(), HandlerDef(this, "controllers.UserController", "getRiderPosts", Seq(), "GET", """""", _prefix + """users/riderposts""")
)
                      

// @LINE:30
def setRegistrationId(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.UserController.setRegistrationId(), HandlerDef(this, "controllers.UserController", "setRegistrationId", Seq(), "POST", """""", _prefix + """users/regid""")
)
                      
    
}
                          

// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:21
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:23
def deleteRiderPost(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deleteRiderPost(id), HandlerDef(this, "controllers.Application", "deleteRiderPost", Seq(classOf[Long]), "POST", """""", _prefix + """riderposts/$id<[^/]+>/delete""")
)
                      

// @LINE:22
def newRiderPost(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.newRiderPost(), HandlerDef(this, "controllers.Application", "newRiderPost", Seq(), "POST", """""", _prefix + """riderposts""")
)
                      

// @LINE:8
def posts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.posts(), HandlerDef(this, "controllers.Application", "posts", Seq(), "GET", """""", _prefix + """posts""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:21
def riderPostsJson(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.riderPostsJson(), HandlerDef(this, "controllers.Application", "riderPostsJson", Seq(), "GET", """""", _prefix + """riderposts""")
)
                      

// @LINE:24
def riderPostSearch(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.riderPostSearch(), HandlerDef(this, "controllers.Application", "riderPostSearch", Seq(), "POST", """""", _prefix + """riderposts/search""")
)
                      
    
}
                          

// @LINE:26
// @LINE:25
class ReverseSearchController {
    

// @LINE:26
def acceptRide(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SearchController.acceptRide(id), HandlerDef(this, "controllers.SearchController", "acceptRide", Seq(classOf[Long]), "POST", """""", _prefix + """riderposts/$id<[^/]+>/accept""")
)
                      

// @LINE:25
def offerRide(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.SearchController.offerRide(id), HandlerDef(this, "controllers.SearchController", "offerRide", Seq(classOf[Long]), "POST", """""", _prefix + """riderposts/$id<[^/]+>/offer""")
)
                      
    
}
                          

// @LINE:41
class ReverseNotificationController {
    

// @LINE:41
def sendData(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.NotificationController.sendData(), HandlerDef(this, "controllers.NotificationController", "sendData", Seq(), "GET", """""", _prefix + """gcm""")
)
                      
    
}
                          
}
                  
      