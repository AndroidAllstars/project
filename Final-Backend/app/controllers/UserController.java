package controllers;

import java.util.List;

import models.RiderPost;
import models.YRUser;
import models.DriverPost;

import org.codehaus.jackson.node.ObjectNode;

import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;

public class UserController extends Controller {
	
	static Form<YRUser> userForm = Form.form(YRUser.class);
	
	public static Result users() {
    	ObjectNode result = Json.newObject();
    	
    	result.put("success", true);
    	result.put("numberOfResults", YRUser.findAllUsers().size());
    	
    	result.put("results", Json.toJson(YRUser.findAllUsers()));
    	return ok(result);
    }
	
	@With(SecurityController.class)
	public static Result getDriverPosts() {
		ObjectNode result = Json.newObject();
		YRUser user = SecurityController.getUser();
		
		List<DriverPost> list = user.driverPosts;
		result.put("success", true);
		result.put("numberOfResults", list.size());
		result.put("results", Json.toJson(list));
		
		return ok(result);
	}
	
	@With(SecurityController.class)
	public static Result getRiderPosts() {
		ObjectNode result = Json.newObject();
		YRUser user = SecurityController.getUser();
		
		List<RiderPost> list = user.riderPosts;
		result.put("success", true);
		result.put("numberOfResults", list.size());
		result.put("results", Json.toJson(list));
		
		return ok(result);
	}
    
    public static Result registerUser() {
    	ObjectNode result = Json.newObject();
    	Form<YRUser> filledForm = userForm.bindFromRequest();
    	YRUser user = filledForm.get();
    	
    	if(filledForm.hasErrors()) {
    		result.put("success", false);
    		result.put("errorMessage", filledForm.errorsAsJson());
    		return badRequest(result);
    	}
    	
    	if(YRUser.getYRUser(user.getUname()) != null) {
    		result.put("success", false);
    		result.put("errorMessage", "User with that username already exists.");
    		return badRequest(result);
    	}
    	
    	//looks redundant but the setPass function contains the SHA-512 function call
    	user.setPass(user.getPass());
    	YRUser.create(user);
    	result.put("success", true);
    	
    	return ok(result);
    }
    
    public static Result loginUser() {
    	ObjectNode result = Json.newObject();
    	DynamicForm form = null;
    
    	if(DynamicForm.form() != null) {
    		form = DynamicForm.form().bindFromRequest();
    	} else {
    		result.put("success", false);
    		result.put("errorMessage", "Empty login form.");
    		return badRequest(result);
    	}
    	
    	if(form.get("uname") == null || form.get("pass") == null) {
    		result.put("success", false);
    		result.put("errorMessage", "Missing either username or password.");
    		return badRequest(result);
    	}
    	
    	/*if(form.get("registration_id") == null) {
    		result.put("success", false);
    		result.put("errorMessage", "Missing registration_id");
    		return badRequest(result);
    	} */
    	
    	String username = form.get("uname");
    	String password = form.get("pass");
    	//String registration_id = form.get("registration_id");
    	
    	YRUser user = YRUser.findByUsernameAndPassword(username, password);
    	if(user != null) {
    		//user.setRegistration_id(registration_id);
    		result.put("success", true);
    		return ok(result);
    	} else {
    		result.put("success", false);
    		return badRequest(result);
    	}
    }
    
    public static Result findByAuthToken() {
    	DynamicForm filledForm = DynamicForm.form().bindFromRequest();
    	YRUser user = YRUser.findByAuthToken(filledForm.get("authToken"));
    	return ok(Json.toJson(user));
    }
    
    public static Result setRegistrationId() {
    	ObjectNode result = Json.newObject();
    	
    	DynamicForm filledForm = DynamicForm.form().bindFromRequest();
    	//YRUser user = SecurityController.getUser();
    	YRUser user;
    	if(request().getHeader("X-Auth-Token") == null) {
    		user = YRUser.findByUsername("raymond");
    	} else {
    		user = YRUser.findByAuthToken(request().getHeader("X-Auth-Token").toString());
    		if(user == null) {
    			result.put("success", false);
    			result.put("errorMessage", "Invalid auth token");
    			return unauthorized(result);
    		}
    	}
    	
    	if(filledForm.get("regId") == null) {
    		result.put("success", false);
    		result.put("errorMessage", "Registration_id was not found.");
    		return badRequest(result);
    	}
    	String registration_id = filledForm.get("regId");
    	user.setRegistration_id(registration_id);
    	user.save();
    	
    	result.put("success", true);
    	return ok(result);
    }
}
