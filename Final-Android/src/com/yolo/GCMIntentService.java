/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yolo;

import static com.yolo.CommonUtilities.SENDER_ID;
import static com.yolo.CommonUtilities.displayMessage;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.yolo.gcm.DemoActivity;
import com.yolo.gcm.ServerUtilities;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {
	PendingIntent pi;
	int num;
    @SuppressWarnings("hiding")
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(SENDER_ID);
        num = 0;
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        displayMessage(context, getString(R.string.gcm_registered));
        ServerUtilities.register(context, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        if (GCMRegistrar.isRegisteredOnServer(context)) {
            ServerUtilities.unregister(context, registrationId);
        } else {
            // This callback results from the call to unregister made on
            // ServerUtilities when the registration to the server failed.
            Log.i(TAG, "Ignoring unregister callback");
        }
        
    }

    @Override
    protected void onMessage(Context context, Intent intent) {    	
    	
        Log.i(TAG, "Received message -------------------");        
        Log.d(TAG, "Bundle Received : " + intent.getExtras().toString());
    	      
        String grabbedID = intent.getExtras().getString("id");    
        
        String notifyType = intent.getExtras().getString("type");    	
        String personalizedMessage = "";
        
        if (notifyType.equals("accept_rider"))
        	personalizedMessage = "You've been accepted to ride with me";        
        else if (notifyType.equals("rider_request"))
        	personalizedMessage = "I want to ride with you";
        else if (notifyType.equals("invite_rider"))
        	{ personalizedMessage = "I'm inviting you to ride with me";     
        		//start fragment to accept ride?
        	Intent i = new Intent(context, checkin.class);
        	i.putExtra("invited", true);
        	num++;
     		pi = PendingIntent.getActivity(context, 0, i, num);
        	
        	}
        else if (notifyType.equals("invite_accept"))
        	personalizedMessage = "I accept your invite to ride with you";
        else if (notifyType.equals("invite_decline"))
        	personalizedMessage = "I accept your invite to ride with you";
        else if (notifyType.equals("payment"))
        	personalizedMessage = "You got paid";
        else
        	personalizedMessage = "TROUBLE IN PARADISE";
        
        String message = "From GCM... From ID number " + grabbedID + "... " + personalizedMessage;               
        
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message, pi);
               
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message, pi);
    }

    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, PendingIntent pi) {
    	//System.out.println("messagess is = " + message);
    	//System.out.println("messagess is = " + message);
    	//System.out.println("messagess is = " + message);
    	
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification.Builder(context)
	       .setContentTitle("New Message on YoloRide")
	        .setContentText(message)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setContentIntent(pi)
	        .addAction(R.drawable.ic_launcher, "Accept", pi).build(); 
 		notification.flags |= Notification.FLAG_AUTO_CANCEL;
        String title = context.getString(R.string.app_name);
        Intent notificationIntent = new Intent(context, DemoActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify(0, notification);
        //String notification.toString();
    }

}
