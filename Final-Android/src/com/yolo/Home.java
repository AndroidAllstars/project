

package com.yolo;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import com.yolo.fragments.HomePostViewerFragment;


import android.os.Bundle;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ListAdapter;
import android.widget.ArrayAdapter;
import android.widget.Toast;

public class Home extends Activity {
	Button			camera_button;
	/*ImageButton     example_button;
	ImageButton  	how_button; //*/
	Button 			postOfferBtn;
	Button 			searchBtn;
	Button			postWantedBtn;
	Button			editBtn;
	ListView 		listOfRides;
	Button 			tempBtn;
	TextView		tempTxt;
	EditText		startSearchETxt;
	EditText		endSearchETxt;
	String			startSearchStr;
	String			endSearchStr;
	String			offerReqStr;
	CPostOffer 		tempPostOffer;
	boolean 		hindi;
	final int SIZE = CPost.getSize();
	@Override
	protected void onPause() {
		super.onPause();
		onResume();
	}
	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("home has been resumed.........................");
	    YoloRide appState = ((YoloRide)getApplicationContext());
	   if (Locale.getDefault().getDisplayLanguage().equals("Hindi"))
		{
			Button postOfferBtn = (Button) findViewById(R.id.btnOfferHome);
			postOfferBtn.setText("सवारी प्रस्ताव पोस्ट");
			Button searchBtn = (Button) findViewById(R.id.btnSearchHome);
			searchBtn.setText("खोज");
			Button postRequestBtn = (Button) findViewById(R.id.btnWantedHome);
			postRequestBtn.setText("सवारी अनुरोध पोस्ट");
			Button editBtn = (Button) findViewById(R.id.btnEditHome);
			editBtn.setText("सवारी प्रस्ताव पोस्ट");
			EditText starter = (EditText) findViewById(R.id.eTxtStartHome);
			starter.setHint("स्थान शुरू");
			EditText ender = (EditText) findViewById(R.id.eTxtEndHome);
			ender.setHint("अंत स्थान");
		}
		else
		{
			Button postOfferBtn = (Button) findViewById(R.id.btnOfferHome);
			postOfferBtn.setText("Post Ride Offer");
			Button searchBtn = (Button) findViewById(R.id.btnSearchHome);
			searchBtn.setText("Search");
			Button postRequestBtn = (Button) findViewById(R.id.btnWantedHome);
			postRequestBtn.setText("Post Ride Wanted");
			Button editBtn = (Button) findViewById(R.id.btnEditHome);
			editBtn.setText("Edit Profile");
			EditText starter = (EditText) findViewById(R.id.eTxtStartHome);
			starter.setHint("start point");
			EditText ender = (EditText) findViewById(R.id.eTxtEndHome);
			ender.setHint("end point");
		}
	    
	    final ListView listview = (ListView) findViewById(R.id.listOfRidesHome);
	    String[] values = new String[] { tempPostOffer.toString()};
	    ArrayList<CPostOffer> list = new ArrayList<CPostOffer>();
    //	System.out.println("LIST SIZE IS : " + list.size());
	    StringBuilder	     builder = new StringBuilder();

	    try {
	    //here is where the json needs to be retrieved (GET) from the server and a builder made from it (?)
			
	    HttpGet httpget = new HttpGet("http://protected-meadow-2488.herokuapp.com/users/driverposts");
	    HttpClient httpclient = new DefaultHttpClient();
		
	    
	      HttpContext localContext = appState.getContext();

		      System.out.println("HOME SAYS THE COOKIE STORE IS: "  + localContext.getAttribute( ClientContext.COOKIE_STORE));
		 //     String cookieToken = (String) localContext.getAttribute( ClientContext.COOKIE_STORE);
		      CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
		      System.out.println("HERE ARE SOME COOKIES: " + store.getCookies().get(0));
		      Cookie chocolate = store.getCookies().get(0);
		      System.out.println("THE VALUE IS " + chocolate.getValue());
	      httpget.setHeader("X-AUTH-TOKEN", chocolate.getValue());
	    HttpResponse response = httpclient.execute(httpget, localContext);
	    
	    HttpEntity entity = response.getEntity();
	    Integer statusCode = response.getStatusLine().getStatusCode();
	    InputStream content = entity.getContent();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	    String line = reader.readLine();
	    builder.append(line);
	    Translate translate = new Translate();
	    CPostOffer tempPO = new CPostOffer();
	    list =  translate.action(new JSONObject(builder.toString()), tempPO);
	    System.out.println("TEMP POST MADE");
	    list.add(tempPO);
	    System.out.println("STRING BUILDERED");

				} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    final StableArrayAdapter adapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, list);
	    System.out.println("MADE THE ARRAY ADAPTER");
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final CPostOffer item = (CPostOffer) parent.getItemAtPosition(position);
	        Toast.makeText(getApplicationContext(),
	        	      "Items start point is " + item.startPoint, Toast.LENGTH_LONG)
	        	      .show();
	        showPostView(item);
	      }

	    });//*/
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		tempPostOffer = new CPostOffer("start", "end", "12:00", "12/12/12", false, false, false, 99, 3);
		System.out.println("JUST STARTING OFF");
	/*	listOfRides = (ListView) findViewById(R.id.listOfRidesHome);
		//tempBtn = (Button) findViewById(R.id.btnWantedHome);
	//	tempTxt.setText("hello this is dog");
		Context ctx = this;
		tempPostOffer = new CPostOffer("start", "end", "12:00", "12/12/12", false, false, false, 99.99, 3);
		ArrayAdapter<CPostOffer> adapter = new ArrayAdapter<CPostOffer>(ctx, R.id.textView1);
		System.out.println("this CPost says...................  : " + tempPostOffer);
		adapter.add(tempPostOffer);
		listOfRides.setAdapter(adapter);
		//*/

		postOfferBtn = (Button) findViewById(R.id.btnOfferHome);
		postOfferBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.PostOffer");
			startActivity(i);
			}
	    }); //*/
		
		postWantedBtn = (Button) findViewById(R.id.btnWantedHome);
		postWantedBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.PostRequest");
			startActivity(i);
			}
	    }); //*/

		editBtn = (Button) findViewById(R.id.btnEditHome);
		editBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	    YoloRide appState = ((YoloRide)getApplicationContext());

	    	  boolean hindi2 = appState.getHindi();
		      appState.setHindi(!hindi2);
		      onResume();
	      }
		});
		
		searchBtn = (Button) findViewById(R.id.btnSearchHome);
		startSearchETxt = (EditText) findViewById(R.id.eTxtStartHome);
		endSearchETxt = (EditText) findViewById(R.id.eTxtEndHome);
		searchBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.Search");
			//i.putExtra("test", "WORKING");
			startSearchStr = startSearchETxt.getText().toString();
			endSearchStr   =   endSearchETxt.getText().toString();
			offerReqStr   = "r";
			i.putExtra("start", startSearchStr);
			i.putExtra("end",     endSearchStr);
			i.putExtra("o/r",     offerReqStr);
			startActivity(i);
			}
	    }); //*/
		
	    final ListView listview = (ListView) findViewById(R.id.listOfRidesHome);
	    //String[] values = new String[] { tempPostOffer.toString()};
	    ArrayList<CPostOffer> list = new ArrayList<CPostOffer>();
    //	System.out.println("LIST SIZE IS : " + list.size());
	    StringBuilder	     builder = new StringBuilder();

	    try {
	    //here is where the json needs to be retrieved (GET) from the server and a builder made from it (?)
			
	    HttpGet httpget = new HttpGet("http://protected-meadow-2488.herokuapp.com/users/driverposts");
	    HttpClient httpclient = new DefaultHttpClient();
		
	    
	    YoloRide appState = ((YoloRide)getApplicationContext());
	      HttpContext localContext = appState.getContext();

		      System.out.println("HOME SAYS THE COOKIE STORE IS: "  + localContext.getAttribute( ClientContext.COOKIE_STORE));
		 //     String cookieToken = (String) localContext.getAttribute( ClientContext.COOKIE_STORE);
		      CookieStore store = (CookieStore) localContext.getAttribute( ClientContext.COOKIE_STORE);
		      System.out.println("HERE ARE SOME COOKIES: " + store.getCookies().get(0));
		      Cookie chocolate = store.getCookies().get(0);
		      System.out.println("THE VALUE IS " + chocolate.getValue());
	      httpget.setHeader("X-AUTH-TOKEN", chocolate.getValue());
	    HttpResponse response = httpclient.execute(httpget, localContext);
	    
	    HttpEntity entity = response.getEntity();
	    Integer statusCode = response.getStatusLine().getStatusCode();
	    InputStream content = entity.getContent();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(content));
	    String line = reader.readLine();
	    builder.append(line);
	    Translate translate = new Translate();
	    CPostOffer tempPO = new CPostOffer();
		
	    list =  translate.action(new JSONObject(builder.toString()), tempPO);
	    System.out.println("TEMP POST MADE");
	    list.add(tempPO);
	    System.out.println("STRING BUILDERED");

				} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    System.out.println("TRANSLATED");

	    final StableArrayAdapter adapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, list);
	    System.out.println("MADE THE ARRAY ADAPTER");
	    listview.setAdapter(adapter);

	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final CPostOffer item = (CPostOffer) parent.getItemAtPosition(position);
	        showPostView(item);
	      }

	    });//*/
	    
	}

    void showPostView(CPostOffer postIn) {
        //mStackLevel++;

        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        System.out.println("ABOUT TO START THE DIALOG");
        DialogFragment newFragment = HomePostViewerFragment.newInstance(postIn);
        newFragment.show(ft, "dialog");
    }
	  private class StableArrayAdapter extends ArrayAdapter<CPostOffer> {

	    HashMap<CPostOffer, Integer> mIdMap = new HashMap<CPostOffer, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<CPostOffer> objects) {
	      super(context, textViewResourceId, objects);
	    	System.out.println("SUPER'D");
	    	System.out.println("OBJECTS.SIZE = " + objects.size());
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	    	System.out.println("LISTING");
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	    	CPostOffer item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
	  
}


