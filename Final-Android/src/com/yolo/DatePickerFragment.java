package com.yolo;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

public class DatePickerFragment extends DialogFragment                            
implements DatePickerDialog.OnDateSetListener {
	String DATE = "date";
	String dateStr;
	private OnCompleteListener mListener;
	EditText eTxtDepTime;
public interface OnCompleteListener {
    public abstract void onComplete(String date, String type);
}


@Override
public void onAttach(Activity activity) {
	super.onAttach(activity);
    try {
        this.mListener = (OnCompleteListener)activity;
    }
    catch (final ClassCastException e) {
        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
    }
} 
//*/

@Override
public Dialog onCreateDialog(Bundle savedInstanceState) {
// Use the current time as the default values for the picker
final Calendar c = Calendar.getInstance();
int year = c.get(Calendar.YEAR);
int month = c.get(Calendar.MONTH);
int day = c.get(Calendar.DAY_OF_MONTH);

// Create a new instance of DatePickerDialog and return it
return new DatePickerDialog(getActivity(), this, year, month, day);

}

public void onDateSet(DatePicker view, int year, int month, int day) {
	//dateStr = month + "/" + day + "/" + year;
	month++;
	dateStr = "";
	if(month < 10)
		dateStr += "0";
	 if(day < 10)
		 dateStr += month + "/0" + day + "/" +  year;
	 else
		 dateStr += month + "/" + day + "/" + year;
	 //eTxtDepTime = (EditText) getActivity().findViewById(R.id.eTxtDepTimePO);
	 //eTxtDepTime.setText("hi hi");
	System.out.println("dateSet complete");
	 this.mListener.onComplete(dateStr, DATE);
}


}