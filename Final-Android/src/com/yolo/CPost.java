package com.yolo;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;

import android.text.format.Time;

abstract public class CPost {
	
	long id;
	
	String startPoint;
	String endPoint;
	String depTime;
	String depDate;
	String poster;
	// add in double fee;
	// add in seats later;
	boolean pickup;
	boolean pets;
	boolean handicap;
	final static int SIZE = 15;
	Calendar cal = Calendar.getInstance();
	
	public CPost(){
		Date today = cal.getTime();
		Time now = new Time();
		now.setToNow();
		String dateStr = dateStr = "";
		{if(now.month < 10)
			dateStr += "0";
		 if(now.monthDay < 10)
			 dateStr += (now.month + 1) + "/0" + now.monthDay + "/" +  now.year;
		 else
			 dateStr += (now.month + 1) + "/" + now.monthDay + "/" + now.year;
		}
		String timeStr = "";
		{
			if(now.hour < 10)
				timeStr += "0";
			 if(now.minute < 10)
				 timeStr += now.hour + ":0" + now.minute;
			 else
				 timeStr += now.hour + ":" + now.minute;
		}
		System.out.println("THE DEFAULT DATE IS : " + dateStr);
		System.out.println("THE DEFAULT TIME IS : " + timeStr);
		 startPoint = "a";
		 endPoint 	= "b";
		 poster = "default";
		 depTime 	= timeStr;
		 depDate 	= dateStr;
		// add in double fee;
		// add in seats later;
		 pickup 	= false;
		 pets 		= false;
		 handicap 	= false;
	}
	
	
	public static int getSize(){
		return(SIZE);
	}
	public CPost(String startPointIn, String endPointIn, String depTimeIn, String depDateIn, boolean pickupIn, boolean petsIn, boolean handicapIn) {
		 poster = "you";

		 startPoint = startPointIn;
		 endPoint 	= endPointIn;
		 depTime 	= depTimeIn;
		 depDate 	= depDateIn;
		// add in double fee;
		// add in seats later;
		 pickup 	= pickupIn;
		 pets 		= petsIn;
		 handicap 	= handicapIn;
	}

	public CPost(String posterIn, String startPointIn, String endPointIn, String depTimeIn, String depDateIn, boolean pickupIn, boolean petsIn, boolean handicapIn) {
		 poster 	= posterIn;
		 startPoint = startPointIn;
		 endPoint 	= endPointIn;
		 depTime 	= depTimeIn;
		 depDate 	= depDateIn;
		// add in double fee;
		// add in seats later;
		 pickup 	= pickupIn;
		 pets 		= petsIn;
		 handicap 	= handicapIn;
	}

	public void setPoster(String userName){
		poster = userName;
	}
	
	public String getPoster(){
		return(poster);
	}
	public String getStart(){
		return startPoint;
	}

	public long getID()
	{
		return id;
	}
	
	public void setID(long myID)
	{
		id = myID;
	}
	
	public String getEnd(){
		return endPoint;
	}
	public String getTime(){
		return depTime;
	}
	public String getDate(){
		return depDate;
	}
	public boolean getPickup(){
		return pickup;
	}
	public boolean getPets(){
		return pets;
	}
	public boolean getCrip(){
		return handicap;
	}
	
	public String toString(){
		String outStr;
		if(startPoint.equals(null))
			outStr = "End of list!";
		else
			outStr = "Ride on " + depDate + " at " + depTime + "\nFrom " + startPoint + " to " + endPoint;
		return outStr;
	}
	
	abstract List<NameValuePair> submit();
	
}
