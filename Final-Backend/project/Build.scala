import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "YoloRide"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
    "org.mindrot" % "jbcrypt" % "0.3m",
    "com.google.android.gcm" % "gcm-server" % "1.0.2"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += "GCM Server Repository" at "https://raw.github.com/slorber/gcm-server-repository/master/releases/"     
  )

}
