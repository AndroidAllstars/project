package com.yolo;

public interface OnCompleteListener {
    public abstract void onComplete(String time);
}
