package com.yolo;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment
implements TimePickerDialog.OnTimeSetListener{
	String timeStr;
	final String TIME = "time";
	private OnCompleteListener mListener;
	EditText eTxtDepTime;
public interface OnCompleteListener {
    public abstract void onComplete(String time, String type);
}


@Override
public void onAttach(Activity activity) {
	super.onAttach(activity);
    try {
        this.mListener = (OnCompleteListener)activity;
    }
    catch (final ClassCastException e) {
        throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
    }
} 
//*/

@Override
public Dialog onCreateDialog(Bundle savedInstanceState) {
// Use the current time as the default values for the picker
final Calendar c = Calendar.getInstance();
int hour = c.get(Calendar.HOUR_OF_DAY);
int minute = c.get(Calendar.MINUTE);

// Create a new instance of TimePickerDialog and return it
return new TimePickerDialog(getActivity(), this, hour, minute,
DateFormat.is24HourFormat(getActivity()));

}

public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	timeStr = "";
	if(hourOfDay < 10)
		timeStr += "0";
	 if(minute < 10)
		 timeStr += hourOfDay + ":0" + minute;
	 else
		 timeStr += hourOfDay + ":" + minute;
	 //eTxtDepTime = (EditText) getActivity().findViewById(R.id.eTxtDepTimePO);
	 //eTxtDepTime.setText("hi hi");
	 this.mListener.onComplete(timeStr, TIME);
}


}