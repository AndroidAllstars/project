// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:139fbc5a514ca31fca2be2c4620ddac11d6d3f90
// @DATE:Mon May 20 05:21:57 PDT 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix  
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" } 


// @LINE:6
private[this] lazy val controllers_Application_index0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
        

// @LINE:8
private[this] lazy val controllers_Application_posts1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("posts"))))
        

// @LINE:9
private[this] lazy val controllers_Application_newBasePost2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("posts"))))
        

// @LINE:10
private[this] lazy val controllers_Application_deletePost3 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("posts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/delete"))))
        

// @LINE:11
private[this] lazy val controllers_Application_driverPostsJson4 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts"))))
        

// @LINE:12
private[this] lazy val controllers_Application_driverPostsJson5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/json"))))
        

// @LINE:13
private[this] lazy val controllers_Application_deleteDriverPost6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/"),DynamicPart("id", """[^/]+""",true),StaticPart("/delete"))))
        

// @LINE:14
private[this] lazy val controllers_Application_newDriverPost7 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts"))))
        

// @LINE:15
private[this] lazy val controllers_Application_search8 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("driverposts/search"))))
        

// @LINE:17
private[this] lazy val controllers_Application_users9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:18
private[this] lazy val controllers_Application_registerUser10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:19
private[this] lazy val controllers_Application_loginUser11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users/login"))))
        

// @LINE:21
private[this] lazy val controllers_Application_auth12 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login/auth"))))
        

// @LINE:22
private[this] lazy val controllers_Application_verify13 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login/verify"))))
        

// @LINE:24
private[this] lazy val controllers_Assets_at14 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.index()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """posts""","""controllers.Application.posts()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """posts""","""controllers.Application.newBasePost()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """posts/$id<[^/]+>/delete""","""controllers.Application.deletePost(id:Long)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts""","""controllers.Application.driverPostsJson()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/json""","""controllers.Application.driverPostsJson()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/$id<[^/]+>/delete""","""controllers.Application.deleteDriverPost(id:Long)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts""","""controllers.Application.newDriverPost()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """driverposts/search""","""controllers.Application.search()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.Application.users()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""controllers.Application.registerUser()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users/login""","""controllers.Application.loginUser()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login/auth""","""controllers.Application.auth()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login/verify""","""controllers.Application.verify()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
       
    
def routes:PartialFunction[RequestHeader,Handler] = {        

// @LINE:6
case controllers_Application_index0(params) => {
   call { 
        invokeHandler(controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Nil,"GET", """ Home page""", Routes.prefix + """"""))
   }
}
        

// @LINE:8
case controllers_Application_posts1(params) => {
   call { 
        invokeHandler(controllers.Application.posts(), HandlerDef(this, "controllers.Application", "posts", Nil,"GET", """""", Routes.prefix + """posts"""))
   }
}
        

// @LINE:9
case controllers_Application_newBasePost2(params) => {
   call { 
        invokeHandler(controllers.Application.newBasePost(), HandlerDef(this, "controllers.Application", "newBasePost", Nil,"POST", """""", Routes.prefix + """posts"""))
   }
}
        

// @LINE:10
case controllers_Application_deletePost3(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.deletePost(id), HandlerDef(this, "controllers.Application", "deletePost", Seq(classOf[Long]),"POST", """""", Routes.prefix + """posts/$id<[^/]+>/delete"""))
   }
}
        

// @LINE:11
case controllers_Application_driverPostsJson4(params) => {
   call { 
        invokeHandler(controllers.Application.driverPostsJson(), HandlerDef(this, "controllers.Application", "driverPostsJson", Nil,"GET", """""", Routes.prefix + """driverposts"""))
   }
}
        

// @LINE:12
case controllers_Application_driverPostsJson5(params) => {
   call { 
        invokeHandler(controllers.Application.driverPostsJson(), HandlerDef(this, "controllers.Application", "driverPostsJson", Nil,"GET", """""", Routes.prefix + """driverposts/json"""))
   }
}
        

// @LINE:13
case controllers_Application_deleteDriverPost6(params) => {
   call(params.fromPath[Long]("id", None)) { (id) =>
        invokeHandler(controllers.Application.deleteDriverPost(id), HandlerDef(this, "controllers.Application", "deleteDriverPost", Seq(classOf[Long]),"POST", """""", Routes.prefix + """driverposts/$id<[^/]+>/delete"""))
   }
}
        

// @LINE:14
case controllers_Application_newDriverPost7(params) => {
   call { 
        invokeHandler(controllers.Application.newDriverPost(), HandlerDef(this, "controllers.Application", "newDriverPost", Nil,"POST", """""", Routes.prefix + """driverposts"""))
   }
}
        

// @LINE:15
case controllers_Application_search8(params) => {
   call { 
        invokeHandler(controllers.Application.search(), HandlerDef(this, "controllers.Application", "search", Nil,"POST", """""", Routes.prefix + """driverposts/search"""))
   }
}
        

// @LINE:17
case controllers_Application_users9(params) => {
   call { 
        invokeHandler(controllers.Application.users(), HandlerDef(this, "controllers.Application", "users", Nil,"GET", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:18
case controllers_Application_registerUser10(params) => {
   call { 
        invokeHandler(controllers.Application.registerUser(), HandlerDef(this, "controllers.Application", "registerUser", Nil,"POST", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:19
case controllers_Application_loginUser11(params) => {
   call { 
        invokeHandler(controllers.Application.loginUser(), HandlerDef(this, "controllers.Application", "loginUser", Nil,"POST", """""", Routes.prefix + """users/login"""))
   }
}
        

// @LINE:21
case controllers_Application_auth12(params) => {
   call { 
        invokeHandler(controllers.Application.auth(), HandlerDef(this, "controllers.Application", "auth", Nil,"GET", """""", Routes.prefix + """login/auth"""))
   }
}
        

// @LINE:22
case controllers_Application_verify13(params) => {
   call { 
        invokeHandler(controllers.Application.verify(), HandlerDef(this, "controllers.Application", "verify", Nil,"GET", """""", Routes.prefix + """login/verify"""))
   }
}
        

// @LINE:24
case controllers_Assets_at14(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}
    
}
        