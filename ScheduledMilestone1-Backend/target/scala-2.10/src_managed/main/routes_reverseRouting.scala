// @SOURCE:/Users/raymondlau/Projects/yolorideplay/conf/routes
// @HASH:139fbc5a514ca31fca2be2c4620ddac11d6d3f90
// @DATE:Mon May 20 05:21:57 PDT 2013

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._
import java.net.URLEncoder

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers {

// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:9
def newBasePost(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "posts")
}
                                                

// @LINE:17
def users(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:14
def newDriverPost(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts")
}
                                                

// @LINE:22
def verify(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login/verify")
}
                                                

// @LINE:8
def posts(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "posts")
}
                                                

// @LINE:6
def index(): Call = {
   Call("GET", _prefix)
}
                                                

// @LINE:15
def search(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/search")
}
                                                

// @LINE:18
def registerUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:19
def loginUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "users/login")
}
                                                

// @LINE:21
def auth(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "login/auth")
}
                                                

// @LINE:13
def deleteDriverPost(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "driverposts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/delete")
}
                                                

// @LINE:12
// @LINE:11
def driverPostsJson(): Call = {
   () match {
// @LINE:11
case () if true => Call("GET", _prefix + { _defaultPrefix } + "driverposts")
                                                        
// @LINE:12
case () if true => Call("GET", _prefix + { _defaultPrefix } + "driverposts/json")
                                                        
   }
}
                                                

// @LINE:10
def deletePost(id:Long): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "posts/" + implicitly[PathBindable[Long]].unbind("id", id) + "/delete")
}
                                                
    
}
                          

// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          
}
                  


// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers.javascript {

// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:9
def newBasePost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.newBasePost",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "posts"})
      }
   """
)
                        

// @LINE:17
def users : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.users",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:14
def newDriverPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.newDriverPost",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts"})
      }
   """
)
                        

// @LINE:22
def verify : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.verify",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login/verify"})
      }
   """
)
                        

// @LINE:8
def posts : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.posts",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "posts"})
      }
   """
)
                        

// @LINE:6
def index : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.index",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + """"})
      }
   """
)
                        

// @LINE:15
def search : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.search",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/search"})
      }
   """
)
                        

// @LINE:18
def registerUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.registerUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:19
def loginUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.loginUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "users/login"})
      }
   """
)
                        

// @LINE:21
def auth : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.auth",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "login/auth"})
      }
   """
)
                        

// @LINE:13
def deleteDriverPost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deleteDriverPost",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                        

// @LINE:12
// @LINE:11
def driverPostsJson : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.driverPostsJson",
   """
      function() {
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts"})
      }
      if (true) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "driverposts/json"})
      }
      }
   """
)
                        

// @LINE:10
def deletePost : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.deletePost",
   """
      function(id) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "posts/" + (""" + implicitly[PathBindable[Long]].javascriptUnbind + """)("id", id) + "/delete"})
      }
   """
)
                        
    
}
              

// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              
}
        


// @LINE:24
// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
package controllers.ref {

// @LINE:22
// @LINE:21
// @LINE:19
// @LINE:18
// @LINE:17
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:10
// @LINE:9
// @LINE:8
// @LINE:6
class ReverseApplication {
    

// @LINE:9
def newBasePost(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.newBasePost(), HandlerDef(this, "controllers.Application", "newBasePost", Seq(), "POST", """""", _prefix + """posts""")
)
                      

// @LINE:17
def users(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.users(), HandlerDef(this, "controllers.Application", "users", Seq(), "GET", """""", _prefix + """users""")
)
                      

// @LINE:14
def newDriverPost(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.newDriverPost(), HandlerDef(this, "controllers.Application", "newDriverPost", Seq(), "POST", """""", _prefix + """driverposts""")
)
                      

// @LINE:22
def verify(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.verify(), HandlerDef(this, "controllers.Application", "verify", Seq(), "GET", """""", _prefix + """login/verify""")
)
                      

// @LINE:8
def posts(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.posts(), HandlerDef(this, "controllers.Application", "posts", Seq(), "GET", """""", _prefix + """posts""")
)
                      

// @LINE:6
def index(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.index(), HandlerDef(this, "controllers.Application", "index", Seq(), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:15
def search(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.search(), HandlerDef(this, "controllers.Application", "search", Seq(), "POST", """""", _prefix + """driverposts/search""")
)
                      

// @LINE:18
def registerUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.registerUser(), HandlerDef(this, "controllers.Application", "registerUser", Seq(), "POST", """""", _prefix + """users""")
)
                      

// @LINE:19
def loginUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.loginUser(), HandlerDef(this, "controllers.Application", "loginUser", Seq(), "POST", """""", _prefix + """users/login""")
)
                      

// @LINE:21
def auth(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.auth(), HandlerDef(this, "controllers.Application", "auth", Seq(), "GET", """""", _prefix + """login/auth""")
)
                      

// @LINE:13
def deleteDriverPost(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deleteDriverPost(id), HandlerDef(this, "controllers.Application", "deleteDriverPost", Seq(classOf[Long]), "POST", """""", _prefix + """driverposts/$id<[^/]+>/delete""")
)
                      

// @LINE:11
def driverPostsJson(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.driverPostsJson(), HandlerDef(this, "controllers.Application", "driverPostsJson", Seq(), "GET", """""", _prefix + """driverposts""")
)
                      

// @LINE:10
def deletePost(id:Long): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.deletePost(id), HandlerDef(this, "controllers.Application", "deletePost", Seq(classOf[Long]), "POST", """""", _prefix + """posts/$id<[^/]+>/delete""")
)
                      
    
}
                          

// @LINE:24
class ReverseAssets {
    

// @LINE:24
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          
}
                  
      