package models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonView;
import org.joda.time.DateTime;

import com.avaje.ebean.ExpressionList;

import play.db.ebean.Model.Finder;

@Entity
//@Table(name="DPOST")
public class DriverPost extends BasePost {
	
	public Integer maxSeats;
	public Integer seatsOpen;
	public Double reqFee;
	
	public Double escrow;
	
	public Integer getNumRequests() {
		return requestedRiders.size();
	}
	
	public Integer getSeatsOpen() {
		if(maxSeats != null) {
			return(maxSeats - riders.size());
		} else {
			return null;
		}
	}
	
	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "driver_post_riders")
	public Set<YRUser> riders = new HashSet<YRUser>();
	
	@ManyToMany
	@JsonIgnore
	@JoinTable(name = "driver_post_req")
	public Set<YRUser> requestedRiders = new HashSet<YRUser>();
	
	@OneToMany(mappedBy = "post")
	public Set<OfferFeedback> feedback = new HashSet<OfferFeedback>();
	
	@OneToMany(mappedBy = "post")
	public Set<Payment> payments = new HashSet<Payment>();
	
	/*public Set<String> getUserFeedback() {
		Set<String> userFeedback = new HashSet<String>();
		for(OfferFeedback offerFeedback : feedback) {
			userFeedback.add(offerFeedback.rater.getUname());
		}
		return userFeedback;
	}*/
	
	public Set<String> getRidersList() {
		Set<String> ridersList = new HashSet<String>(riders.size());
		for(YRUser rider : riders) {
			ridersList.add(rider.getUname());
		}
		return ridersList;
	} 
	
	public Set<String> getRequestedList() {
		Set<String> ridersList = new HashSet<String>(riders.size());
		for(YRUser rider : requestedRiders) {
			ridersList.add(rider.getUname());
		}
		return ridersList;
	} 
	
	public static Finder<Long,DriverPost> findDriverPost = new Finder(
	   Long.class, DriverPost.class
	);

	public static List<DriverPost> allDriverPost() {
	   return findDriverPost.all();
	}
	
	public static List<DriverPost> searchDriverPost(String startPoint, String endPoint,
			DateTime reqDateTime, String pets, String handicap, String pickup, String reqFee) {
		ExpressionList<DriverPost> search = findDriverPost.where().eq("endPoint", endPoint);
		
		if(startPoint != null) search = search.eq("startPoint", startPoint);
		if(reqDateTime != null) search = search.between("depDateTime", reqDateTime, reqDateTime.plusMonths(1));
		if(pets != null) search = search.eq("pets", Boolean.parseBoolean(pets));
		if(handicap != null) search = search.eq("handicap", Boolean.parseBoolean(handicap));
		if(pickup != null) search = search.eq("pickup", Boolean.parseBoolean(pickup));
		if(reqFee != null) search = search.between("reqFee", 0, Double.parseDouble(reqFee));
		
		return search.orderBy("depDateTime asc").findList();
		
	}
		 
	public static void driverCreate(DriverPost driverPost) { 
	    //driverPost.startDate = DateTime.now();
	    //driverPost.endDate = null;
	    driverPost.setPostedDate(DateTime.now());
	    driverPost.save();
	}
	
	public static Boolean deleteDriverPost(Long id) {
		if(findDriverPost.where().eq("id", id).findList().size() < 1) {
			return false;
		}
		findDriverPost.ref(id).delete();
		return true;
	}
	

}
