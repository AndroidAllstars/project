ScheduledMilestone1-Backend/ is the backend app
ScheduledMilestone1_AndroidApp/ is the Android app files

Checkpoint readme:
Required functionality for the scheduled milestone listed as:
1.	user can create account
2.	user can make a ride offer
3.	user can display list of available rides between specified locations
We have implemented the above functionality in the following ways:
1.	We have registration set up to authenticate a user using the accounts existing on their phones. Once authenticated they are allowed to register an account with the app which is then stored on our server.
2.	A user can make a ride offer simply by tapping the button labeled post ride offer and filling out the following form. This data is then stored in the server.
3.	From the homepage there is a search area along the top. By clicking the search button the user will be brought to the search page where they can type in start and end locations in order to display a list of matching rides offered. In the final version the search boxes on the homepage will also initialize a search, currently a search must be performed from the search page.

Backend is done using Play-Framework 2.1 (playframework.org).