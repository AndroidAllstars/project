
package views.html

import play.templates._
import play.templates.TemplateMagic._

import play.api.templates._
import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._
/**/
object index extends BaseScalaTemplate[play.api.templates.Html,Format[play.api.templates.Html]](play.api.templates.HtmlFormat) with play.api.templates.Template2[List[BasePost],Form[BasePost],play.api.templates.Html] {

    /**/
    def apply/*1.2*/(posts: List[BasePost], basePostForm: Form[BasePost]):play.api.templates.Html = {
        _display_ {import helper._


Seq[Any](format.raw/*1.55*/("""

"""),format.raw/*4.1*/("""
"""),_display_(Seq[Any](/*5.2*/main("Base post list")/*5.24*/ {_display_(Seq[Any](format.raw/*5.26*/("""
    
    <h1>"""),_display_(Seq[Any](/*7.10*/posts/*7.15*/.size())),format.raw/*7.22*/(""" post(s)</h1>

    <ul>
      """),_display_(Seq[Any](/*10.8*/for(post <- posts) yield /*10.26*/ {_display_(Seq[Any](format.raw/*10.28*/("""
        <li>
        """),_display_(Seq[Any](/*12.10*/post/*12.14*/.startPoint)),format.raw/*12.25*/(""" - """),_display_(Seq[Any](/*12.29*/post/*12.33*/.endPoint)),format.raw/*12.42*/("""
        </li>
      """)))})),format.raw/*14.8*/("""
    </ul>

    <h2>Add a new post</h2>
    """),_display_(Seq[Any](/*18.6*/form(routes.Application.newBasePost())/*18.44*/ {_display_(Seq[Any](format.raw/*18.46*/("""
      """),_display_(Seq[Any](/*19.8*/inputText(basePostForm("startPoint")))),format.raw/*19.45*/(""" - """),_display_(Seq[Any](/*19.49*/inputText(basePostForm("endPoint")))),format.raw/*19.84*/("""
      <input type="submit" value="Create">
    """)))})),format.raw/*21.6*/("""
""")))})),format.raw/*22.2*/("""
"""))}
    }
    
    def render(posts:List[BasePost],basePostForm:Form[BasePost]): play.api.templates.Html = apply(posts,basePostForm)
    
    def f:((List[BasePost],Form[BasePost]) => play.api.templates.Html) = (posts,basePostForm) => apply(posts,basePostForm)
    
    def ref: this.type = this

}
                /*
                    -- GENERATED --
                    DATE: Mon May 20 03:39:39 PDT 2013
                    SOURCE: /Users/raymondlau/Projects/yolorideplay/app/views/index.scala.html
                    HASH: a201a1b7f1c49ee6d99eef79318bd9ec227858de
                    MATRIX: 746->1|892->54|920->73|956->75|986->97|1025->99|1075->114|1088->119|1116->126|1182->157|1216->175|1256->177|1315->200|1328->204|1361->215|1401->219|1414->223|1445->232|1498->254|1578->299|1625->337|1665->339|1708->347|1767->384|1807->388|1864->423|1944->472|1977->474
                    LINES: 26->1|30->1|32->4|33->5|33->5|33->5|35->7|35->7|35->7|38->10|38->10|38->10|40->12|40->12|40->12|40->12|40->12|40->12|42->14|46->18|46->18|46->18|47->19|47->19|47->19|47->19|49->21|50->22
                    -- GENERATED --
                */
            