package com.yolo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
//*/

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.yolo.fragments.DatePickerFragment;
import com.yolo.fragments.TimePickerFragment;


public class CopyOfPostOffer extends Activity implements TimePickerFragment.OnCompleteListener, DatePickerFragment.OnCompleteListener{
	Button postBtn;
	CPostOffer tempPostOffer;
	String startPointTemp;
	String endPointTemp;
	String depTimeTemp;
	String depDateTemp;
	final static int SIZE = CPost.getSize();

	boolean pickupTemp;
	boolean petsTemp;
	boolean handicapTemp;
	
	String pickupStr;
	String petsStr;
	String handicapStr;
	double reqFeeTemp;
	
	String reqFeeStr;
	
	int seatsTemp;
	
	String seatsStr;
	EditText eTxtStartPoint;
	EditText eTxtEndPoint;
	EditText eTxtDepTime;
	EditText eTxtDepDate;
	EditText eTxtSeats;
	EditText eTxtReqFee;
	CheckBox checkPickup;
	CheckBox checkPets;
	CheckBox checkCrips;
	Button   timeInput;
	Button   dateInput;

	HttpClient httpclient = new DefaultHttpClient();
  //  HttpPost httppost = new HttpPost("http://www.yoursite.com/script.php"); //set this up
    HttpPost httppost = new HttpPost("http://protected-meadow-2488.herokuapp.com/driverposts"); //set this up
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_offer);
		if (android.os.Build.VERSION.SDK_INT > 9) {     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();     StrictMode.setThreadPolicy(policy); } 
		timeInput     = (Button) findViewById(R.id.timeInBtnPO);
		dateInput     = (Button) findViewById(R.id.dateInBtnPO);
		eTxtStartPoint = (EditText) findViewById(R.id.eTxtStartPointPO);
		eTxtEndPoint = (EditText) findViewById(R.id.eTxtEndPointPO);
		eTxtDepTime = (EditText) findViewById(R.id.timeTxtPO);
		eTxtDepDate = (EditText) findViewById(R.id.eTxtDepDatePO);
		eTxtSeats = (EditText) findViewById(R.id.eTxtSeatsPO);
		eTxtReqFee = (EditText) findViewById(R.id.eTxtReqFeePO);
		checkPickup = (CheckBox) findViewById(R.id.checkPickupPO);
		checkPets = (CheckBox) findViewById(R.id.checkPetsPO);
		checkCrips = (CheckBox) findViewById(R.id.checkCripsPO);

		timeInput.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    	  showTimePickerDialog(v);
	    	  
	      }
		});
		dateInput.setOnClickListener(new OnClickListener() {
		      public void onClick(View v) {
		    	  showDatePickerDialog(v);
		      }
			});
		postBtn = (Button) findViewById(R.id.btnSubmitPO);
		postBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
	    /*	  startPointTemp = eTxtStartPoint.toString();
	    	  endPointTemp   = eTxtEndPoint.toString();
	    	  depTimeTemp    = eTxtDepTime.toString();
	    	  depDateTemp    = eTxtDepTime.toString();
	    	  

	    	  pickupStr     = checkPickup.toString();
	    	  petsStr       = checkPickup.toString();
	    	  handicapStr   = checkCrips.toString();
	    	  
	    	 
	    	  seatsStr       = (eTxtSeats.toString());
	    	  reqFeeStr      = (eTxtReqFee.toString());
	    	  
	    	  //*/
	    	  startPointTemp = eTxtStartPoint.getText().toString();
	    	  endPointTemp   = eTxtEndPoint.getText().toString();
	    	  depTimeTemp    = eTxtDepTime.getText().toString();
	    	  depDateTemp    = eTxtDepDate.getText().toString();
	    	  //depTimeTemp = "some bullshit time";
	    	  //depDateTemp = "whenever, man";
	    	  
	    	  
	    	  pickupStr     = checkPickup.getText().toString();
	    	  petsStr       = checkPickup.getText().toString();
	    	  handicapStr   = checkCrips.getText().toString();
	    	  
	    	  pickupTemp     = checkPickup.isChecked();
	    	  petsTemp       = checkPickup.isChecked();
	    	  handicapTemp   = checkCrips.isChecked();
	    	 
	    	  seatsStr       = (eTxtSeats.getText().toString());
	    	  reqFeeStr      = (eTxtReqFee.getText().toString());
	    	  
	    	  
	    	  
	    	  //*/
	    	//  System.out.println("HEY HEY HEY LOOK HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  : '" + seatsStr + "' DONE");
		    	//  seatsTemp      = Integer.valueOf(eTxtSeats.toString());
		    	//  reqFeeTemp      = Double.valueOf(eTxtReqFee.toString());

	    	
	    	  
	    	/*  seatsTemp = 3;
	    	// reqFeeTemp = 10000;
	    	  pickupTemp = false;
	    	  petsTemp   = false;
	    	  handicapTemp = false; //*/
	    	  
	    	  
	    	  tempPostOffer = new CPostOffer(startPointTemp, endPointTemp, depTimeTemp, depDateTemp, pickupTemp, petsTemp, handicapTemp, (int) reqFeeTemp, seatsTemp);
	    	  //public void submit(){
	    	  try {
  				   // Add your data
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   1111111111111111");
  		        List<NameValuePair> listTemp = new ArrayList<NameValuePair>(10);
  		        listTemp = tempPostOffer.submit();
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   2222222222222222");
	    		  httppost.setEntity(new UrlEncodedFormEntity(listTemp));
	    		 
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   3333333333333333");

  		        // Execute HTTP Post Request
  		       HttpResponse response = httpclient.execute(httppost);
  		     HttpEntity entity = response.getEntity();
		       Integer statusCode = response.getStatusLine().getStatusCode();
		       String responseText = null;
		       StringBuilder builder = new StringBuilder();
		       
		       InputStream content = entity.getContent();
		       BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		       String line = reader.readLine();
		       builder.append(line);
		       
		       try {
		    	 JSONObject jsonObject = new JSONObject(builder.toString());
		    	 Boolean postSuccess = jsonObject.getBoolean("success");
		    	 
		    	 if(postSuccess == true) {
		    		 System.out.println("SUCCESS");
		    	 } else {
		    		 System.out.println("ERROR");
		    		 System.out.println("ERROR = " + jsonObject.getString("errorMessage"));
		    		 /*JSONArray errors = jsonObject.getJSONArray("errorMessage");
		    		 for(int i = 0; i < errors.length(); i++) {
		    			 System.out.println("Error " + i + " - " + errors.get(i));
		    		 } */
		    	 }
		    	 
		       } catch(Exception e) {
		    	   e.printStackTrace();
		       }
	    		  //System.out.println("HEY LOOK AT THIS NUMBER HERE:   4444444444444444");
	    		  //System.out.println("HERE IS THE RESPONSE: " + response.getEntity().getContent().toString());
	    		  
	    		  
	    		  
	    		  
	    		  
	    		  
	  		    } catch (ClientProtocolException e) {
	  		        // TODO Auto-generated catch block
  		    } catch (IOException e) {
  		        // TODO Auto-generated catch block
  		    } //*/
  		
	   
	    	  //somehow submit this sheet
	    	  Intent i = new Intent();
	    	//  i.setClassName("com.yolo", "com.yolo.Home");
	    	  i.setClassName("com.yolo", "com.yolo.Search"); //deal wit it
				String startSearchStr = eTxtStartPoint.getText().toString();
				String endSearchStr   =   eTxtEndPoint.getText().toString();
				String offerReqStr   = "r"; 
				i.putExtra("start", startSearchStr);
				i.putExtra("end",     endSearchStr);
				i.putExtra("o/r",     offerReqStr);
	    	  startActivity(i);
			}
	    }); //*/
		
	}

	  public void showTimePickerDialog(View v) {
		    DialogFragment newFragment = new TimePickerFragment();
		    newFragment.show(getFragmentManager(), "timePicker");
		    
		}

	  public void showDatePickerDialog(View v) {
		    DialogFragment newFragment = new DatePickerFragment();
		    newFragment.show(getFragmentManager(), "datePicker");
		    
		}

		@Override
		public void onComplete(String time, String type) {
			if(type.equals("time"))
				eTxtDepTime.setText(time);
			else if(type.equals("date"))
				eTxtDepDate.setText(time);
		}
		
		/*
		@Override
		public void DatePickerFragment.onComplete(String time) {
			eTxtDepTime.setText(time);
		}
		/*/
}
