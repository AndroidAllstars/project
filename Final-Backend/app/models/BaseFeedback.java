package models;

import play.db.ebean.Model;
import play.db.ebean.Model.Finder;
import play.mvc.*;
import javax.persistence.*;

import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
public class BaseFeedback extends Model {
	
	@Id
	public Long id;
	
	public String comments;
	
	public static Finder<Long,BaseFeedback> findBaseFeedback = new Finder(
			   Long.class, BaseFeedback.class
			);
	
}
