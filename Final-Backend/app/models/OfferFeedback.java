package models;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;

import play.db.ebean.Model.Finder;

@Entity
public class OfferFeedback extends BaseFeedback {
	
	@ManyToOne
	@JsonIgnore
	public YRUser rater;
	
	public String getUname() {
		return rater.getUname();
	}
	
	@ManyToOne
	@JsonIgnore
	public YRUser ratee;
	
	@ManyToOne
	@JsonIgnore
	public DriverPost post;
	
	public Boolean successfulRide;
	public Boolean wouldRideAgain;
	public Integer ratingOfDriver;
	public Integer ratingOfCar;
	
	public static Finder<Long,OfferFeedback> findOfferFeedback = new Finder(
			   Long.class, OfferFeedback.class
			);
	
	public int hashCode() {
		return new HashCodeBuilder(17,31).append(rater.getUname()).build();
	}
	
	public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (!(obj instanceof OfferFeedback))
            return false;

        OfferFeedback rhs = (OfferFeedback) obj;
        return new EqualsBuilder().
            // if deriving: appendSuper(super.equals(obj)).
            append(rater.getUname(), rhs.rater.getUname()).
            isEquals();
    }

}
