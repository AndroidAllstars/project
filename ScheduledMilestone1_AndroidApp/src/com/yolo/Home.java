

package com.yolo;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ListAdapter;
import android.widget.ArrayAdapter;

public class Home extends Activity {
	Button			camera_button;
	/*ImageButton     example_button;
	ImageButton  	how_button; //*/
	Button 			postOfferBtn;
	Button 			searchBtn;
	Button			postWantedBtn;
	ListView 		listOfRides;
	Button 			tempBtn;
	TextView		tempTxt;
	CPostOffer tempPostOffer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		tempPostOffer = new CPostOffer("start", "end", "12:00", "12/12/12", false, false, false, 99.99, 3);
	/*	listOfRides = (ListView) findViewById(R.id.listOfRidesHome);
		//tempBtn = (Button) findViewById(R.id.btnWantedHome);
	//	tempTxt.setText("hello this is dog");
		Context ctx = this;
		tempPostOffer = new CPostOffer("start", "end", "12:00", "12/12/12", false, false, false, 99.99, 3);
		ArrayAdapter<CPostOffer> adapter = new ArrayAdapter<CPostOffer>(ctx, R.id.textView1);
		System.out.println("this CPost says...................  : " + tempPostOffer);
		adapter.add(tempPostOffer);
		listOfRides.setAdapter(adapter);
		//*/
		
		postOfferBtn = (Button) findViewById(R.id.btnOfferHome);
		postOfferBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.PostOffer");
			startActivity(i);
			}
	    }); //*/

		postWantedBtn = (Button) findViewById(R.id.btnWantedHome);
		postWantedBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.PostWanted");
			startActivity(i);
			}
	    }); //*/
		
		searchBtn = (Button) findViewById(R.id.btnSearchHome);
		searchBtn.setOnClickListener(new OnClickListener() {
	      public void onClick(View v) {
			Intent i = new Intent();
			i.setClassName("com.yolo", "com.yolo.Search");
			startActivity(i);
			}
	    }); //*/

	    final ListView listview = (ListView) findViewById(R.id.listOfRidesHome);
	    String[] values = new String[] { tempPostOffer.toString()};

	    final ArrayList<String> list = new ArrayList<String>();
	    for (int i = 0; i < values.length; ++i) {
	      list.add(values[i]);
	    }
	    final StableArrayAdapter adapter = new StableArrayAdapter(this,
	        android.R.layout.simple_list_item_1, list);
	    listview.setAdapter(adapter);

/*	    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	      @Override
	      public void onItemClick(AdapterView<?> parent, final View view,
	          int position, long id) {
	        final String item = (String) parent.getItemAtPosition(position);
	      }

	    });//*/
	}

	  private class StableArrayAdapter extends ArrayAdapter<String> {

	    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

	    public StableArrayAdapter(Context context, int textViewResourceId,
	        List<String> objects) {
	      super(context, textViewResourceId, objects);
	      for (int i = 0; i < objects.size(); ++i) {
	        mIdMap.put(objects.get(i), i);
	      }
	    }

	    @Override
	    public long getItemId(int position) {
	      String item = getItem(position);
	      return mIdMap.get(item);
	    }

	    @Override
	    public boolean hasStableIds() {
	      return true;
	    }

	  }
}


